<?php

namespace App\Providers;

use Illuminate\View\View;

use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // locations
        view()->composer(
            'layouts.main',
            'App\Http\ViewComposers\LocationComposer'
        );
        view()->composer(
            'errors.404',
            'App\Http\ViewComposers\LocationComposer'
        );
        view()->composer(
            'home',
            'App\Http\ViewComposers\LocationComposer'
        );
        view()->composer(
            'listings',
            'App\Http\ViewComposers\LocationComposer'
        );
        view()->composer(
            'gallery',
            'App\Http\ViewComposers\LocationComposer'
        );
        view()->composer(
            'auth.register',
            'App\Http\ViewComposers\LocationComposer'
        );
        view()->composer(
            'auth.login',
            'App\Http\ViewComposers\LocationComposer'
        );
        view()->composer(
            'auth.passwords.email',
            'App\Http\ViewComposers\LocationComposer'
        );
        view()->composer(
            'auth.passwords.reset',
            'App\Http\ViewComposers\LocationComposer'
        );
        view()->composer(
            'house',
            'App\Http\ViewComposers\LocationComposer'
        );
        view()->composer(
            'house_sunnyvale',
            'App\Http\ViewComposers\LocationComposer'
        );
        view()->composer(
            'house_sanjose',
            'App\Http\ViewComposers\LocationComposer'
        );
        view()->composer(
            'house_cupertino',
            'App\Http\ViewComposers\LocationComposer'
        );
        view()->composer(
            'house_santaclara',
            'App\Http\ViewComposers\LocationComposer'
        );
        view()->composer(
            'house_piscataway',
            'App\Http\ViewComposers\LocationComposer'
        );
        view()->composer(
            'house_milpitas',
            'App\Http\ViewComposers\LocationComposer'
        );
        view()->composer(
            'house_metuchen',
            'App\Http\ViewComposers\LocationComposer'
        );
        view()->composer(
            'house_levis',
            'App\Http\ViewComposers\LocationComposer'
        );
        view()->composer(
            'shared-living',
            'App\Http\ViewComposers\LocationComposer'
        );
        view()->composer(
            'team',
            'App\Http\ViewComposers\LocationComposer'
        );
        view()->composer(
            'book',
            'App\Http\ViewComposers\LocationComposer'
        );
        view()->composer(
            'locations',
            'App\Http\ViewComposers\LocationComposer'
        );
        view()->composer(
            'amenities',
            'App\Http\ViewComposers\LocationComposer'
        );
        view()->composer(
            'rooms',
            'App\Http\ViewComposers\LocationComposer'
        );
        view()->composer(
            'testimonials',
            'App\Http\ViewComposers\LocationComposer'
        );
        view()->composer(
            'dreamhome',
            'App\Http\ViewComposers\LocationComposer'
        );
        view()->composer(
            'about',
            'App\Http\ViewComposers\LocationComposer'
        );
        view()->composer(
            'contact',
            'App\Http\ViewComposers\LocationComposer'
        );
        view()->composer(
            'contact',
            'App\Http\ViewComposers\LocationComposer'
        );
        view()->composer(
            'help',
            'App\Http\ViewComposers\LocationComposer'
        );
        view()->composer(
            'rooms_shared',
            'App\Http\ViewComposers\LocationComposer'
        );
        view()->composer(
            'rooms_private',
            'App\Http\ViewComposers\LocationComposer'
        );
        view()->composer(
            'rooms_family',
            'App\Http\ViewComposers\LocationComposer'
        );
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
