<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Checkfront;
use App\FieldBook\PhieldBook;

use Cache;
use Config;
use Session;

class HouseController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->Checkfront = new Checkfront();

        // we need to call fieldbook for location and room type code mapping data
        $fb_keys = array(
          'api_key' => env('FIELDBOOK_KEY'),
          'api_secret' => env('FIELDBOOK_SECRET')
        );
        $this->fb = new PhieldBook($fb_keys);
        $this->fb->book_id = env('BOOK_ID');
    }

    public function show($id)
    {
        $filteredItems = array();
        $inputs = array();
        $inputs = Session::get('inputs');
        
        $startDate = date("m/d/Y");
        if(!empty($inputs)) {
            $startDate = $inputs['start_date'];
        } else {
            $inputs['start_date'] = date("m/d/Y"); // today
            $inputs['end_date'] = date('m/d/Y', strtotime($inputs['start_date'] . ' + 29 day')); // 30 days from today
            $inputs['user_end_date'] = date('m/d/Y', strtotime($inputs['start_date'] . ' + 29 day')); // 30 days from today
            $inputs['location'] = ''; // all locations
        }

        $endDate = date('Y-m-d', strtotime($startDate . ' + 29 day'));
        $location = $inputs['location'];

        $data = array(
            'qty' => 1,
            'month' => 1,
            'start_date' => $startDate,
            'end_date' => $endDate,
            'location' => $location,
            'today' => date("m/d/Y")
        );
        $inputs = $data;

        // Single home
        $home = $this->Checkfront->get('item/'.$id, $data);
        // print_r($home);
        $home = $home['item'];
        $images = $home['image'];
        
        // massaging data
        // sku has all need information.
        // check plutio https://chaay.plutio.com/projects/longtermstayco/conversations/NYPDPtKfpKg27PimD
        $sku = explode('x', $home['sku']);
        
        // get location code from sku and get location name from fieldbook
        // match both and present location name
        
        // match loc code from sku with fieldbook
        $home['loc'] = '';
        $home['loc-lat'] = '';
        $home['loc-lon'] = '';
        $home['loc-full'] = '';
        if (isset($home['meta']['location'])) {
            $home['loc-lat'] = $home['meta']['location']['lat'];
            $home['loc-lon'] = $home['meta']['location']['lng'];
            $home['loc-full'] = preg_replace('#^\d+#', '', $home['meta']['location']['str']);
        }

        $locations = $this->cache(env('LOCATIONS_SHEET'));
        foreach($locations as $key=>$loc) {
            if(strtolower($loc['code']) == strtolower($sku[0])) {
                $home['loc'] = $loc['location_name'];
                $home['loc_code'] = $sku[0];
            }
        }
        
        // private, shared, family type
        // match roomtype code from sku with fieldbook
        $home['roomtype'] = '';
        $roomtype = $this->cache(env('ROOMTYPE_SHEET'));
        foreach($roomtype as $key=>$room) {
            $skuRoom = explode('z', $sku[4]);
            // print_r($skuRoom);
            if(strtolower($room['code']) == strtolower($skuRoom[0])) {
                $home['roomtype'] = $room['type'];
                $home['roomtype_code'] = $skuRoom[0];
                if(isset($skuRoom[1])) {
                    $home['bath'] = $skuRoom[1];
                } else {
                    $home['bath'] = 'nab';
                }
            }
        }
        
        // short term and long term leases
        $stayterm = $sku[3];
        $stayterm = preg_replace('/\d/', '', $stayterm );
        $home['stayterm'] = ($stayterm == 'lt') ? "Long term" : "Short term";
        $home['bath'] = ($home['bath'] == 'ab') ? "Attached Bath" : "No Attached Bath";
        $home['price'] = "$".floor($home["rate"]["sub_total"])." per month";
        
        return view('house', ['home' => $home, 'inputs'=> $inputs, 'name' => $home['name']]);
    }
    
    public function house_sunnyvale()
    {
        $filteredItems = array();
        $inputs = array();
        $inputs = Session::get('inputs');
        
        $startDate = date("m/d/Y");
        if(!empty($inputs)) {
            $startDate = $inputs['start_date'];
        }

        $endDate = date('Y-m-d', strtotime($startDate . ' + 29 day'));
        

        $data = array(
            'qty' => 1,
            'month' => 1,
            'start_date' => $startDate,
            'end_date' => $endDate,
            'today' => date("m/d/Y")
        );
        $inputs = $data;

        
        return view('house_sunnyvale', ['inputs'=> $inputs]);
    }

    public function house_sanjose()
    {
        $filteredItems = array();
        $inputs = array();
        $inputs = Session::get('inputs');
        
        $startDate = date("m/d/Y");
        if(!empty($inputs)) {
            $startDate = $inputs['start_date'];
        }

        $endDate = date('Y-m-d', strtotime($startDate . ' + 29 day'));
        

        $data = array(
            'qty' => 1,
            'month' => 1,
            'start_date' => $startDate,
            'end_date' => $endDate,
            'today' => date("m/d/Y")
        );
        $inputs = $data;

        
        return view('house_sanjose', ['inputs'=> $inputs]);
    }

    public function house_cupertino()
    {
        $filteredItems = array();
        $inputs = array();
        $inputs = Session::get('inputs');
        
        $startDate = date("m/d/Y");
        if(!empty($inputs)) {
            $startDate = $inputs['start_date'];
        }

        $endDate = date('Y-m-d', strtotime($startDate . ' + 29 day'));
        

        $data = array(
            'qty' => 1,
            'month' => 1,
            'start_date' => $startDate,
            'end_date' => $endDate,
            'today' => date("m/d/Y")
        );
        $inputs = $data;

        
        return view('house_cupertino', ['inputs'=> $inputs]);
    }

    public function house_santaclara()
    {
        $filteredItems = array();
        $inputs = array();
        $inputs = Session::get('inputs');
        
        $startDate = date("m/d/Y");
        if(!empty($inputs)) {
            $startDate = $inputs['start_date'];
        }

        $endDate = date('Y-m-d', strtotime($startDate . ' + 29 day'));
        

        $data = array(
            'qty' => 1,
            'month' => 1,
            'start_date' => $startDate,
            'end_date' => $endDate,
            'today' => date("m/d/Y")
        );
        $inputs = $data;

        
        return view('house_santaclara', ['inputs'=> $inputs]);
    }

    public function house_piscataway()
    {
        $filteredItems = array();
        $inputs = array();
        $inputs = Session::get('inputs');
        
        $startDate = date("m/d/Y");
        if(!empty($inputs)) {
            $startDate = $inputs['start_date'];
        }

        $endDate = date('Y-m-d', strtotime($startDate . ' + 29 day'));
        

        $data = array(
            'qty' => 1,
            'month' => 1,
            'start_date' => $startDate,
            'end_date' => $endDate,
            'today' => date("m/d/Y")
        );
        $inputs = $data;

        
        return view('house_piscataway', ['inputs'=> $inputs]);
    }

    public function house_milpitas()
    {
        $filteredItems = array();
        $inputs = array();
        $inputs = Session::get('inputs');
        
        $startDate = date("m/d/Y");
        if(!empty($inputs)) {
            $startDate = $inputs['start_date'];
        }

        $endDate = date('Y-m-d', strtotime($startDate . ' + 29 day'));
        

        $data = array(
            'qty' => 1,
            'month' => 1,
            'start_date' => $startDate,
            'end_date' => $endDate,
            'today' => date("m/d/Y")
        );
        $inputs = $data;

        
        return view('house_milpitas', ['inputs'=> $inputs]);
    }

    public function house_metuchen()
    {
        $filteredItems = array();
        $inputs = array();
        $inputs = Session::get('inputs');
        
        $startDate = date("m/d/Y");
        if(!empty($inputs)) {
            $startDate = $inputs['start_date'];
        }

        $endDate = date('Y-m-d', strtotime($startDate . ' + 29 day'));
        

        $data = array(
            'qty' => 1,
            'month' => 1,
            'start_date' => $startDate,
            'end_date' => $endDate,
            'today' => date("m/d/Y")
        );
        $inputs = $data;

        
        return view('house_metuchen', ['inputs'=> $inputs]);
    }

    public function house_levis()
    {
        $filteredItems = array();
        $inputs = array();
        $inputs = Session::get('inputs');
        
        $startDate = date("m/d/Y");
        if(!empty($inputs)) {
            $startDate = $inputs['start_date'];
        }

        $endDate = date('Y-m-d', strtotime($startDate . ' + 29 day'));
        

        $data = array(
            'qty' => 1,
            'month' => 1,
            'start_date' => $startDate,
            'end_date' => $endDate,
            'today' => date("m/d/Y")
        );
        $inputs = $data;

        
        return view('house_levis', ['inputs'=> $inputs]);
    }

    public function cache($sheet_title)
    {
        $this->fb->sheet_title = $sheet_title;
        
        if (Cache::has($sheet_title))  //If it's cached...
            return response(Cache::get($sheet_title))->original;   //... return the value from cache and we're done.

        $response = $this->fb->get();  //If it wasn't cached, execute the request and grab the response

        $cachingTime = Config::get('cache.tts');  //Let's cache it for 60 minutes
        Cache::put($sheet_title, $response, $cachingTime);  //Cache response
    }
}