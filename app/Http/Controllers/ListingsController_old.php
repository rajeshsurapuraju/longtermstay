<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\CheckFront\Booking;

class ListingsController extends Controller
{
    public $booking;
    public function listings()
    {
        //echo session()->get('start_date') . "\n";
        //echo session()->get('end_date') . "\n";
        //echo session()->get('location');

        //destroy($_SESSION['inputs']);

        $start_date = session()->get('start_date'); 
        $end_date = session()->get('end_date');
        $location = session()->get('location');

        $search = array(
                'start_date'=> $start_date,
                'end_date'=> $end_date,
                'location'=> $location
            );
    
        $_SESSION['inputs'] = $search;

        //echo "items : <pre>";
        //print_r($_SESSION['inputs']);
        //exit;

        if(!empty($start_date)) {
            
            // Query inventory to booking api for listings

            $this->booking = new Booking(); 
            //exit;

            $items = $this->booking->query_inventory(
                array(
                    'start_date'=>$start_date,
                    'end_date'=>$end_date,
                    'keyword' =>$location,
                    // change these booking parameters to suit your setup:
                    'param'=>array( 'guests' => 1 )
                )
            );
            
        //echo "items : <pre>";
        //print_r($items);
        //exit;

            // Query Categories from booking api for categories
            $categories = $this->booking->query_category();
            
            $listings = array();

            // If API returns listings
            if(count($items)) {
                // Loop thru the listings returned by API
                foreach($items as $item_id => $item) {
                    // Check if listing item contains rate slip
                    if (empty($item['rate']['slip'])) continue;

                    // Check if listing item is available
                    if ($item['rate']['summary']['title'] === 'Unavailable') continue;

                    // Create listings object for each item with fields required to display on page.
                    $listings[$item_id] = [
                        'item_id' => $item['item_id'],
                        'name' => $item['name'],
                        'summary' => $item['summary'],
                        'slip' => $item['rate']['slip'],
                        'available' => $item['rate']['available'],
                        'date' => $item['rate']['summary']['date'],
                        'category_id' => $item['category_id'],
                        'category_name' => isset($categories[$item['category_id']]) ? $categories[$item['category_id']]['name'] : '',
                        'price' => empty($item['rate']['summary']['price']['title']) ? $item['rate']['summary']['price']['total'] : $item['rate']['summary']['price']['title'],
                        'price_unit' => empty($item['rate']['summary']['price']['unit']) ? 'for 30 days' : $item['rate']['summary']['price']['unit'],
                        'price_details' => $item['rate']['summary']['details'],
                        'image' => $item['image'],
                        'location' => isset($item['meta']['location']) ? $item['meta']['location'] : ''
                    ];
                }
            }

            // Assign
            $data['listings'] = $listings;
            $data['search'] = $search;
            //$data['searchquery'] = $this->getView('searchquery', $_SESSION['inputs'], TRUE);

            // Load Listings page view using Default template
            //$this->template->load('default', 'listings', $data);

            return view('listings', compact('data')); 


        } else {
            // Redirect to home if empty user search data
            return redirect('home');
        }        

    }

    public function select(Request $request)
    {
        //echo "<pre>";
        //print_r($request->slip[0]);
        //exit;

        $this->booking = new Booking();
        $this->booking->set($request->slip);

        return redirect('book');

    }

    public function book(Request $request)
    {
        //echo "<pre>";
        //print_r($request->slip[0]);
        //exit;

        $this->booking = new Booking();

        //echo "<pre>";
        //print_r($_SESSION['item']);
        //exit;

        $session = $_SESSION;
        //echo "Reset : " . $request->get('reset');
        //exit;
        
        if($request->get('reset') == 1)
        {
            $this->booking->clear();
            
            $start_date = session()->get('start_date'); 
            $end_date = session()->get('end_date');
            $location = session()->get('location');

            if($start_date != '')
                return redirect('search_validation?start_date='.$start_date.'&end_date='.$end_date.'&location='.$location);
            else
                return redirect('home');
        }

        return view('book', compact('session'));

    }
}
