<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestimonialsController extends Controller
{
    public function index()
    {
        $start_date = session()->get('start_date'); 
        $end_date = session()->get('end_date');
        $location = session()->get('location');

        $search = array(
                'start_date'=> $start_date,
                'end_date'=> $end_date,
                'location'=> $location
            );
       return view('testimonials', ['search' => $search]);
    }
}