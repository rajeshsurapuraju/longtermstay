<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Mail;
use Session;

class PagesController extends Controller
{
    public function locations()
    {
        $start_date = session()->get('start_date'); 
        $end_date = session()->get('end_date');
        $location = session()->get('location');

        $search = array(
                'start_date'=> $start_date,
                'end_date'=> $end_date,
                'location'=> $location
            );
       return view('locations', ['search' => $search]); 
    }

    public function amenities()
    {
        $start_date = session()->get('start_date'); 
        $end_date = session()->get('end_date');
        $location = session()->get('location');

        $search = array(
                'start_date'=> $start_date,
                'end_date'=> $end_date,
                'location'=> $location
            );
       return view('amenities', ['search' => $search]); 
    }

    public function contact(Request $request)
    {
        //print_r($_POST);
        //exit;
           return view('contact'); 
    }

    public function postContact(Request $request)
    {
        $data = array(
            'email' => $request->email,
            'name' => $request->name,
            'phone' => $request->phone,
            'loc' => $request->loc,
            'room' => $request->room,
            'flexroom' => $request->flexroom,
            'movein' => $request->movein,
            'flexmovein' => $request->flexmovein,
            'address' => $request->address,
            'calltime' => $request->calltime,
            'mailfirst' => $request->mailfirst,
            'bodyMessage' => $request->message
        );
        // print_r($data);
        $this->validate($request, ['email' => 'required|email', 'name' => 'min:3', 'message' => 'min:10']);
        Mail::send('layouts.email_contact', $data, function($message) use ($data){
            $message->from($data['email']);
            $message->to(env('MAIL_TO'));
            $message->subject(env('MAIL_SUBJECT'));
        });
        Session::flash('success', 'Email sent successfully');
        return redirect('contact');
    }

    public function about()
    {
           return view('about'); 
    }

    public function team()
    {
           return view('team'); 
    }

    public function search_validation(Request $request)
    {
        //print_r($request->get('start_date'));
        
        if(Session::has('start_date'))
        {
            //exit;
            Session::forget('start_date');
            Session::forget('end_date');
            Session::forget('location');
        }

        session()->put('start_date', $request->get('start_date'));
        session()->put('end_date', $request->get('end_date'));
        session()->put('location', $request->get('location'));

        // echo session()->get('end_date');

        return redirect('listings');
    }

}
