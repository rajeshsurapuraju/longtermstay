<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FieldBook\PhieldBook;
use Checkfront;
use App\LTSAPI\LTSAPI;

use Cache;
use Config;
use Session;

class ListingsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->Checkfront = new Checkfront();
        $this->ltsapi = new LTSAPI();
        
        $this->api_flag = env('LTS_CORE_API', false);
        $qs = $request->query();

        // use either checkfront or LTS API
        if($this->api_flag) {
            if(empty($qs) || $qs['ltsapi'] == 1) {
                $this->api = new LTSAPI();
            } else {
                $this->api = new Checkfront();
                $this->api_flag = false;
            }
        } else {
            $this->api = new Checkfront();
        }
        
        // we need to call fieldbook for location and room type code mapping data
        $fb_keys = array(
          'api_key' => env('FIELDBOOK_KEY'),
          'api_secret' => env('FIELDBOOK_SECRET')
        );
        $this->fb = new PhieldBook($fb_keys);
        $this->fb->book_id = env('BOOK_ID');
    }

    public function listings(Request $request)
    {
        $locItems = array();
        $notLocItems = array();
        $filteredItems = array();
        $locations = array();
        $roomtype = array();


        $startDate = $request->input('start_date') ? $request->input('start_date') : date("m/d/Y");
        $endDate = date('m/d/Y', strtotime($startDate . ' + 29 day'));
        $keyword = $request->input('keyword') ? $request->input('keyword') : '';
        $location = $request->input('location') ? $request->input('location') : null;
        $userEndDate = $request->input('end_date') ? $request->input('end_date') : $endDate;

        $request['start_date'] = $startDate;
        $request['end_date'] = $endDate;
        $request['keyword'] = $keyword;
        $request['location'] = $location;
        $request['today'] = date("m/d/Y");
        $request['user_end_date'] = $userEndDate;

        $data = array(
            'start_date' => $this->api_flag ? date("Ymd", strtotime($startDate)) : $startDate,
            'end_date' => $this->api_flag ? date("Ymd", strtotime($endDate)) : $endDate,
            'keyword' => $keyword, // remove this once api is ready, setting this to empty and filter on server
            'location' => $location,
            'cityCode' => $location,
            'available' => 1,
            'today' => date("m/d/Y")
            //'rules' => 'soft',
        );
        
        $search = array(
            'start_date'=> $startDate,
            'end_date'=> $userEndDate,
            'location'=> $location
        );

        Session::put('inputs',$request->all());

        if(!empty($startDate)) {

            $items = $this->api->get('item', $data);
            // print_r($items);

            $listings = array();

            // if api returns items
            if($items) {
                
                // for checkfront get inner items, 1 level deep
                $items = $this->api_flag ? $items : $items['items'];

                // roll thru all items and massage data
                foreach($items as $item) {
                    // print_r($item);
                    // print_r($item['category']);
                    
                    // since number of items is less
                    // we get items for all locations and filter them here.
                    // we do this for faster client side filtering too.
                    $matchLoc = "/".strtolower($location)."x/i";
                    // do only for checkfront as lts api should handle this in future
                    if(!$this->api_flag) {
                        if(!preg_match($matchLoc, $item['sku'])) {
                            continue;
                        }
                    }
                    
                    // massaging data
                    // sku has all need information.
                    // check plutio https://chaay.plutio.com/projects/longtermstayco/conversations/NYPDPtKfpKg27PimD
                    $sku = explode('x', $item['sku']);
                    
                    // get location code from sku and get location name from fieldbook
                    // match both and present location name
                    
                    // match loc code from sku with fieldbook
                    $item['loc'] = '';
                    $locations = $this->cache(env('LOCATIONS_SHEET'));
                    foreach($locations as $key=>$loc) {
                        if(strtolower($loc['code']) == strtolower($sku[0])) {
                            $item['loc'] = $loc['location_name'];
                            $item['loc_code'] = $sku[0];
                        }
                    }
                    
                    // private, shared, family type
                    // match roomtype code from sku with fieldbook
                    $item['roomtype'] = '';
                    $roomtype = $this->cache(env('ROOMTYPE_SHEET'));
                    foreach($roomtype as $key=>$room) {
                        $skuRoom = explode('z', $sku[4]);
                        // print_r($skuRoom);
                        if(strtolower($room['code']) == strtolower($skuRoom[0])) {
                            $item['roomtype'] = $room['type'];
                            $item['roomtype_code'] = $skuRoom[0];
                            if(isset($skuRoom[1])) {
                                $item['bath'] = $skuRoom[1];
                            } else {
                                $item['bath'] = 'nab';
                            }
                        }
                    }
                    
                    // short term and long term leases
                    $stayterm = $sku[3];
                    $stayterm = preg_replace('/\d/', '', $stayterm );
                    $item['stayterm'] = $stayterm;
                    
                    
                    if($this->api_flag) {
                        $item['rate'] = [
                            'summary' => [
                                'price' => [
                                    'total' => 'unrated'
                                ]
                            ],
                            'sub_total' => [
                                '1000'
                            ]
                        ];
                    }
                    
                    $item['price'] = floor($item["rate"]["sub_total"]);


                    $images = $item['image'];
                    if($images) {
                        foreach($images as $img) {
                            $item['img']=$img['path'];
                            break;
                        }
                    } else {
                        $item['img']='/media/M167-4--6962.jpg';
                    }
                    array_push($filteredItems, $item);
                }
            }

            // print_r($filteredItems);
            Session::put('items', $filteredItems);
            
            return view('listings', ['items'=>$filteredItems, 'search'=> $search, 'locations' => $locations, 'roomtype' => $roomtype]); 


        } else {
            // Redirect to home if empty user search data
            return redirect('home');
        }        

    }
    
    public function cache($sheet_title)
    {
        $this->fb->sheet_title = $sheet_title;
        
        if (Cache::has($sheet_title))  //If it's cached...
            return response(Cache::get($sheet_title))->original;   //... return the value from cache and we're done.

        $response = $this->fb->get();  //If it wasn't cached, execute the request and grab the response

        $cachingTime = Config::get('cache.tts');  //Let's cache it for 60 minutes
        Cache::put($sheet_title, $response, $cachingTime);  //Cache response
    }
}