<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// use App\LTSAPI\LTSAPI;

use Session;

class HomeController extends Controller
{
    public function index()
    {
        // search data

        $inputs = array();

        // get search inputs from session
        $inputs = Session::get('inputs');

        // if empty session data and empty inputs, create your own data to fill search form.
        // or if user has come first time to the page
        if(empty($inputs)) {
            $inputs['start_date'] = date("m/d/Y"); // today
            $inputs['end_date'] = date('m/d/Y', strtotime($inputs['start_date'] . ' + 29 day')); // 30 days from today
            $inputs['user_end_date'] = date('m/d/Y', strtotime($inputs['start_date'] . ' + 29 day')); // 30 days from today
            $inputs['location'] = ''; // all locations
        }

        // today
        if(empty($inputs['today'])) {
            $inputs['today'] = date("m/d/Y");
        }

        // set defaults
        $startDate = ($inputs['start_date']) ? $inputs['start_date'] : date("m/d/Y");
        $endDate = ($inputs['user_end_date']) ? $inputs['user_end_date'] : date('m/d/Y', strtotime($startDate . ' + 29 day'));
        $location = ($inputs['location']) ? $inputs['location'] : '';

        $data = array(
            'start_date' => $startDate,
            'end_date' => $endDate,
            'location' => $location
        );

        return view('home', ['data' => $data, 'search' => $data]);

    }
}
