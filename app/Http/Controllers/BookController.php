<?php

namespace App\Http\Controllers;

use Config;
use App\Http\Requests;
use Illuminate\Http\Request;
use Checkfront;
use Session;

class BookController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->session_id = '';
        $this->Checkfront = new Checkfront();            
    }
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $inputs = array();
        $inputs = Session::get('inputs');

        $startDate = date("Y-m-d");
        if(!empty($inputs)) {
            $startDate = $inputs['start_date'];
        } else {
            $inputs['start_date'] = date("m/d/Y"); // today
            $inputs['end_date'] = date('m/d/Y', strtotime($inputs['start_date'] . ' + 29 day')); // 30 days from today
            $inputs['user_end_date'] = date('m/d/Y', strtotime($inputs['start_date'] . ' + 29 day')); // 30 days from today
            $inputs['location'] = ''; // all locations
        }
        // $startDate = (empty($inputs) == 1) ? date("Y-m-d") : $inputs['start_date']) ? $inputs['start_date'] : date("Y-m-d");
        $endDate = date('Y-m-d', strtotime($startDate . ' + 29 day'));

        //echo(session_id());
        $data = array(
            'qty' => 1,
            'month' => 1,
            'start_date' => $startDate,
            'end_date' => $endDate
        );

        $item = $this->Checkfront->get('item/'.$id, $data);
        $item = $item['item'];
        $item['loc'] = '';
        if(isset($item['meta']['location'])) {
            $item['loc'] = preg_replace('#^\d+#', '', $item['meta']['location']['str']);
        }
        $name = explode('_', $item['name']);
        $item['name'] = $name[0];
        $images = $item['image'];
        if($images) {
            foreach($images as $img) {
                $item['img']=$img['path'];
            }
        } else {
            $item['img']='/media/M167-4--6962.jpg';
        }

        $form = $this->Checkfront->get('booking/form');

        if(isset($_POST['slip'])) {
            $response = $this->Checkfront->post('booking/session',array('slip'=>$_POST['slip']));

            $this->session_id = $response['booking']['session']['id'];
            Session::put('data',$response['booking']['session']);
            // print_r( Session::all());

            return view('book', ['item' => $item,'form' => $form, 'inputs' => $inputs]);
        }
        return view('book', ['item' => $item,'form' => $form, 'inputs' => $inputs]);
    }
    public function validation(Request $request) {
        $inputs = Session::get('inputs');
        
        $this->validate($request, [
            'customer_name' => 'required',
            'customer_email' => 'required',
            'customer_phone' => 'required',
            'customer_address' => 'required',
            'customer_city' => 'required',
            'customer_country' => 'required',
            'customer_region' => 'required',
            'customer_postal_zip' => 'required',
            // 'note' => 'required',
            'customer_tos_agree' => 'required',
            'cust_fb' => 'required',
            'cust_comp_name' => 'required'
        ]);
        // appending user input end date in note for our reference
        $_POST['note'] = $_POST['note'] . "*** User selected end date is " . $inputs['user_end_date'] . " ***";
        $form = $_POST;
        $data = Session::get('data');
        $this->session_id = $data['id'];

        $response = $this->Checkfront->post('booking/create',array('form'=>$form, 'session_id' => $this->session_id));
        if($response['request']['status'] == 'OK') {
            // Successful transactions will return a url to be redirected to for payment or an invoice.
            header("Location: {$response['request']['data']['url']}");
            return redirect($response['request']['data']['url']);
        } else {
            // Show error on booking failure
            echo($response['request']['error']['details']);
            echo($response['request']['status']);
        }
    }
}