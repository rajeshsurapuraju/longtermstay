<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Session;
use App\FieldBook\PhieldBook;

use Cache;
use Config;

class DreamHomeController extends Controller
{
    /**
     * Create a movie composer.
     *
     * @return void
     */
    public function __construct()
    {
        $dreamhome_sheet = env('DREAMHOME_SHEET');
        
        $fb_keys = array(
          'api_key' => env('FIELDBOOK_KEY'),
          'api_secret' => env('FIELDBOOK_SECRET')
        );
        
        $this->fb = new PhieldBook($fb_keys);

        $this->fb->book_id = env('BOOK_ID');
        $this->fb->sheet_title = $dreamhome_sheet;
        
        if (Cache::has($dreamhome_sheet))
            $this->data = response(Cache::get($dreamhome_sheet))->original;

        $this->data = $this->fb->get();

        $cachingTime = Config::get('cache.tts');
        Cache::put($dreamhome_sheet, $this->data, $cachingTime);
    }

    public function index()
    {
        print_r($this->data);
        exit;
        $inputs = array();
        $inputs = Session::get('inputs');
        if(empty($inputs)) {
            $inputs['start_date'] = date("m/d/Y");
            $inputs['end_date'] = date('m/d/Y', strtotime($inputs['start_date'] . ' + 30 day'));
            $inputs['location'] = '';
        }
        if(empty($inputs['today'])) {
            $inputs['today'] = date("m/d/Y");
        }
        
        $startDate = ($inputs['start_date']) ? $inputs['start_date'] : date("m/d/Y");
        $endDate = date('m/d/Y', strtotime($startDate . ' + 30 day'));
                
        $data = array(
            'start_date' => $startDate,
            'endDate' => $endDate,
            'location' => null
        );
        return view('home', ['data' => $inputs, 'search' => $inputs]);

    }

    public function showSlug($slug)
    {
        print_r($this->data);

        if (!$product) {
            abort(404);
        }
        return $this->show($product->id);
    }
}
