<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\FieldBook\PhieldBook;

use Cache;
use Config;

class LocationComposer
{
    /**
     * Create a movie composer.
     *
     * @return void
     */
    public function __construct()
    {
        /*
        $key = $request->fullUrl();  //key for caching/retrieving the response value

        if (Cache::has($key))  //If it's cached...
            return response(Cache::get($key));   //... return the value from cache and we're done.

        $response = $next($request);  //If it wasn't cached, execute the request and grab the response

        $cachingTime = Config::get('cache.tts');  //Let's cache it for 60 minutes
        Cache::put($key, $response->getContent(), $cachingTime);  //Cache response

        */

        $locations_sheet = env('LOCATIONS_SHEET');
        $locations_page_sheet = env('LOCATIONS_PAGE_SHEET');
        $home_page_sheet = env('HOME_PAGE_SHEET');
        $testimonials_sheet = env('TESTIMONIALS_SHEET');
        $blog_sheet = env('BLOG_SHEET');
        $dreamhome_sheet = env('DREAMHOME_SHEET');
        $nav_sheet = env('NAV_SHEET');
        $promotions_sheet = env('PROMOTIONS_SHEET');
        $logo_sheet = env('LOGO_SHEET');
        $team_sheet = env('TEAM_SHEET');
        $footer_sheet = env('FOOTER_SHEET');
        $roomtype_sheet = env('ROOMTYPE_SHEET');
        $features_sheet = env('FEATURES_SHEET');
        $sections_sheet = env('SECTIONS_SHEET');
        $meta_sheet = env('META_SHEET');

        $fb_keys = array(
          'api_key' => env('FIELDBOOK_KEY'),
          'api_secret' => env('FIELDBOOK_SECRET')
        );

        $this->fb = new PhieldBook($fb_keys);

        $this->fb->book_id = env('BOOK_ID');

        $this->locations = $this->cache($locations_sheet);
        $this->locations_page = $this->cache($locations_page_sheet);
        $this->home_page = $this->cache($home_page_sheet);
        $this->testimonials = $this->cache($testimonials_sheet);
        $this->blog = $this->cache($blog_sheet);
        $this->dreamhome = $this->cache($dreamhome_sheet);
        $this->nav = $this->cache($nav_sheet);
        $this->promotions = $this->cache($promotions_sheet);
        $this->roomtype = $this->cache($roomtype_sheet);
        $this->features = $this->cache($features_sheet);
        $this->meta = $this->cache($meta_sheet);
        $this->sections = $this->cache($sections_sheet);
        $this->team = $this->cache($team_sheet);
        $this->logo = $this->cache($logo_sheet);
        $this->logo = $this->logo[0];

        $this->fb->sheet_title = $footer_sheet;

        // rearrange key value pairs for easier use
        $this->hp = Array();
        if(count($this->home_page)) {
            foreach($this->home_page as $key=>$val) {
                $this->hp[$val['field']] = $val['value'];
            }
        }

        // rearrange key value pairs for easier use
        $this->sec = Array();
        if(count($this->sections)) {
            foreach($this->sections as $key=>$val) {
                $this->sec[$val['name']] = $val['show'];
            }
        }

        $cachingTime = Config::get('cache.tts');

        // footer top row
        if (Cache::has('top_row')) {
            $this->footer_top = response(Cache::get('top_row'));
            $this->footer_top = $this->footer_top->original;
        }
        else {
            $this->footer_top = $this->fb->search(['group' => 'top_row']);
            Cache::put('top_row', $this->footer_top, $cachingTime);
        }
        // print_r($this->footer_top);

        // footer contact
        if (Cache::has('contact')) {
            $this->footer_contact = response(Cache::get('contact'));
            $this->footer_contact = $this->footer_contact->original;
        }
        else {
            $this->footer_contact = $this->fb->search(['group' => 'contact']);
            Cache::put('contact', $this->footer_contact, $cachingTime);
        }

        // footer social
        if (Cache::has('social')) {
            $this->social = response(Cache::get('social'));
            $this->social = $this->social->original;
        }
        else {
            $this->social = $this->fb->search(['group' => 'social']);
            Cache::put('social', $this->social, $cachingTime);
        }

        // footer copyright
        if (Cache::has('copyright')) {
            $this->copyright = response(Cache::get('copyright'));
            $this->copyright = $this->copyright->original;
        }
        else {
            $this->copyright = $this->fb->search(['group' => 'copyright']);
            Cache::put('copyright', $this->copyright, $cachingTime);
        }

        // footer col1
        if (Cache::has('col1')) {
            $this->col1 = response(Cache::get('col1'));
            $this->col1 = $this->col1->original;
        }
        else {
            $this->col1 = $this->fb->search(['group' => 'col1']);
            Cache::put('col1', $this->col1, $cachingTime);
        }

        // footer col2
        if (Cache::has('col2')) {
            $this->col2 = response(Cache::get('col2'));
            $this->col2 = $this->col2->original;
        }
        else {
            $this->col2 = $this->fb->search(['group' => 'col2']);
            Cache::put('col2', $this->col2, $cachingTime);
        }

        // footer col3
        if (Cache::has('col3')) {
            $this->col3 = response(Cache::get('col3'));
            $this->col3 = $this->col3->original;
        }
        else {
            $this->col3 = $this->fb->search(['group' => 'col3']);
            Cache::put('col3', $this->col3, $cachingTime);
        }

        // footer col4
        if (Cache::has('col4')) {
            $this->col4 = response(Cache::get('col4'));
            $this->col4 = $this->col4->original;
        }
        else {
            $this->col4 = $this->fb->search(['group' => 'col4']);
            Cache::put('col4', $this->col4, $cachingTime);
        }

        // footer col_headings
        if (Cache::has('col_headings')) {
            $this->col_headings = response(Cache::get('col_headings'));
            $this->col_headings = $this->col_headings->original;
        }
        else {
            $this->col_headings = $this->fb->search(['group' => 'col_headings']);
            Cache::put('col_headings', $this->col_headings, $cachingTime);
        }
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with([
            'blog' => $this->blog,
            'dreamhome' => $this->dreamhome,
            'locations' => $this->locations,
            'locations_page' => $this->locations_page,
            'home_page' => $this->hp,
            'testimonials' => $this->testimonials,
            'nav' => $this->nav,
            'logo' => $this->logo,
            'footer_top' => $this->footer_top,
            'footer_contact' => $this->footer_contact,
            'social' => $this->social,
            'col1' => $this->col1,
            'col2' => $this->col2,
            'col3' => $this->col3,
            'col4' => $this->col4,
            'col_headings' => $this->col_headings,
            'copyright' => $this->copyright,
            'promotions' => $this->promotions,
            'roomtype' => $this->roomtype,
            'features' => $this->features,
            'meta' => $this->meta,
            'sections' => $this->sec,
            'team' => $this->team,
        ]);
    }

    public function cache($sheet_title)
    {

        $this->fb->sheet_title = $sheet_title;

        if (Cache::has($sheet_title))  //If it's cached...
            return response(Cache::get($sheet_title))->original;   //... return the value from cache and we're done.

        $response = $this->fb->get();  //If it wasn't cached, execute the request and grab the response

        $cachingTime = Config::get('cache.tts');  //Let's cache it for 60 minutes
        Cache::put($sheet_title, $response, $cachingTime);  //Cache response
    }
}