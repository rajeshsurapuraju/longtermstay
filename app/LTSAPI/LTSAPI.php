<?php
namespace App\LTSAPI;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

/**
 * LTS API
 *
 * @package     LTSAPI
 * @author      rajeshs142@gmail.com
 *
 *
 */


/*
 * @access public
 * @package LTSAPI
*/


class LTSAPI {

	public $error = array();
	private $api_timeout = '30';

	private $host;

	function __construct($config=array(),$session_id='') {

        if(empty($config)) { 
            $config= array(
				'host' => env('LTS_CORE_API_HOST', false),
				'path' => env('LTS_CORE_API_PATH', false),
				'port' => env('LTS_CORE_API_PORT', false)
			);
        }
		$this->host = $config['host'];
		$this->api_path = $config['path'];
		$this->port = $config['port'];
		$this->api_url = "http://{$this->host}:{$this->port}/{$this->api_path}";
		$this->client = new Client([
			'headers' => ['Accept' => 'application/json'],
			'connect_timeout' => 30,
			'allow_redirects' => [
		        'max'             => 2,        // allow at most 10 redirects.
		        'strict'          => true,      // use "strict" RFC compliant redirects.
		        'referer'         => true,      // add a Referer header
		        'track_redirects' => true
		    ]
		]);
		
	}


	
	/**
	 * API call via guzzle
	 *
	 * @param string $url 
	 * @param array $data post / get data
	 *
	 * @return array json parsed response array
	 */
	final private function call($url,$data=array(),$type='') {

		if($type=='post') {
			$response = $this->client->post($url, $data);
		} else {
			$response = $this->client->get($url);			
		}
		$code = $response->getStatusCode(); // 200
		
		if($code == 200) {
			$body = json_decode($response->getBody(), true);
			return $body;
		} else {
			return false;
		}
	}

	/**
	 * API GET request
	 *
	 * @param string $url 
	 * @param array $ags get data
	 *
	 * @return array json parsed response array
	 */
	final public function get($path, $data=array()) {
		if ($path == 'item') {
			$path = 'itemSearch';
		} else if ($path == 'category') {
			$path = 'cf/category';
		}
		if($data) $path .= '/?'  . http_build_query($data);
		$url = $this->api_url . '/' . $path;
		if($response = $this->call($url)) {
			return $response;
		} else {
			return false;
		}
	}


	/**
	 * API PUT request
	 *
	 * @param string $url 
	 * @param array $ags put data
	 *
	 * @return array json parsed response array
	 */
	final public function put($path,$data) {
		$url = $this->api_url . '/' . $path;
		return $this->call($url,$data,'put');
	}


	/**
	 * API DELETE request
	 *
	 * @param string $url 
	 *
	 * @return array json parsed response array
	 */
	final public function delete($path,$data) {
		$url = $this->api_url . '/' . $path;
		return $this->call($url,$data,'delete');
	}


	/**
	 * API POST
	 *
	 * @param string $path uri path
	 * @param array $data post data
	 *
	 * @return mixed 
	 */
	final public function post($path,$data) {
		$url = $this->api_url . '/' . $path;
		return $this->call($url,$data, 'post');
	}

}
?>
