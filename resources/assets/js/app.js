
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue'));

const app = new Vue({
    el: '#app'
});

/* jquery */
/* document ready */
$(function() {
	/* listings submit/select action */
	var selectBtn = $('.listings-select-btn');
	for(var i=0; i<selectBtn.length; i++) {
		$(selectBtn[i]).on('click', submitForm);
	}
	function submitForm(e) {
		var slipInpt = $('#slip');
		var listingsForm = $('#listings-form');
		slipInpt.val($(this).attr('data-slip'));
		listingsForm.submit();
	}

	/* current nav highlight */
	var nav = $('nav li');
	var url = $(location).attr('href');
	var values = Array.prototype.map.call(nav, function(obj) {
		var re = new RegExp(obj.className, 'g');
		if(url.match(re)) {
			$(obj).addClass('nav-selected');
		} else {
			$(obj).removeClass('nav-selected');
		}
	});

	/* add page class to HTML tag */
	var strings = url.split('/');
	$('html').addClass('html-'+strings[strings.length-1]);

	/* fixed header */
	var searchForm = $('.search-form');
	var body = $('body');
	if (body.hasClass('home')) {
		$(window).scroll(function() {
			var scrollTop = body.scrollTop();
			if (scrollTop > 600 && !searchForm.hasClass('fixed')) {
				searchForm.addClass('fixed');
			} else if (scrollTop < 600) {
				searchForm.removeClass('fixed');
			}
		});
	}

	/* tooltip initialization */
	$('[data-toggle="tooltip"]').tooltip({
		container: 'body'
	})

	/* search form validations */
	var startDate = $('#start-date');
	var endDate = $('#end-date');
	var oneday = 1000 * 60 * 60 * 24;
	var currentDate = new Date();
	currentDate = (currentDate.getMonth()+1)+'/'+currentDate.getDate()+'/'+currentDate.getFullYear();
	var today = new Date(currentDate);
	startDate.datepicker({
		numberOfMonths: 2,
		minDate: currentDate,
		maxDate: '+1y'
	});
	endDate.datepicker({
		numberOfMonths: 2,
		minDate: '+29d',
		maxDate: '+1y +29d'
	});
	if (!startDate.val())
		startDate.datepicker().datepicker("setDate", today);
	if (!endDate.val())
		endDate.datepicker().datepicker("setDate", "+29 days");

	var searchBtn = $('.search-btn');
	searchBtn.on('click', submitSearch);
	function submitSearch(e) {
		e.preventDefault();
		var startDateValidations = false;
		var endDateValidations = false;
		var startDateVal = startDate.val();
		var endDateVal = endDate.val();
		var startDateFormat = new Date(startDateVal);
		var endDateFormat = new Date(endDateVal);
		startDate.popover({
			content: function() {
				return this.attr('data-content');
			},
			placement: 'bottom',
			trigger: 'manual',
			container: 'body'
		});
		endDate.popover({
			content: function() {
				return this.attr('data-content');
			},
			placement: 'bottom',
			trigger: 'manual',
			container: 'body'
		});
		if(!startDateVal) {
			startDate.attr('data-content', 'Please enter Start date.');
			startDate.popover('show');
		} else {
			if (startDateFormat.getTime() < today.getTime()) {
				startDate.attr('data-content', 'Start date should be greater than or equal to Current date.');
				startDate.popover('show');
			} else {
				startDateValidations = true;
				startDate.popover('hide');
			}
		}

		if(!endDateVal) {
			endDate.attr('data-content', 'End date should be greater than Start date by 30 days.');
			endDate.popover('show');
		} else {
			if ((endDateFormat.getTime() - startDateFormat.getTime())/oneday < 30) {
				endDate.attr('data-content', 'End date should be greater than Start date by 30 days.');
				endDate.popover('show');
			} else {
				endDateValidations = true;
				endDate.popover('hide');
			}
		}

		if (startDateValidations && endDateValidations) {
			var searchForm = $('#form-search');
			searchForm.submit();
		}
	}



	/* booking form validations */
	var bookBtn = $('.btn-book');
	bookBtn.on('click', submitBooking);
	function submitBooking(e) {
		e.preventDefault();
		var mustHave = $('.must-have');
		var errors = 0;
		for (var i= 0; i< mustHave.length; i++) {
			errors += validateInput(mustHave.eq(i));
		}
		if (!$('#customer_tos_agree:checked').length) {
			errors++;
		}
		if (!errors) {
			$('.bookings-form').submit();
		}
	}

	/* payment & contact form validations */
	var mustHave = $('.must-have');
	for (var i= 0; i< mustHave.length; i++) {
		mustHave.eq(i).on('focus', function(e) {
			$(this).tooltip('show');
		});
		mustHave.eq(i).on('blur', function(e) {
			$(this).tooltip('hide');
		});
	}

	var contactBtn = $('.contact-btn');
	contactBtn.on('click', submitContact);
	function submitContact(e) {
		e.preventDefault();
		var mustHave = $('.must-have');
		var errors = 0;
		for (var i= 0; i< mustHave.length; i++) {
			errors += validateInput(mustHave.eq(i));
		}
		if (!errors) {
			$('.contact-form').submit();
		}
	}

	/* validate inputs */
	function validateInput(node) {
		var error = $('<span class="validation"><span class="fa fa-close form-control-feedback" aria-hidden="true"></span><span id="inputSuccess2Status" class="sr-only">(error)</span></span>');
		var success = $('<span class="validation"><span class="fa fa-check form-control-feedback" aria-hidden="true"></span><span id="inputSuccess2Status" class="sr-only">(success)</span></span>');
		node.siblings('.validation').remove();
		if(!node.val().trim()) {
			var err = error.clone();
			node.after(err);
			node.parent().addClass('has-error');
			return 1;
		} else {
			var suc = success.clone();
			node.after(suc);
			node.parent().addClass('has-success');
			return 0;
		}
	}
});
