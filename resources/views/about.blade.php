@extends('layouts.main')

@include('pages.meta-tags', ['meta' => $meta[8]])

@section('header')
	    @include('pages.header')
@endsection

@section('body')
<div class='heading-wrapper py-30px bg-white'>
	<h2 class='heading'>About Us</h2>
</div>
<div class='about page row'>
	<div class='row'>
		<div class="col-lg-8 col-lg-offset-2">
			<div class='content-wrapper'>
				<p>We are a Delaware corporation, operating 14 guesthouses directly managed by us in south bay area providing hospitality solutions to almost hundreds of folks working with leading companies like Apple, Google, Yahoo, Cisco, Facebook, Comcast, Motorola, HP, Samsung etc.</p>
				<p></p>
				<p>We are an established brand having strong value system, customer focus and clear policies. Our mantra is help you concentrate on your core area of business and let us handle everything else in the most professional, hassle-free and tailor-made setting, something we are best at.</p>
			</div>
		</div>
	</div>
</div>


@endsection
