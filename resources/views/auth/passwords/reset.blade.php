@extends('layouts.main')

@include('pages.meta-tags', ['meta' => $meta[1]])

@section('header')
	    @include('pages.header')
@endsection

@section('body')
<div class="text-center login-page">
	<h2 class='bg-white py-30px heading'>Reset Password</h2>
    <div class="my-30px bg-white p-30px d-inline-block" style="width:280px;">
        <form class="form-horizontal" method="POST" action="{{ route('password.request') }}">
            {{ csrf_field() }}

            <input type="hidden" name="token" value="{{ $token }}">

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <!-- <label for="email" class="col-md-4 control-label">E-Mail Address</label> -->

                <div>
                    <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" required autofocus placeholder="email">

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <!-- <label for="password" class="col-md-4 control-label">Password</label> -->

                <div>
                    <input id="password" type="password" class="form-control" name="password" required placeholder="password">

                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <!-- <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label> -->
                <div>
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="confirm password">

                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary d-block w-100">
                    Reset Password
                </button>
            </div>
        </form>
    </div>
</div>
@endsection