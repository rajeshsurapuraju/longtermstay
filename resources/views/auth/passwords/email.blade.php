@extends('layouts.main')

@include('pages.meta-tags', ['meta' => $meta[1]])

@section('header')
	    @include('pages.header')
@endsection

@section('body')
<div class="text-center login-page">
	<h2 class='bg-white py-30px heading'>Reset Password</h2>
    <div class="my-30px bg-white p-30px d-inline-block" style="width:280px;">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <!-- <label for="email" class="col-md-4 control-label">E-Mail Address</label> -->

                <div>
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required placeholder="email address">

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary d-block w-100">
                    Send Password Reset Link
                </button>
            </div>
        </form>
    </div>
</div>
@endsection