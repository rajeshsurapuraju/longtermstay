@extends('layouts.main')

@include('pages.meta-tags', ['meta' => $meta[6]])

@section('header')
	    @include('pages.header')
@endsection

@section('body')

<div class="text-center login-page">
	<h2 class='bg-white py-30px heading'> Sign In </h2>
    <div class="my-30px bg-white p-30px d-inline-block" style="width:280px;">
        
        <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}
	        <div class="br-1 pb-15px">
	            <a href="/redirect" class="w-100 d-block fb-wrapper bdrs-4px">
	                <span class="fa fa-facebook"></span>
					<span class="continue">Continue with Facebook</span>
	            </a>
	            <!-- <br /> -->
	            <!-- <a href="/redirect" class="btn btn-social btn-facebook">
	                <span class="fa fa-google-plus"></span>
	                Google Plus
	            </a>
	            <br />
	            <a href="/redirect" class="btn btn-social btn-facebook">
	                <span class="fa fa-twitter"></span>
	                Twitter
	            </a> -->
	        </div>
			<div class="pb-15px"> or </div>
            <div class="pb-15px{{ $errors->has('email') ? ' has-error' : '' }}">
                <!-- <label for="email" class="col-md-4 control-label">E-Mail Address</label> -->

                <div>
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus placeholder="email address">

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="pb-15px{{ $errors->has('password') ? ' has-error' : '' }}">
                <!-- <label for="password" class="col-md-4 control-label">Password</label> -->

                <div>
                    <input id="password" type="password" class="form-control" name="password" required placeholder="password">

                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="pb-15px">
                <div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                        </label>
                    </div>
                </div>
            </div>

            <div class="pb-15px">
                <button type="submit" class="btn btn-primary d-block w-100">
                    Sign in
                </button>
            </div>
			<div>
                <a class="btn-link" href="{{ route('password.request') }}">
                    Forgot Your Password?
                </a>
			</div>
        </form>
    </div>
</div>
@endsection
