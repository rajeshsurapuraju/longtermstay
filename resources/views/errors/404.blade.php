<!DOCTYPE html>
<html lang="en">
<!--

 _                         _____                     _____ _              
| |                       |_   _|                   /  ___| |             
| |     ___  _ __   __ _    | | ___ _ __ _ __ ___   \ `--.| |_ __ _ _   _ 
| |    / _ \| '_ \ / _` |   | |/ _ \ '__| '_ ` _ \   `--. \ __/ _` | | | |
| |___| (_) | | | | (_| |   | |  __/ |  | | | | | | /\__/ / || (_| | |_| |
\_____/\___/|_| |_|\__, |   \_/\___|_|  |_| |_| |_| \____/ \__\__,_|\__, |
                    __/ |                                            __/ |
                   |___/                                            |___/ 

-->
    <head>
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-T4DJ2TV');</script>
        <!-- End Google Tag Manager -->

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>@yield('title')</title>
        <meta name="description" content="@yield('og-description')">
        <meta property="og:title" content="@yield('og-title')">
        <meta property="og:type" content="@yield('og-type')">
        <meta property="og:image" content="@yield('og-img')">
        <meta property="og:description" content="@yield('og-description')">
        <script src="/js/jquery.min.js"></script>
        <link rel="icon" type="image/png" href="/img/logo.png">
        <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    </head>
        <body class="@yield('class_body', 'default')">
            <!-- Google Tag Manager (noscript) -->
            <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T4DJ2TV"
            height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
            <!-- End Google Tag Manager (noscript) -->
            <div class='main text-center'>
                @section('search')
                @show
                <pre>


       ___ _____    ___           ______                  _   _       _    ______                    _ 
      /   |  _  |  /   |          | ___ \                | \ | |     | |   |  ___|                  | |
     / /| | |/' | / /| |  ______  | |_/ /_ _  __ _  ___  |  \| | ___ | |_  | |_ ___  _   _ _ __   __| |
    / /_| |  /| |/ /_| | |______| |  __/ _` |/ _` |/ _ \ | . ` |/ _ \| __| |  _/ _ \| | | | '_ \ / _` |
    \___  \ |_/ /\___  |          | | | (_| | (_| |  __/ | |\  | (_) | |_  | || (_) | |_| | | | | (_| |
        |_/\___/     |_/          \_|  \__,_|\__, |\___| \_| \_/\___/ \__| \_| \___/ \__,_|_| |_|\__,_|
                                              __/ |                                                    
                                             |___/                                                     
        </pre>
        </div>
        <div class="text-center">
            <p>
                <a href="/">Go to Home page</a>
            </p>
        </div>
        <script src="/js/bootstrap.min.js"></script>
        <script src='/js/bootstrap-formhelpers.js'></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src='/js/ltstay.js'></script>
    </body>
</html>
