@extends('layouts.main')

@include('pages.meta-tags', ['title' => 'Long Term Stay - Never Miss Your Home', 'ogtype' => 'website', 'ogtitle' => 'Long Term Stay', 'ogdescription' => 'Long Term Stay,Extended stay,fully furnishedPrivate,Family,Shared Rooms,monthly lease,wifi,tv,Indian,food,kitchen,Sunnyvale,Santa Clara,Milpitas,San Jose,Bay Area'])

@section('header')
	    @include('pages.header')
@endsection

@section('body')
        @include('pages.bookhome')
@endsection
