@extends('layouts.main')

@include('pages.meta-tags', ['meta' => $meta[3]])

@section('header')
	    @include('pages.header')
@endsection

@section('body')
	    @include('pages.rooms')
@endsection