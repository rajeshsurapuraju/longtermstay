@extends('layouts.main')

@include('pages.meta-tags', ['meta' => $meta[1]])

@section('header')
	    @include('pages.header')
@endsection

@section('search')
	    @include('pages.search', ['search' => $search])
@endsection
@section('body')
	    @include('pages.search_query', ['search' => $search])
        @include('pages.listings_final')
@endsection
