<div>
<p>Hey,</p>
<div style="padding: 0px 0px 20px 20px;">
    {{$name}}({{$email}}) has sent a message.<br><br>
	<br>******************************<br>
    <div>Name: {{$name}}</div>
    <div>Email: {{$email}}</div>
    <div>Phone: {{$phone}}</div>
    <div>Preferred Area: 
	@if($loc)
		@foreach ($loc as $l)
			{{$l}} ,
		@endforeach
	@endif
	</div>
    <div>Room Type: 
	@if($room)
		@foreach ($room as $r)
			{{$r}} ,
		@endforeach
	@endif
	</div>
    <div>Flexible Room Type: {{$flexroom}}</div>
    <div>Move-in Date: {{$movein}}</div>
    <div>Flexible Move-in Date: {{$flexmovein}}</div>
    <div>Office / Business Address: {{$address}}</div>
    <div>Best time to call: {{$calltime}}</div>
    <div>Send me a mail first: {{$mailfirst}}</div>
	<br>******************************<br>
    <div style="padding: 10px 0px">
        <b>Message:</b><br>
        {{$bodyMessage}}
    </div>
</div>

Thanks,<br>
{{$name}}
</div>