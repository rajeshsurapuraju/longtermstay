@extends('layouts.main')

@include('pages.meta-tags', ['meta' => $meta[1]])

@section('header')
	    @include('pages.header')
@endsection

@section('body')
<main class="container-fluid">
	<h3 class="text-center py-30px bg-white bdrs-4px mb-10px">Almost done! We just need a few details.</h3>
	<div class="row">
		<div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3 pb-10px mb-10px">
	    @include('pages.bookhome')
		</div>
	</div>
</main>
@endsection
