@extends('layouts.main')

@include('pages.meta-tags', ['meta' => $meta[13]])

@section('header')
	    @include('pages.header')
@endsection

@section('body')
	    @include('pages.gallery', ['embedsocial' => env('EMBED_SOCIAL', false)])
@endsection
