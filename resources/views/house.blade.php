@extends('layouts.main')

@include('pages.meta-tags', ['meta' => ['title' => 'Long Term Stay - '.$name, 'ogtype' => 'website', 'ogtitle' => 'Long Term Stay - '.$name, 'keywords' => 'Long Term Stay - '.$name, 'image' => '/img/hero.jpg']])

@section('header')
	    @include('pages.header')
@endsection

@section('body')
<main class="my-3">
    @include('pages.homedetail')
</main>
@endsection
