@extends('layouts.main')

@include('pages.meta-tags', ['meta' => $meta[1]])

@section('header')
	    @include('pages.header')
@endsection

@section('body')
<main class="my-3">
    <section class="homedetail container-fluid p-0">
    	<div class="big-img text-center">
	    	        				<img src="https://longtermstay.checkfront.com/media/M226-1--8278.jpg">
	        				<img src="https://longtermstay.checkfront.com/media/M226-2--8335.jpg">
	        				<img src="https://longtermstay.checkfront.com/media/M226-3--8389.jpg">
	        				<img src="https://longtermstay.checkfront.com/media/M226-4--8439.jpg">
	        			</div>
	<div class="">
		<div class="col-md-8 col-md-offset-2">
			<div class="row">
				<div class="col-sm-8 home-detail-content">
					<div class="bg-white home-detail-section">
						<div class="head">
							<!-- <div class="text-capitalize">ctx05x12z16z17z18xlt10135xfaznab</div> -->
							<h2 class="text-capitalize">Family room</h2>
							<div class="text-capitalize mb-10px">CT2 Family Long Term</div>
					        <div class="text-capitalize summary"><p>Family option for Long Term Guests  at Cupertino's Posh location near I280's Wolfe road exit. Very near to all Cupertino offices and easy access to Palo alto, Sunnyvale and Mountain View via 280 and 85. Walking distance to the Main St and all that's Cupertino's best. This is a 7 Bed/ 3Bath house with Ample Street Parking available.</p></div>
					        <div class="text-capitalize">
								<img src="/img/logo.png" height="30px">
								<span id="walkscore-loc" data-loc="Cupertino CA" data-loc-lat="37.3247244" data-loc-lon="-122.0208301"> McLaren Pl, Cupertino 95014</span>
							</div>
						</div>
					</div>
					<div class="bg-white home-detail-section">
						<div class="row features-icons">
							<div>
								<i class="fa fa-wifi" title="Free Wi-Fi Internet"></i>
							</div>
							<div>
								<i class="fa fa-coffee" title="Breakfast items included"></i>
							</div>
							<div>
								<i class="fa fa-plane" title="Airport Pickup and Drop"></i>
							</div>
							<div>
								<i class="fa fa-tv" title="Satelite and Cable TV Included"></i>
							</div>
							<div>
								<i class="fa fa-car" title="Car parking"></i>
							</div>
							<div>
								<i class="fa fa-cutlery" title="You can cook here"></i>
							</div>
						</div>
					</div>
					<div class="bg-white home-detail-section">
						<div class="about">
							<h3 class="">About this home</h3>
							<p></p>

							<ul class="pills">
								<li>No Attached Bath</li>
								<li>Family room</li>
								<li>Cupertino CA</li>
								<li>Long term</li>
								<li>$1650 per month</li>
							</ul>
						</div>
					</div>
					<div id="map" class="mb-15px" style="height: 300px; position: relative; overflow: hidden;"><div style="height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; background-color: rgb(229, 227, 223);"><div class="gm-style" style="position: absolute; z-index: 0; left: 0px; top: 0px; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px;"><div tabindex="0" style="position: absolute; z-index: 0; left: 0px; top: 0px; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px; cursor: url(&quot;https://maps.gstatic.com/mapfiles/openhand_8_8.cur&quot;) 8 8, default;"><div style="z-index: 1; position: absolute; top: 0px; left: 0px; width: 100%; transform-origin: 0px 0px 0px; transform: matrix(1, 0, 0, 1, 0, 0);"><div style="position: absolute; left: 0px; top: 0px; z-index: 100; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: 0;"><div aria-hidden="true" style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;"><div style="width: 256px; height: 256px; position: absolute; left: 36px; top: -209px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 36px; top: 47px;"></div><div style="width: 256px; height: 256px; position: absolute; left: -220px; top: -209px;"></div><div style="width: 256px; height: 256px; position: absolute; left: -220px; top: 47px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 292px; top: -209px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 292px; top: 47px;"></div></div></div></div><div style="position: absolute; left: 0px; top: 0px; z-index: 101; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 102; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 103; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: -1;"><div aria-hidden="true" style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;"><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 36px; top: -209px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 36px; top: 47px;"><canvas draggable="false" height="256" width="256" style="user-select: none; position: absolute; left: 0px; top: 0px; height: 256px; width: 256px;"></canvas></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -220px; top: -209px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -220px; top: 47px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 292px; top: -209px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 292px; top: 47px;"></div></div></div></div><div style="position: absolute; z-index: 0; left: 0px; top: 0px;"><div style="overflow: hidden; width: 475px; height: 300px;"><img src="https://maps.googleapis.com/maps/api/js/StaticMapService.GetMapImage?1m2&amp;1i2701788&amp;2i6511057&amp;2e1&amp;3u16&amp;4m2&amp;1u475&amp;2u300&amp;5m5&amp;1e0&amp;5sen-US&amp;6sus&amp;10b1&amp;12b1&amp;token=111675" style="width: 475px; height: 300px;"></div></div><div style="position: absolute; left: 0px; top: 0px; z-index: 0;"><div aria-hidden="true" style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;"><div style="position: absolute; left: 36px; top: -209px; transition: opacity 200ms ease-out;"><img draggable="false" alt="" src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i16!2i10554!3i25433!4i256!2m3!1e0!2sm!3i403100274!3m9!2sen-US!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=67929" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: -220px; top: 47px; transition: opacity 200ms ease-out;"><img draggable="false" alt="" src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i16!2i10553!3i25434!4i256!2m3!1e0!2sm!3i403100238!3m9!2sen-US!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=125822" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 36px; top: 47px; transition: opacity 200ms ease-out;"><img draggable="false" alt="" src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i16!2i10554!3i25434!4i256!2m3!1e0!2sm!3i403100274!3m9!2sen-US!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=103950" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: -220px; top: -209px; transition: opacity 200ms ease-out;"><img draggable="false" alt="" src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i16!2i10553!3i25433!4i256!2m3!1e0!2sm!3i403100238!3m9!2sen-US!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=89801" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 292px; top: -209px; transition: opacity 200ms ease-out;"><img draggable="false" alt="" src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i16!2i10555!3i25433!4i256!2m3!1e0!2sm!3i403100298!3m9!2sen-US!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=50915" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 292px; top: 47px; transition: opacity 200ms ease-out;"><img draggable="false" alt="" src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i16!2i10555!3i25434!4i256!2m3!1e0!2sm!3i403100286!3m9!2sen-US!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=10047" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div></div></div></div><div class="gm-style-pbc" style="z-index: 2; position: absolute; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px; left: 0px; top: 0px; opacity: 0;"><p class="gm-style-pbt"></p></div><div style="z-index: 3; position: absolute; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px; left: 0px; top: 0px;"><div style="z-index: 1; position: absolute; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px; left: 0px; top: 0px;"></div></div><div style="z-index: 4; position: absolute; top: 0px; left: 0px; width: 100%; transform-origin: 0px 0px 0px; transform: matrix(1, 0, 0, 1, 0, 0);"><div style="position: absolute; left: 0px; top: 0px; z-index: 104; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 105; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 106; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 107; width: 100%;"></div></div></div><div style="margin-left: 5px; margin-right: 5px; z-index: 1000000; position: absolute; left: 0px; bottom: 0px;"><a target="_blank" href="https://maps.google.com/maps?ll=37.324724,-122.02083&amp;z=16&amp;t=m&amp;hl=en-US&amp;gl=US&amp;mapclient=apiv3" title="Click to see this area on Google Maps" style="position: static; overflow: visible; float: none; display: inline;"><div style="width: 66px; height: 26px; cursor: pointer;"><img alt="" src="https://maps.gstatic.com/mapfiles/api-3/images/google4.png" draggable="false" style="position: absolute; left: 0px; top: 0px; width: 66px; height: 26px; user-select: none; border: 0px; padding: 0px; margin: 0px;"></div></a></div><div style="background-color: white; padding: 15px 21px; border: 1px solid rgb(171, 171, 171); font-family: Roboto, Arial, sans-serif; color: rgb(34, 34, 34); box-shadow: rgba(0, 0, 0, 0.2) 0px 4px 16px; z-index: 10000002; display: none; width: 256px; height: 148px; position: absolute; left: 88px; top: 60px;"><div style="padding: 0px 0px 10px; font-size: 16px;">Map Data</div><div style="font-size: 13px;">Map data ©2017 Google</div><div style="width: 13px; height: 13px; overflow: hidden; position: absolute; opacity: 0.7; right: 12px; top: 12px; z-index: 10000; cursor: pointer;"><img alt="" src="https://maps.gstatic.com/mapfiles/api-3/images/mapcnt6.png" draggable="false" style="position: absolute; left: -2px; top: -336px; width: 59px; height: 492px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div></div><div class="gmnoprint" style="z-index: 1000001; position: absolute; right: 172px; bottom: 0px; width: 125px;"><div draggable="false" class="gm-style-cc" style="user-select: none; height: 14px; line-height: 14px;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a style="color: rgb(68, 68, 68); text-decoration: none; cursor: pointer; display: none;">Map Data</a><span>Map data ©2017 Google</span></div></div></div><div class="gmnoscreen" style="position: absolute; right: 0px; bottom: 0px;"><div style="font-family: Roboto, Arial, sans-serif; font-size: 11px; color: rgb(68, 68, 68); direction: ltr; text-align: right; background-color: rgb(245, 245, 245);">Map data ©2017 Google</div></div><div class="gmnoprint gm-style-cc" draggable="false" style="z-index: 1000001; user-select: none; height: 14px; line-height: 14px; position: absolute; right: 99px; bottom: 0px;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a href="https://www.google.com/intl/en-US_US/help/terms_maps.html" target="_blank" style="text-decoration: none; cursor: pointer; color: rgb(68, 68, 68);">Terms of Use</a></div></div><button draggable="false" title="Toggle fullscreen view" aria-label="Toggle fullscreen view" type="button" style="background: none; border: 0px; margin: 10px 14px; padding: 0px; position: absolute; cursor: pointer; user-select: none; width: 25px; height: 25px; overflow: hidden; top: 0px; right: 0px;"><img alt="" src="https://maps.gstatic.com/mapfiles/api-3/images/sv9.png" draggable="false" class="gm-fullscreen-control" style="position: absolute; left: -52px; top: -86px; width: 164px; height: 175px; user-select: none; border: 0px; padding: 0px; margin: 0px;"></button><div draggable="false" class="gm-style-cc" style="user-select: none; height: 14px; line-height: 14px; position: absolute; right: 0px; bottom: 0px;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a target="_new" title="Report errors in the road map or imagery to Google" href="https://www.google.com/maps/@37.3247244,-122.0208301,16z/data=!10m1!1e1!12b1?source=apiv3&amp;rapsrc=apiv3" style="font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); text-decoration: none; position: relative;">Report a map error</a></div></div><div class="gmnoprint gm-bundled-control gm-bundled-control-on-bottom" draggable="false" controlwidth="28" controlheight="93" style="margin: 10px; user-select: none; position: absolute; bottom: 107px; right: 28px;"><div class="gmnoprint" controlwidth="28" controlheight="55" style="position: absolute; left: 0px; top: 38px;"><div draggable="false" style="user-select: none; box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 4px -1px; border-radius: 2px; cursor: pointer; background-color: rgb(255, 255, 255); width: 28px; height: 55px;"><button draggable="false" title="Zoom in" aria-label="Zoom in" type="button" style="background: none; display: block; border: 0px; margin: 0px; padding: 0px; position: relative; cursor: pointer; user-select: none; width: 28px; height: 27px; top: 0px; left: 0px;"><div style="overflow: hidden; position: absolute; width: 15px; height: 15px; left: 7px; top: 6px;"><img alt="" src="https://maps.gstatic.com/mapfiles/api-3/images/tmapctrl.png" draggable="false" style="position: absolute; left: 0px; top: 0px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 120px; height: 54px;"></div></button><div style="position: relative; overflow: hidden; width: 67%; height: 1px; left: 16%; background-color: rgb(230, 230, 230); top: 0px;"></div><button draggable="false" title="Zoom out" aria-label="Zoom out" type="button" style="background: none; display: block; border: 0px; margin: 0px; padding: 0px; position: relative; cursor: pointer; user-select: none; width: 28px; height: 27px; top: 0px; left: 0px;"><div style="overflow: hidden; position: absolute; width: 15px; height: 15px; left: 7px; top: 6px;"><img alt="" src="https://maps.gstatic.com/mapfiles/api-3/images/tmapctrl.png" draggable="false" style="position: absolute; left: 0px; top: -15px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 120px; height: 54px;"></div></button></div></div><div class="gm-svpc" controlwidth="28" controlheight="28" style="background-color: rgb(255, 255, 255); box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 4px -1px; border-radius: 2px; width: 28px; height: 28px; cursor: url(&quot;https://maps.gstatic.com/mapfiles/openhand_8_8.cur&quot;) 8 8, default; position: absolute; left: 0px; top: 0px;"><div style="position: absolute; left: 1px; top: 1px;"></div><div style="position: absolute; left: 1px; top: 1px;"><div aria-label="Street View Pegman Control" style="width: 26px; height: 26px; overflow: hidden; position: absolute; left: 0px; top: 0px;"><img alt="" src="https://maps.gstatic.com/mapfiles/api-3/images/cb_scout5.png" draggable="false" style="position: absolute; left: -147px; top: -26px; width: 215px; height: 835px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div aria-label="Pegman is on top of the Map" style="width: 26px; height: 26px; overflow: hidden; position: absolute; left: 0px; top: 0px; visibility: hidden;"><img alt="" src="https://maps.gstatic.com/mapfiles/api-3/images/cb_scout5.png" draggable="false" style="position: absolute; left: -147px; top: -52px; width: 215px; height: 835px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div aria-label="Street View Pegman Control" style="width: 26px; height: 26px; overflow: hidden; position: absolute; left: 0px; top: 0px; visibility: hidden;"><img alt="" src="https://maps.gstatic.com/mapfiles/api-3/images/cb_scout5.png" draggable="false" style="position: absolute; left: -147px; top: -78px; width: 215px; height: 835px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div></div></div><div class="gmnoprint" controlwidth="28" controlheight="0" style="display: none; position: absolute;"><div title="Rotate map 90 degrees" style="width: 28px; height: 28px; overflow: hidden; position: absolute; background-color: rgb(255, 255, 255); box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 4px -1px; border-radius: 2px; cursor: pointer; display: none;"><img alt="" src="https://maps.gstatic.com/mapfiles/api-3/images/tmapctrl4.png" draggable="false" style="position: absolute; left: -141px; top: 6px; width: 170px; height: 54px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div class="gm-tilt" style="width: 28px; height: 28px; overflow: hidden; position: absolute; background-color: rgb(255, 255, 255); box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 4px -1px; border-radius: 2px; top: 0px; cursor: pointer;"><img alt="" src="https://maps.gstatic.com/mapfiles/api-3/images/tmapctrl4.png" draggable="false" style="position: absolute; left: -141px; top: -13px; width: 170px; height: 54px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div></div></div><div class="gmnoprint" style="margin: 10px; z-index: 0; position: absolute; cursor: pointer; left: 0px; top: 0px;"><div class="gm-style-mtc" style="float: left; position: relative;"><div role="button" tabindex="0" title="Show street map" aria-label="Show street map" aria-pressed="true" draggable="false" style="direction: ltr; overflow: hidden; text-align: center; position: relative; color: rgb(0, 0, 0); font-family: Roboto, Arial, sans-serif; user-select: none; font-size: 11px; background-color: rgb(255, 255, 255); padding: 8px; border-bottom-left-radius: 2px; border-top-left-radius: 2px; -webkit-background-clip: padding-box; background-clip: padding-box; box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 4px -1px; min-width: 22px; font-weight: 500;">Map</div><div style="background-color: white; z-index: -1; padding: 2px; border-bottom-left-radius: 2px; border-bottom-right-radius: 2px; box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 4px -1px; position: absolute; left: 0px; top: 29px; text-align: left; display: none;"><div draggable="false" title="Show street map with terrain" style="color: rgb(0, 0, 0); font-family: Roboto, Arial, sans-serif; user-select: none; font-size: 11px; background-color: rgb(255, 255, 255); padding: 6px 8px 6px 6px; direction: ltr; text-align: left; white-space: nowrap;"><span role="checkbox" style="box-sizing: border-box; position: relative; line-height: 0; font-size: 0px; margin: 0px 5px 0px 0px; display: inline-block; background-color: rgb(255, 255, 255); border: 1px solid rgb(198, 198, 198); border-radius: 1px; width: 13px; height: 13px; vertical-align: middle;"><div style="position: absolute; left: 1px; top: -2px; width: 13px; height: 11px; overflow: hidden; display: none;"><img alt="" src="https://maps.gstatic.com/mapfiles/mv/imgs8.png" draggable="false" style="position: absolute; left: -52px; top: -44px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 68px; height: 67px;"></div></span><label style="vertical-align: middle; cursor: pointer;">Terrain</label></div></div></div><div class="gm-style-mtc" style="float: left; position: relative;"><div role="button" tabindex="0" title="Show satellite imagery" aria-label="Show satellite imagery" aria-pressed="false" draggable="false" style="direction: ltr; overflow: hidden; text-align: center; position: relative; color: rgb(86, 86, 86); font-family: Roboto, Arial, sans-serif; user-select: none; font-size: 11px; background-color: rgb(255, 255, 255); padding: 8px; border-bottom-right-radius: 2px; border-top-right-radius: 2px; -webkit-background-clip: padding-box; background-clip: padding-box; box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 4px -1px; min-width: 41px; border-left: 0px;">Satellite</div><div style="background-color: white; z-index: -1; padding: 2px; border-bottom-left-radius: 2px; border-bottom-right-radius: 2px; box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 4px -1px; position: absolute; right: 0px; top: 29px; text-align: left; display: none;"><div draggable="false" title="Show imagery with street names" style="color: rgb(0, 0, 0); font-family: Roboto, Arial, sans-serif; user-select: none; font-size: 11px; background-color: rgb(255, 255, 255); padding: 6px 8px 6px 6px; direction: ltr; text-align: left; white-space: nowrap;"><span role="checkbox" style="box-sizing: border-box; position: relative; line-height: 0; font-size: 0px; margin: 0px 5px 0px 0px; display: inline-block; background-color: rgb(255, 255, 255); border: 1px solid rgb(198, 198, 198); border-radius: 1px; width: 13px; height: 13px; vertical-align: middle;"><div style="position: absolute; left: 1px; top: -2px; width: 13px; height: 11px; overflow: hidden;"><img alt="" src="https://maps.gstatic.com/mapfiles/mv/imgs8.png" draggable="false" style="position: absolute; left: -52px; top: -44px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 68px; height: 67px;"></div></span><label style="vertical-align: middle; cursor: pointer;">Labels</label></div></div></div></div></div></div></div>
				    <script>
				    	function initMap() {
							var lat = document.getElementById('walkscore-loc').getAttribute('data-loc-lat') || '37.384846';
							var lon = document.getElementById('walkscore-loc').getAttribute('data-loc-lon') || '122.028988';
					        var ltstay = {lat: parseFloat(lat), lng: parseFloat(lon)};
							console.log(ltstay);
					        var map = new google.maps.Map(document.getElementById('map'), {
					          zoom: 16,
					          center: ltstay
					        });
					        var marker = new google.maps.Marker({
					          position: ltstay,
					          map: map
					        });
				      	}
				    </script>
				    <script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDuR_14tBhYtA30Nd8c6CXY3ADTf4Wt2tE&amp;callback=initMap"></script>
					<div class="bg-white home-detail-section">
						<h3 class="">Features of this home</h3>
						<div class="row features">
							<div class="col-sm-6">
								<div>
									<i class="fa fa-check-circle-o" aria-hidden="true"></i>
									Free Wi-Fi Internet inside rooms
								</div>
								<div>
									<i class="fa fa-check-circle-o" aria-hidden="true"></i>
									Breakfast Items included
								</div>
								<div>
									<i class="fa fa-check-circle-o" aria-hidden="true"></i>
									Airport Pickup and Drop
								</div>
								<div>
									<i class="fa fa-check-circle-o" aria-hidden="true"></i>
									Satelite and Cable TV Included
								</div>
								<div>
									<i class="fa fa-check-circle-o" aria-hidden="true"></i>
									Appliances
								</div>
							</div>
							<div class="col-sm-6">
								<div>
									<i class="fa fa-check-circle-o" aria-hidden="true"></i>
									Room Cleaning Services
								</div>
								<div>
									<i class="fa fa-check-circle-o" aria-hidden="true"></i>
									Beautiful view on the city
								</div>
								<div>
									<i class="fa fa-check-circle-o" aria-hidden="true"></i>
									Free parking on Driveway, Street
								</div>
								<div>
									<i class="fa fa-check-circle-o" aria-hidden="true"></i>
									Fan / Air Conditioner
								</div>
								<div>
									<i class="fa fa-check-circle-o" aria-hidden="true"></i>
									Refrigerator
								</div>
							</div>
						</div>
					</div>
					<div class="bg-white home-detail-section">
						<h3 class="">Policies</h3>
						<div class="policies">
							<div>
								<i class="fa fa-check-circle-o" aria-hidden="true"></i>
								No Smoking in the room.
							</div>
							<div>
								<i class="fa fa-check-circle-o" aria-hidden="true"></i>
								Pets are not allowed
							</div>
							<div>
								<i class="fa fa-check-circle-o" aria-hidden="true"></i>
								Cancellation and prepayment policies vary according to room type. Please email to us to learn more about it.
							</div>
							<div>
								<i class="fa fa-check-circle-o" aria-hidden="true"></i>
								When booking more than 5 rooms, different policies and additional supplements may apply.
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4 home-detail-book">
					<div class="bg-white home-detail-section">
			  			<h3 class="pb-10px">Book</h3>
			            <form role="search" method="POST" action="/book/226">
			                <div class="form-group row">
			                    <label for="delivery-address" class="control-label col-md-4 py-2 m-0 pr-0 text-d4d4d4">Price</label>
			                    <div class="col-md-8 pl-md-0">
			                        <label for="delivery-address" class="control-label py-2 m-0 p-0"><b>$1,650.00</b></label>
			            			<input type="hidden" name="slip[]" id="slip" value="226.20171213X30-1">
			                        <input type="hidden" name="_token" value="X6wDxzDXRYZ3mWXYrN9gAWTXSjujQTdKs0STlZLs">
			                        <input name="_method" type="hidden" value="POST">
                                            
			                    </div>
			                </div>
			                <!-- <div class="form-group row">
			                    <label for="months" class="control-label col-md-4 py-2 m-0 pr-0"># of months</label>
			                    <div class="col-md-8 pl-md-0">
			                        <input id="months" name="months" type="text" class="form-control">
			                    </div>
			                </div> -->
			                <div class="form-group row">
			                    <label for="startdate" class="control-label col-md-4 py-2 m-0 pr-0 text-d4d4d4">Start Date</label>
			                    <div class="col-md-8 pl-md-0">
                                    <!-- <div class="mt-2">12/13/2017</div> -->
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="startdate" class="control-label col-md-4 py-2 m-0 pr-0 text-d4d4d4">Duration</label>
			                    <div class="col-md-8 pl-md-0">
			                        <div class="mt-2">30 days</div>
			                    </div>
			                </div>
			                <!-- <div class="form-group row">
			                    <label for="delivery-address" class="control-label col-md-4 py-2 m-0 pr-0 text-d4d4d4">Delivery Address</label>
			                    <div class="col-md-8 pl-md-0">
			                        <input id="delivery-address" name="delivery-address" type="text" class="form-control">
			                    </div>
			                </div> -->
			                <div class="form-group row">
			                    <label for="delivery-address" class="control-label col-md-4 py-2 m-0 pr-0 text-d4d4d4">Monthly Price</label>
			                    <div class="col-md-8 pl-md-0">
			                        <label for="delivery-address" class="control-label py-2 m-0 p-0">
										<b>$1,650.00</b>
										<div><i class="small-90" style="font-weight: normal;"> recurring monthly till stay ends</i></div>
									</label>
			                    </div>
			                </div>
			                <div class="">
			                    <button id="search-btn-nav-top" type="submit" class="btn btn-primary col-8" style="margin-left: 4px;" disabled>
			                        Sold out
			                    </button>
			                    <!-- <a class="btn btn-primary col-8" href='/book/226'>Book now</a> -->
			                </div>
			            </form>
					</div>
					<div class="bg-white home-detail-section d-none">
						<h3>Specials</h3>
						<div>Get 10% off for first month on this home.</div>
						<div>Book within next 1 hour</div>
					</div>
					<div class="bg-white home-detail-section d-none">
						<h3>Busy? Need a reminder?</h3>
						<div>
							<form class="d-inline-block">
								<input type="email" placeholder="email address" style="width:100%;" class="mb-10px form-control">
								Remind me in 
								<input type="number" value="3" style="width:50px;" class="mb-10px text-center form-control d-inline-block" min="1" max="30">
								days
								<input type="submit" value="Remind me!" class="btn btn-success d-block">
							</form>
						</div>
					</div>
					<div class="bg-white home-detail-section">
						<h3>Check whats nearby</h3>
						<script type="text/javascript">
							var ws_wsid = '285166fd12b6b003e9d2bf2432ece66f';
							var ws_address = document.getElementById('walkscore-loc').getAttribute('data-loc');
							var ws_format = 'tall';
							var ws_width = '100%';
							var ws_height = '350';
						</script>
						<style type="text/css">#ws-walkscore-tile{position:relative;text-align:left}#ws-walkscore-tile *{float:none;}</style>
						<div id="ws-walkscore-tile" style="position: relative; text-align: left; text-decoration: none; padding: 0px; font-stretch: normal; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; word-spacing: normal; text-transform: none; vertical-align: baseline; text-indent: 0px; text-shadow: none; white-space: normal; background-image: none; background-color: transparent; height: 350px; width: 100%;"><iframe marginheight="0" marginwidth="0" height="350px" frameborder="0" scrolling="no" width="100%" style="margin: 0px; outline: none; text-align: left; text-decoration: none; padding: 0px; font-stretch: normal; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; word-spacing: normal; text-transform: none; vertical-align: baseline; text-indent: 0px; text-shadow: none; white-space: normal; background-image: none; background-color: transparent; border: 0px;" src="//www.walkscore.com/serve-walkscore-tile.php?wsid=285166fd12b6b003e9d2bf2432ece66f&amp;s=Cupertino-CA&amp;o=h&amp;c=f&amp;h=350&amp;fh=0&amp;w=163"></iframe></div>
						<script type="text/javascript" src="/js/walkscore.js"></script>						
					</div>
					<div class="bg-white home-detail-section">
						<h3>Need a ride?</h3>
						<div class="mb-10px">Avail our friendly partner services for</div>
						<ul>
							<li>pick and drop to airport</li>
							<li>shopping around town</li>
							<li>pick and drop to office</li>
							<li>running errands and more</li>
						</ul>
						<div class="small-90" style="padding: 10px 0;"><i>additional rates apply</i></div>
					</div>
				</div>
			</div>
		</div>
	</div>
    </section></main>@endsection
