@extends('layouts.main')

@include('pages.meta-tags', ['meta' => $meta[4]])

@section('header')
	    @include('pages.header')
@endsection

@section('body')
	    @include('pages.amenities_final')
@endsection
