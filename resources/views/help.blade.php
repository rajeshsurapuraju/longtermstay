@extends('layouts.main')

@include('pages.meta-tags', ['meta' => $meta[12]])

@section('header')
	@include('pages.header')
@endsection

@section('body')
	@include('pages.help')
@endsection
