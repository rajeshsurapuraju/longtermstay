<div class="testimonials section">
	<div class="heading-wrapper row">
		<h2 class="heading">What people say about Long Term Stay</h2>
		<div class="heading-subtext">Read what people like you think about their experience with Long Term Stay</div>
	</div>
	<div class="content-wrapper row text-center px-5">
		@if($testimonials)
			@foreach($testimonials as $testi)
				@if($loop->index > 2)
					@break
				@endif
				<div class="col-xs-12 col-sm-4 mb-10px">
					<div class="testi-wrap">
						<div class="pb-10px">
							@if($testi['image'])
								<img src="{{$testi['image']}}">
							@else
								<i class="fa fa-user"></i>
							@endif
						</div>
						<b>
							{{$testi['name']}}
						</b>
						<div class="pb-10px">{{$testi['company']}}</div>
						<div>
							<i>{{$testi['description']}}</i>
						</div>
					</div>
				</div>
			@endforeach
		@endif
	</div>
	<div class="btn-wrapper">
		<a href="{{ url('/testimonials') }}" class="btn btn-primary btn-lg">Read All Reviews →</a>
	</div>
</div>
