<div class="search-form">
    <form action="/listings" class="form-inline" role="form" id="form-search" method="get" accept-charset="utf-8">
        <div class="form-group">
            <label class="search-label" for="inputStartDate">Start Date 
                <span class="required">*</span>
            </label>
            <div>
                <input name="start_date" value="{{$search['start_date']}}" class="form-control start_date " id="start-date" placeholder="mm/dd/yyyy" type="text" data-provider="datepicker">
            </div>
        </div>
        <div class="form-group">
            <label class="search-label" for="inputEndDate">End Date 
                <span class="required">*</span>
            </label>
            <div>
                <input name="end_date" value="{{$search['end_date']}}" class="form-control end_date " id="end-date" placeholder="mm/dd/yyyy" type="text" data-provider="datepicker">
            </div>
        </div>
        <div class="form-group">
            <label class="search-label" for="inputLocation">Location</label>
            <div>
                @if ($locations)
                    <select id="location" name="location" class="form-control">
                        @foreach($locations as $key=>$loc)
                            @if($loc['code'] == $search['location'])
                            <option value='{{$loc['code']}}' selected>{{$loc['location_name']}}</option>
                            @else
                            <option value='{{$loc['code']}}'>{{$loc['location_name']}}</option>
                            @endif
                        @endforeach
                    </select>
                @else    
                    <input name="location" value="<?=$location?>" class="form-control" placeholder="ex: Sunnyvale" type="text">
                @endif
                
            </div>
        </div>
        <div class="form-group">
            <div>
                <input name="search" value="Search" class="btn btn-primary search-btn" type="submit">
            </div>
        </div>
    </form>
</div>