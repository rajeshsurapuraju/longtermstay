<div class='container-fluid p-0'>
	<div class='rooms page p-0'>
		<div class='heading-wrapper text-white position-relative'>
			<div class="position-absolute">
				<h2 class='heading'> Rooms that suit you. </h2>
				<div class=''>Choose between Private, Shared and Family rooms.</div>
			</div>
		</div>
		<div class="content-wrapper text-center py-30px container-fluid">
			<div class="row p-30px py-4percent">
				<div class="col-lg-8 col-lg-offset-2">
					<div class="row">
						<div class="col-xs-12 col-sm-4">
							<div class="pb-20px">
								<img src="https://s3-us-west-1.amazonaws.com/longtermstay/longtermstay_single.jpg" alt="ltstay private room">
							</div>
						</div>
						<div class="col-xs-12 col-sm-8">
							<h3 class="bb-1d mb-10px">
								Private Rooms
							</h3>
							<div>
								<div>Beds: 1 Bed, Executive Chair, Tables</div>
								<div>
									<span>Max person:</span>
									<i class="fa fa-user"></i>
								</div>
							</div>
							<div class="btn-wrapper">
								<a href="{{ url('/rooms/private') }}" class="btn bd-s-1">Explore Private Room</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row p-30px bg-white py-4percent">
				<div class="col-lg-8 col-lg-offset-2">
					<div class="row">
						<div class="col-xs-12 col-sm-4">
							<div class="pb-20px">
								<img src="https://s3-us-west-1.amazonaws.com/longtermstay/longtermstay_shared.jpg" alt="ltstay shared room">
							</div>
						</div>
						<div class="col-xs-12 col-sm-8">
							<h3 class="bb-1d mb-10px">
								Shared Rooms
							</h3>
							<div>
								<div>Beds: 2 Single Bed, Table and Exeuctive Chairs</div>
								<div>
									<span>Max person:</span>
									<i class="fa fa-user"></i>
									<i class="fa fa-user"></i>
								</div>
							</div>
							<div class="btn-wrapper">
								<a href="{{ url('/rooms/shared') }}" class="btn bd-s-1">Explore Shared Room</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row p-30px py-4percent">
				<div class="col-lg-8 col-lg-offset-2">
					<div class="row">
						<div class="col-xs-12 col-sm-4">
							<div class="pb-20px">
								<img src="https://s3-us-west-1.amazonaws.com/longtermstay/longtermstay_family.png" alt="ltstay family room">
							</div>
						</div>
						<div class="col-xs-12 col-sm-8">
							<h3 class="bb-1d mb-10px">
								Family Rooms
							</h3>
							<div>
								<div>Beds: 1 Queen Bed, Kid Beds</div>
								<div>
									<span>Max person:</span>
									<i class="fa fa-male"></i>
									<i class="fa fa-female"></i>
									<i class="fa fa-child"></i>
								</div>
							</div>
							<div class="btn-wrapper">
								<a href="{{ url('/rooms/family') }}" class="btn bd-s-1">Explore Family Room</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
