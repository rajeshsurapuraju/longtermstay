<?php
$start_date = date('m/d/Y'); 
$end_date = date('m/d/Y', strtotime('+30 days'));
?>
<div class='locations section bg-white p-0'> 
    <div class='heading-wrapper py-30px'>
        <h2 class='heading'>Locations</h2>
        <div class='heading-subtext'>Currently we are present at following locations, but we are growing into other cities. Stay tuned!!!</div>
    </div>
    <div class='content-wrapper row'>
        <div class="col-sm-12 col-md-6">
            <div class='location-wrapper row' style="padding-left: 10px !important;">
                @if($locations_page)
                    @foreach($locations_page as $item)
                        <div class="col-xs-{{$item['width']}} mb-10px pl-5px pr-5px">
                            <a class='location-link bdrs-4px h-264px h-sm-170px' style="background-image:url({{$item['image']}}); background-position-y: 72%;" href="/search_validation?start_date={{$start_date}}&end_date={{$end_date}}&location={{$item['search_code']}}">
                                <h2>{{$item['city']}}</h2>
                            </a>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
        <div class="col-sm-12 col-md-6">
            <iframe src="https://snazzymaps.com/embed/29381" width="100%" height="100vh" style="border:none; height: 100vh;"></iframe>
        </div>
    </div>
</div>