<div class="rooms section bg-white">
    <div class="heading-wrapper">
        <h2 class="heading">{{$home_page['rooms_heading']}}</h2>
        <div class='heading-subtext'>{{$home_page['rooms_subtxt']}}</div>
    </div>
    <div class="content-wrapper row text-center">
        <div class="col-md-10 col-md-offset-1">
            <div class="row">
                <div class="col-xs-12 col-sm-4 mb-30px">
                    <div>
                        <img src="{{$home_page['rooms_private_image']}}" class="bd-r-tr8px"  alt="ltstay private room">
                    </div>
                    <div class="py-20px bg-f9f9f9 bd-r-bl8px">
                        <h3 class="bb-1d mb-10px">
                            <a href="{{$home_page['rooms_private_url']}}" class="heading">{{$home_page['rooms_private_txt']}}</a>
                        </h3>
                        <div>
                            <div>Beds: 1 Bed <br>Table and Exeuctive Chairs</div>
                            <div>
                                <span>Max person:</span>
                                <i class="fa fa-user"></i>
                            </div>
                        </div>
                        <div class="btn-wrapper">
                            <a href="{{ url($home_page['rooms_private_url']) }}" class="btn btn-primary btn-lg btn-red">Explore Private Room</a>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 mb-30px">
                    <div>
                        <img src="{{$home_page['rooms_shared_image']}}" class="bd-r-tr8px" alt="ltstay shared room">
                    </div>
                    <div class="py-20px bg-f9f9f9 bd-r-bl8px">
                        <h3 class="bb-1d mb-10px">
                            <a href="{{$home_page['rooms_shared_url']}}" class="heading">{{$home_page['rooms_shared_txt']}}</a>
                        </h3>
                        <div>
                            <div>Beds: 2 Single Bed <br> Table and Exeuctive Chairs</div>
                            <div>
                                <span>Max person:</span>
                                <i class="fa fa-user"></i>
                                <i class="fa fa-user"></i>
                            </div>
                        </div>
                        <div class="btn-wrapper">
                            <a href="{{ url($home_page['rooms_shared_url']) }}" class="btn btn-primary btn-lg btn-red">Explore Shared Room</a>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 mb-30px">
                    <div>
                        <img src="{{$home_page['rooms_family_image']}}" class="bd-r-tr8px" alt="ltstay family room">
                    </div>
                    <div class="py-20px bg-f9f9f9 bd-r-bl8px">
                        <h3 class="bb-1d mb-10px">
                            <a href="{{$home_page['rooms_family_url']}}" class="heading">{{$home_page['rooms_family_txt']}}</a>
                        </h3>
                        <div>
                            <div>Beds: 1 Queen Bed, Kid Beds</div>
                            <div>
                                <span>Max person:</span>
                                <i class="fa fa-male"></i>
                                <i class="fa fa-female"></i>
                                <i class="fa fa-child"></i>
                            </div>
                            <br>
                        </div>
                        <div class="btn-wrapper">
                            <a href="{{ url($home_page['rooms_family_url']) }}" class="btn btn-primary btn-lg btn-red">Explore Family Room</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="text-center">
        <a href="{{ url('/rooms') }}">Explore All Rooms →</a>
    </div> -->
</div>
