<div class="blog section bg-f9f9f9">
    <div class="heading-wrapper">
        <h2 class="heading">{{$home_page['blog_heading']}}</h2>
        <div class="heading-subtext" style="">{{$home_page['blog_subtxt']}}</div>
    </div>
    <div class="content-wrapper row text-center">
        <div>
        @if($blog)
            @foreach($blog as $item)
                @if($loop->index > 5)
                    @break
                @endif
                <div class="col-xs-12 col-sm-6 col-md-4 mb-10px">
                    <div class="blog-wrap">
                        <div class="pb-20px">
                            <img src="{{$item['image']}}" class="bdrs-4px" height="164px" alt="{{$item['title']}}">
                        </div>
                        <h3>
                            <a href="{{$item['url']}}" class="heading">{{$item['title']}}</a>
                        </h3>
                        <div>{{date('d M Y', strtotime($item['date']))}}</div>
                    </div>
                </div>
            @endforeach
        @endif
        </div>
    </div>
    <div class="btn-wrapper">
        <a href="{{ url($home_page['blog_url']) }}" class="btn btn-primary btn-lg">Visit Blog →</a>
    </div>
</div>
