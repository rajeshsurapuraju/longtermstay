<nav class='navbar navbar-default' role='navigation'>
    <div class='container-fluid'>
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class='navbar-header'>
              <button type='button' class='navbar-toggle collapsed' data-toggle='collapse' data-target='#bs-example-navbar-collapse-1' aria-expanded='false'>
                <span class='sr-only'>Toggle navigation</span>
                <span class='icon-bar'></span>
                <span class='icon-bar'></span>
                <span class='icon-bar'></span>
            </button>
            <h1 class='reset'>
                <a class="navbar-brand" href="/">
                    <img
                    alt='LTStay'
                    src="https://s3-us-west-1.amazonaws.com/longtermstay/logo_header_caption.png"
                    style="
                        height: 56px;
                        display: inline;
                    ">
                    <!-- <img src="/img/logotxt.png" style="
                        height: 25px;
                        display: inline;
                    "> -->
                </a>
            </h1>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class='collapse navbar-collapse' id='bs-example-navbar-collapse-1'>
              <ul class='nav navbar-nav navbar-right'>
                @foreach($nav as $navItem)
                <li class='home'>
                    <a href="{{$navItem['link']}}"><span class='nav-item'>{{$navItem['name']}}</span></a>
                </li>
                @endforeach
                @if (Auth::guest())
                    <li class='home'><a href="{{ url('/login') }}"><span class='nav-item'>Sign in</span></a></li>
                    <li class='home'><a href="{{ url('/register') }}"><span class='nav-item'>Sign up</span></a></li>
                @else
                    <li class="home dropdown">
                        <a class="dropdown-toggle" type="button" id="userMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class='nav-item'>{{ Auth::user()->name }}</span>
                        </a>
                        <!-- <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                             <span class="caret"></span>
                        </a> -->

                        <div class="dropdown-menu" aria-labelledby="userMenu">
                            <!-- <a class="dropdown-item d-block nav-item" href="{{ url('/settings') }}">Settings</a> -->
                            <!-- <a class="dropdown-item"  href="{{ url('/logout') }}">Logout</a> -->
                            <a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="dropdown-item d-block nav-item">
                                Logout
                            </a>
                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </li>
                @endif
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
