<div class='footer container-fluid'>
	<div class='footer-cols container'>
		<div class="row">
			<ul class='col-xs-12 col-sm-3'>
				<li><h5>{{$col_headings[0]['name']}}</h5></li>
				@foreach($col1 as $col1Item)
					<li><a href='{{$col1Item['link']}}'>{{$col1Item['name']}}</a></li>
				@endforeach
			</ul>
			<ul class='col-xs-12 col-sm-3'>
				<li><h5>{{$col_headings[1]['name']}}</h5></li>
				@foreach($col2 as $col2Item)
					<li><a href='{{$col2Item['link']}}'>{{$col2Item['name']}}</a></li>
				@endforeach
			</ul>
			<ul class='col-xs-12 col-sm-3'>
				<li><h5>{{$col_headings[2]['name']}}</h5></li>
				@foreach($col3 as $col3Item)
					<li><a href='{{$col3Item['link']}}'>{{$col3Item['name']}}</a></li>
				@endforeach
			</ul>
			<ul class='col-xs-12 col-sm-3'>
				<li><h5>{{$col_headings[3]['name']}}</h5></li>
				@foreach($col4 as $col4Item)
					<li><a href='{{$col4Item['link']}}'>{{$col4Item['name']}}</a></li>
				@endforeach
			</ul>
		</div>
	</div>
	<div>
		<ul class='footer-about'>
			@foreach($footer_top as $footerTopItem)
				<li class='footer-about-item'><a href='{{$footerTopItem['link']}}'>{{$footerTopItem['name']}}</a></li>
			@endforeach
		</ul>
	</div>
	<div class='footer-social'>
		@foreach($social as $socialItem)
			<span>
				<a href='{{$socialItem['link']}}'>
					<i class="fa fa-{{$socialItem['icon']}}" aria-hidden="true"></i>
				</a>
			</span>
		@endforeach
	</div>
	<div class='footer-contact py-20px'>
			<ul>
				@foreach($footer_contact as $footerContactItem)
					<li><a href='{{$footerContactItem['link']}}'>{{$footerContactItem['name']}}</a></li>
				@endforeach
			</ul>
	</div>
	<div class='copyright py-20px'>
		{{$copyright[0]['name']}}
	</div>
</div>
