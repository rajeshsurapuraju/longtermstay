<section>
    @if(isset($car["rate"]["slip"]))
	<h4 class="mb-3 text-center text-capitalize">{{$car["name"]}}</h4>
    <div class="row py-md-3">
        <div class="col-md-4 text-center p-md-0 push-md-8">
  			<img src="https://{{env("CF_HOST")}}{{$car["img"]}}" width="100%" height="auto">
            <div class="pt-3">
                <button type="button" class="btn-link" data-toggle="modal" data-target="#gallery">View Gallery</button>
            </div>
  		</div>
  		<div class="col-md-8 pt-3 pt-md-0 pull-md-4">
  		    <p>{!! $car["summary"] !!}</p>
            <p>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
            </p>
            <!-- <p>
                <span>Price</span>
                <b>{{$car["rate"]["summary"]["price"]["total"]}}</b>
            </p> -->
            <p class="fw-600">
                <i class="small">Available at</i>
                <span class="fw-600 text-capitalize">{{$locationsConfig[$car["loc"]]}}</span>
                <i class="small">for</i>
                {{$car["rate"]["summary"]["price"]["total"]}}
                <i class="small">a month</i>
            </p>
            <p class="invisible-md more-info">
                <span class="d-inline-block small mt-1 mt-sm-0 tags">Sedan</span>
                <span class="d-inline-block small mt-1 mt-sm-0 tags">Electric</span>
                <span class="d-inline-block small mt-1 mt-sm-0 tags">Automatic</span>
                <span class="d-inline-block small mt-1 mt-sm-0 tags">2 door</span>
                <span class="d-inline-block small mt-1 mt-sm-0 tags">4 seats</span>
            </p>
            <ul class="p-0 feedback">
                <li class="text-black pr-3 d-inline-block">
                    Share on
                </li>
                <li class="text-black pr-3 d-inline-block">
                    <a href="http://facebook.com/" class="text-black">
                        <i class="icon mr-2 fa fa-facebook"></i>
                    </a>
                </li>
                <li class="text-black pr-3 d-inline-block">
                    <a href="http://twitter.com/" class="text-black">
                        <i class="icon mr-2 fa fa-twitter"></i>
                    </a>
                </li>
                <li class="text-black pr-3 d-inline-block">
                    <a href="http://plus.google.com/" class="text-black">
                        <i class="icon mr-2 fa fa-google-plus"></i>
                    </a>
                </li>
                <li class="text-black d-inline-block">
                    <a href="mailto:support@carseasy.com" class="text-black">
                        <i class="icon mr-2 fa fa-envelope"></i>
                    </a>
                </li>
            </ul>
  		</div>
  	</div>
    <div class="row py-3">
        <div class="col-md-8 details">
            {!! $car["details"] !!}
  		</div>
  		<div class="col-md-4 py-3 book b-1 bg-search text-white">
  			<h5 class="mb-3 text-center">Book</h5>
            <form role="search" method="POST" action="/book/{{$car["item_id"]}}">
                <div class="form-group row">
                    <label for="delivery-address" class="control-label col-md-4 py-2 m-0 pr-0 text-d4d4d4">Price</label>
                    <div class="col-md-8 pl-md-0">
                        <label for="delivery-address" class="control-label py-2 m-0 p-0"><b>{{$car["rate"]["summary"]["price"]["total"]}}</b></label>
            			<input type='hidden' name='slip[]' id='slip'/ value="{{$car["rate"]["slip"]}}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input name="_method" type="hidden" value="POST">
                                                
                    </div>
                </div>
                <!-- <div class="form-group row">
                    <label for="months" class="control-label col-md-4 py-2 m-0 pr-0"># of months</label>
                    <div class="col-md-8 pl-md-0">
                        <input id="months" name="months" type="text" class="form-control">
                    </div>
                </div> -->
                <div class="form-group row">
                    <label for="startdate" class="control-label col-md-4 py-2 m-0 pr-0 text-d4d4d4">Start Date</label>
                    <div class="col-md-8 pl-md-0">
                        <div class="mt-2">{{$inputs['start_date']}}</div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="startdate" class="control-label col-md-4 py-2 m-0 pr-0 text-d4d4d4">Duration</label>
                    <div class="col-md-8 pl-md-0">
                        <div class="mt-2">30 days</div>
                    </div>
                </div>
                <!-- <div class="form-group row">
                    <label for="delivery-address" class="control-label col-md-4 py-2 m-0 pr-0 text-d4d4d4">Delivery Address</label>
                    <div class="col-md-8 pl-md-0">
                        <input id="delivery-address" name="delivery-address" type="text" class="form-control">
                    </div>
                </div> -->
                <div class="form-group row">
                    <label for="delivery-address" class="control-label col-md-4 py-2 m-0 pr-0 text-d4d4d4">Total Price</label>
                    <div class="col-md-8 pl-md-0">
                        <label for="delivery-address" class="control-label py-2 m-0 p-0"><b>{{$car["rate"]["summary"]["price"]["total"]}}</b></label>
                    </div>
                </div>
                <div class="text-center">
                    <button id="search-btn-nav-top" type="submit" class="btn btn-primary col-8" style="margin-left: 4px;">
                        Book now
                    </button>
                    <!-- <a class="btn btn-primary col-8" href='/book/{{$car["item_id"]}}'>Book now</a> -->
                </div>
            </form>
  		</div>
  	</div>
    <div class="py-3">
        <h5 class="fw-400 text-center bb-1 py-3 mb-3">Similar Deals</h5>
        <div class="row">
            @foreach ($items as $itm)
            <div class="col-md-3">
        		<div class="text-center">
        			<img src="https://{{env("CF_HOST")}}{{$itm['img']}}" style="height: auto; width:100%;">
        		</div>
        		<div class="pt-3">
        			<a href="/car/{{$itm["item_id"]}}">
            			<h5 class="mb-1 text-capitalize">{{$itm['name']}}</h5>
                    </a>
                    <div class="fw-600">
                        <div class="fw-600 text-capitalize">{{$locationsConfig[$itm["loc"]]}}</div>
                        <div class="fw-600">
                            {{$itm["rate"]["summary"]["price"]["total"]}}
                            <i class="small">a month</i>
                        </div>
                    </div>
                    <!-- <b>{{$itm["rate"]["summary"]["price"]["total"]}}</b> -->
                    <p>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                    </p>
        		</div>
        	</div>
            @endforeach
        </div>
    </div>
    <div class="modal fade" id="gallery" tabindex="-1" role="dialog" aria-labelledby="galleryLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="modal-body">
                    <section id="carousel" class="carousel slide p-0" data-ride="carousel">
                        <div class="carousel-inner" role="listbox">
                            @foreach ($car["image"] as $img)
                            <div class="carousel-item justify-content-center">
                    			<img src="https://{{env("CF_HOST")}}{{$img['path']}}">
                            </div>
                            @endforeach
                        </div>
                        <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                        <ol class="carousel-indicators mt-4">
                            @foreach ($car["image"] as $img)
                            <li data-target="#carousel" data-slide-to="{{$loop->index}}" class="mx-3">
                            </li>
                            @endforeach
                        </ol>
                    </section>
                    <script>
                        $($('.carousel-item')[0]).addClass('active');
                        $($('.carousel-indicators li')[0]).addClass('active');
                    </script>
                </div>
            </div>
        </div>
    </div>
    @else
        <div class="text-center py-5">
            <div class="pb-4"> This car is no longer available. </div>
            <a href="{{$homecarouselConfig['action']['link']}}" class="action-btn">{{$homecarouselConfig['action']['txt']}}</a>
        </div>
    @endif
</section>