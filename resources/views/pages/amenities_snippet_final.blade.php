<div class="amenities section bg-f9f9f9">
    <div class="heading-wrapper">
        <h2 class="heading">{{$home_page['amenities_heading']}}</h2>
        <div class="heading-subtext">{{$home_page['amenities_subtxt']}}</div>
    </div>
    <div class="content-wrapper row">
        <div class="col-xs-12 col-sm-6 col-md-3 mb-30px">
            <div class="p-20px bg-white bd-r-8px bd-075 text-center">
                <i class="fa fa-bed fa-lg fz-12rem d-none text-center my-50px" aria-hidden="true"></i>
                <div class="fz-12rem d-block text-center my-50px" aria-hidden="true">
                    <?xml version="1.0" encoding="utf-8"?>
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="-70 40 260 80" style="enable-background:new -70 40 260 80;" xml:space="preserve">
                    <style type="text/css">
                        .st0{fill:#010101;}
                    </style>
                    <g>
                        <path class="st0" d="M104.3,76.1h-89c-0.8,0-1.5-0.7-1.5-1.5V49.5c0-4,3.2-7.2,7.2-7.2h77.7c4,0,7.2,3.2,7.2,7.2v25.1
                            C105.8,75.4,105.1,76.1,104.3,76.1z M16.7,73.1h86V49.5c0-2.3-1.9-4.2-4.2-4.2H20.9c-2.3,0-4.2,1.9-4.2,4.2V73.1z"/>
                        <path class="st0" d="M113.7,92.6H5.8c-0.8,0-1.5-0.7-1.5-1.5V80.2c0-4,3.2-7.2,7.2-7.2H108c4,0,7.2,3.2,7.2,7.2v10.9
                            C115.2,92,114.6,92.6,113.7,92.6z M7.3,89.6h105v-9.4c0-2.3-1.9-4.2-4.2-4.2H11.4c-2.3,0-4.2,1.9-4.2,4.2V89.6z"/>
                        <path class="st0" d="M53.4,76.1H23.8c-0.8,0-1.5-0.7-1.5-1.5V61.3c0-2.5,2-4.5,4.5-4.5h23.7c2.5,0,4.5,2,4.5,4.5v13.3
                            C54.9,75.4,54.3,76.1,53.4,76.1z M25.3,73.1h26.7V61.3c0-0.8-0.7-1.5-1.5-1.5H26.8c-0.8,0-1.5,0.7-1.5,1.5V73.1z"/>
                        <path class="st0" d="M95.7,76.1H66.1c-0.8,0-1.5-0.7-1.5-1.5V61.3c0-2.5,2-4.5,4.5-4.5h23.7c2.5,0,4.5,2,4.5,4.5v13.3
                            C97.2,75.4,96.6,76.1,95.7,76.1z M67.6,73.1h26.7V61.3c0-0.8-0.7-1.5-1.5-1.5H69.1c-0.8,0-1.5,0.7-1.5,1.5V73.1z"/>
                        <path class="st0" d="M113.7,102.3H5.8c-0.8,0-1.5-0.7-1.5-1.5v-9.6c0-0.8,0.7-1.5,1.5-1.5h108c0.8,0,1.5,0.7,1.5,1.5v9.6
                            C115.2,101.6,114.6,102.3,113.7,102.3z M7.3,99.3h105v-6.6H7.3V99.3z"/>
                        <path class="st0" d="M12.3,117H5.8c-0.8,0-1.5-0.7-1.5-1.5v-14.7c0-0.8,0.7-1.5,1.5-1.5h6.5c0.8,0,1.5,0.7,1.5,1.5v14.7
                            C13.8,116.3,13.1,117,12.3,117z M7.3,114h3.5v-11.7H7.3V114z"/>
                        <path class="st0" d="M113.7,117h-6.5c-0.8,0-1.5-0.7-1.5-1.5v-14.7c0-0.8,0.7-1.5,1.5-1.5h6.5c0.8,0,1.5,0.7,1.5,1.5v14.7
                            C115.2,116.3,114.6,117,113.7,117z M108.7,114h3.5v-11.7h-3.5V114z"/>
                    </g>
                    </svg>
                </div>
                <h3 class="text-center">
                    Comfort
                </h3>
                <span>We provide comfortable <i>beds</i>, pillows, blankets and sanitary needs for every customer. We know our customers, so we have a comfortable office desk and chair for every tenant. Your comfort is our priority.</span>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3 mb-30px">
            <div class="p-20px bg-white bd-r-8px bd-075 text-center">
                <i class="fa fa-play-circle-o fa-lg fz-12rem d-none text-center my-50px" aria-hidden="true"></i>
                <div class="fz-12rem d-block text-center my-50px" aria-hidden="true">
                    <?xml version="1.0" ?>
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="-80 0 220 67.5" style="enable-background:new -80 0 220 67.5;" xml:space="preserve">
                        <g>
                            <path d="M32,5c14.888,0,27,12.112,27,27S46.888,59,32,59S5,46.888,5,32S17.112,5,32,5 M32,2C15.433,2,2,15.432,2,32  c0,16.568,13.433,30,30,30s30-13.432,30-30C62,15.432,48.567,2,32,2L32,2z"/><path d="M43.987,28.745L28.013,18.232C25.256,16.418,23,17.635,23,20.935V43.06c0,3.3,2.259,4.521,5.021,2.714l15.959-10.444  C46.741,33.521,46.744,30.559,43.987,28.745z M41.858,33.124l-14.185,9.283C26.753,43.01,26,42.603,26,41.503V22.491  c0-1.1,0.752-1.505,1.671-0.9l14.19,9.338C42.78,31.533,42.779,32.521,41.858,33.124z"/>
                        </g>
                    </svg>
                </div>
                <h3 class="text-center">
                    Work-Fun
                </h3>
                <span>We have super fast <i>broadband</i>. Be it either office work or home entertainment, we got you covered. We have Indian channels, along with Netflix. Super comfortable sofas and big flat screen tv at every residence.</span>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3 mb-30px">
            <div class="p-20px bg-white bd-r-8px bd-075 text-center">
                <i class="fa fa-cutlery fa-lg fz-12rem d-none text-center my-50px" aria-hidden="true"></i>
                <div class="fz-12rem d-block text-center my-50px" aria-hidden="true">
                    <?xml version="1.0" ?>
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="-1000 0 3000 920" style="enable-background:new -1000 0 3000 920;" xml:space="preserve">
                        <g><path d="M642.9,990c-11.3,0-20.4-9.1-20.4-20.4V30.4c0-11.3,9.1-20.4,20.4-20.4c11.3,0,20.4,9.1,20.4,20.4v939.2C663.3,980.9,654.2,990,642.9,990z"/><path d="M765.4,642.9H642.9c-11.3,0-20.4-9.1-20.4-20.4s9.1-20.4,20.4-20.4h102c-2.7-343-73.2-544.3-102.4-551.3c-11.3,0-20.2-9.1-20.2-20.4c0-11.3,9.3-20.4,20.6-20.4c85.5,0,142.9,316.7,142.9,612.5C785.8,633.8,776.7,642.9,765.4,642.9z"/><path d="M357.1,377.5c-78.8,0-142.9-64.1-142.9-142.9V30.4c0-11.3,9.1-20.4,20.4-20.4c11.3,0,20.4,9.1,20.4,20.4v204.2c0,56.3,45.8,102.1,102.1,102.1c56.3,0,102.1-45.8,102.1-102.1V30.4c0-11.3,9.1-20.4,20.4-20.4c11.3,0,20.4,9.1,20.4,20.4v204.2C500,313.4,435.9,377.5,357.1,377.5z"/><path d="M357.1,990c-11.3,0-20.4-9.1-20.4-20.4V30.4c0-11.3,9.1-20.4,20.4-20.4c11.3,0,20.4,9.1,20.4,20.4v939.2C377.5,980.9,368.4,990,357.1,990z"/></g>
                    </svg>
                </div>
                <h3 class="text-center">
                    Dining
                </h3>
                <span>Dont have time to cook or eat outside? Want to go <i>Healthy</i>? On a Diet or want to gain weight? We provide high quality home made food for lunch and dinner. Still hesitant? We deliver it at your office during weekdays.</span>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3 mb-30px">
            <div class="p-20px bg-white bd-r-8px bd-075 text-center">
                <i class="fa fa-home fa-lg fz-12rem d-none text-center my-50px" aria-hidden="true"></i>
                <div class="fz-12rem d-block text-center my-50px" aria-hidden="true">
                    <?xml version="1.0" ?>
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="-50 2 150 46" style="enable-background:new -50 2 150 46;" xml:space="preserve">
                        <g>
                            <path d="M42,48H28V35h-8v13H6V27c0-0.552,0.447-1,1-1s1,0.448,1,1v19h10V33h12v13h10V28c0-0.552,0.447-1,1-1s1,0.448,1,1V48z"/></g><g><path d="M47,27c-0.249,0-0.497-0.092-0.691-0.277L24,5.384L1.691,26.723c-0.399,0.381-1.032,0.368-1.414-0.031     c-0.382-0.399-0.367-1.032,0.031-1.414L24,2.616l23.691,22.661c0.398,0.382,0.413,1.015,0.031,1.414     C47.526,26.896,47.264,27,47,27z"/></g><g><path d="M39,15c-0.553,0-1-0.448-1-1V8h-6c-0.553,0-1-0.448-1-1s0.447-1,1-1h8v8C40,14.552,39.553,15,39,15z"/>
                        </g>
                    </svg>
                </div>
                <h3 class="text-center">
                    Home
                </h3>
                    <span>Feel right at home with fully stocked <i>kitchen</i>. Want to cook at weekends? No problem. In house washer-dryer, iron board and all amenities to get ready after a relaxed weekend. Never miss your home.</span> 
            </div>
        </div>
    </div>
    <div class="text-center">
        <a href="{{ url($home_page['amenities_url']) }}" class="btn btn-primary btn-lg">Explore Amenities →</a>
    </div>
</div>
