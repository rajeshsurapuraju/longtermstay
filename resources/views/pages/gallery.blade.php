<div class='gallery section bg-white'>
	<div class="heading-wrapper row">
		<h2 class="heading">Gallery</h2>
		<div class="heading-subtext">Take a tour of your future place of stay.</div>
	</div>
	<div class="content-wrapper px-5">
		<ul class="nav nav-tabs" id="myTab" role="tablist">
		  <li class="nav-item active">
		    <a class="nav-link" id="images-tab" data-toggle="tab" href="#images" role="tab" aria-controls="images" aria-selected="true">Images</a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link" id="albums-tab" data-toggle="tab" href="#albums" role="tab" aria-controls="albums" aria-selected="false">Home Albums</a>
		  </li>
		</ul>
		<div class="tab-content py-30px" id="myTabContent">
		  <div class="tab-pane fade active in" id="images" role="tabpanel" aria-labelledby="images-tab">
			  <div class='embedsocial-instagram' data-ref="da4ea52bc4c9f11f7339b0cd1d355a6bc1fac899"></div><script>(function(d, s, id){var js; if (d.getElementById(id)) {return;} js = d.createElement(s); js.id = id; js.src = "https://embedsocial.com/embedscript/in.js"; d.getElementsByTagName("head")[0].appendChild(js);}(document, "script", "EmbedSocialInstagramScript"));</script>			  
		  </div>
		  <div class="tab-pane fade" id="albums" role="tabpanel" aria-labelledby="albums-tab">
			  <div class='embedsocial-gallery' data-ref="c03776161e28d967abfce1eca14ad60144e51f01"></div><script>(function(d, s, id){var js; if (d.getElementById(id)) {return;} js = d.createElement(s); js.id = id; js.src = "https://embedsocial.com/embedscript/bi.js"; d.getElementsByTagName("head")[0].appendChild(js);}(document, "script", "EmbedSocialGalleryScript"));</script>
		  </div>
		</div>
	</div>
</div>
