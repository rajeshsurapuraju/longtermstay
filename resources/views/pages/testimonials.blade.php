<div class="testimonials section">
	<div class="heading-wrapper row">
		<h2 class="heading">What people say about Long Term Stay</h2>
		<div class="heading-subtext">Read what people like you think about their experience with Long Term Stay</div>
	</div>
	<div class="content-wrapper px-5">
		@if($embedsocial)
			<div class='embedsocial-reviews' data-ref="eb490aea1b66a6ba4f95737f459c3626091ba58f"></div><script>(function(d, s, id){var js; if (d.getElementById(id)) {return;} js = d.createElement(s); js.id = id; js.src = "https://embedsocial.com/embedscript/ri.js"; d.getElementsByTagName("head")[0].appendChild(js);}(document, "script", "EmbedSocialReviewsScript"));</script>
		@elseif($testimonials)
			@foreach($testimonials as $testi)
				<div class="row mb-10px testi-wrap">
					@if($loop->index % 2)
						<div class="col-sm-2 text-right">
							@if($testi['image'])
								<img src="{{$testi['image']}}">
							@else
								<i class="fa fa-user"></i>
							@endif
						</div>
						<div class="col-sm-10 text-left">
							<div class="pb-10px">
								<i>{{$testi['description']}}</i>
							</div>
							<div>
							@if($testi['rating'])
								@for ($i = 0; $i < $testi['rating']; $i++)
									<i class="fa fa-star"></i>
								@endfor
							@endif
							</div>
							<div><b>{{$testi['name']}}</b></div>
							<div>{{$testi['company']}}</div>
						</div>
					@else
						<div class="col-sm-10 text-right">
							<div class="pb-10px">
								<i>{{$testi['description']}}</i>
							</div>
							<div>
							@if($testi['rating'])
								@for ($i = 0; $i < $testi['rating']; $i++)
									<i class="fa fa-star"></i>
								@endfor
							@endif
							</div>
							<div><b>{{$testi['name']}}</b></div>
							<div>{{$testi['company']}}</div>
						</div>
						<div class="col-sm-2 text-left">
							@if($testi['image'])
								<img src="{{$testi['image']}}">
							@else
								<i class="fa fa-user"></i>
							@endif
						</div>
					@endif
				</div>
			@endforeach
		@endif
	</div>
</div>

