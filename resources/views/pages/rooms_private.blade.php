<div class='heading-wrapper bg-white py-30px'>
	<h2 class='heading'>Private Rooms</h2>
</div>
<div class='rooms page'>
	<div class='content-wrapper container-fluid'>
		<div class="row">
			<div class="col-md-8 col-md-offset-2">		
				<div class="row pb-20px mb-20px">
					<div class="col-xs-12 col-sm-6 col-lg-5 text-center">
						<div class="bg-white">
							<img src="https://s3-us-west-1.amazonaws.com/longtermstay/private1.jpg" alt="ltstay private room">
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-lg-7">
						<p class="bb-1d pb-20px">
							Lease from <b>$1350 per month</b>
						</p>
						<div class="bb-1d pb-20px mb-10px">
							<div>
								Max person to be booked: 1
							</div>
							<div>
								Room bed: 1 Bed, Executive Chair, Tables
							</div>
						</div>
						<p class="bb-1d pb-20px">
							Enjoy your privacy with friends, just at an arm's length. Private rooms come full furnished designed with your comfort in mind. 
							Baths are cleaned every week and offer you the freshness to start...
						</p>
					</div>
				</div>
				<div>
					<ul class="nav nav-tabs" id="myTab" role="tablist">
						<li class="nav-item active">
							<a class="nav-link" id="desc-tab" data-toggle="tab" href="#desc" role="tab" aria-controls="desc" aria-selected="true">Description</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="features-tab" data-toggle="tab" href="#features" role="tab" aria-controls="features" aria-selected="false">Features</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="policies-tab" data-toggle="tab" href="#policies" role="tab" aria-controls="policies" aria-selected="false">Policies</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="images-tab" data-toggle="tab" href="#images" role="tab" aria-controls="images" aria-selected="false">Images</a>
						</li>
					</ul>
					<div class="tab-content py-30px" id="myTabContent">
						<div class="tab-pane fade active in" id="desc" role="tabpanel" aria-labelledby="desc-tab">
							<div>
							<p><strong>Private Room short term: Save an average of $200 per month*</strong></p>
							<p>Light, modern and functional. The single executive rooms are designed for comfort and relaxation. The bathroom will be shared with hot &amp; cold rain shower and complimentary toiletries as standard. With&nbsp;AC and central heating&nbsp;in most homes, our rooms have ample closet space and luggage rack for your personal belongings, a desk and executive chair and all furnishings for your convenience. Missing anything.. Just ask us</p>
							<p>&nbsp;</p>
							<hr>
							<p><strong>Let’s do some&nbsp;maths</strong></p>
							<p><strong><em>First the Actual Data&nbsp;</em></strong><br>
							LTS lease rent for Private room is (six months minimum) is $1350.<br>
							LTS rent for Private room short term (month on month) is $1500.</p>
							<p>Source<br>
							<a href="http://www.numbeo.com/cost-of-living/compare_cities.jsp?country1=United+States&amp;country2=United+States&amp;city1=Sunnyvale%2C+CA&amp;city2=San+Jose%2C+CA&amp;tracking=getDispatchComparison" onclick="__gaTracker('send', 'event', 'outbound-article', 'http://www.numbeo.com/cost-of-living/compare_cities.jsp?country1=United+States&amp;country2=United+States&amp;city1=Sunnyvale%2C+CA&amp;city2=San+Jose%2C+CA&amp;tracking=getDispatchComparison', 'Numbeo');">Numbeo</a><br>
							<del>Data for Private room short term not available or not gathered.</del><br>
							<em>Rent Per Month</em><br>
							<em> Apartment (1 bedroom) in City Centre at annual lease starts at 2,195.00 $ and averages up to 2,482.68 $ &nbsp;with an increase of +13.11 %.&nbsp;</em></p>
							<p><em>Whereas Apartment (1 bedroom) Outside of Centre at annual lease starts at 2,021.25 $ and averages up to 2,138.94 $ with an increase of +5.82 %</em></p>
							<p><a href="https://www.rentjungle.com/average-rent-in-santa-clara-rent-trends" onclick="__gaTracker('send', 'event', 'outbound-article', 'https://www.rentjungle.com/average-rent-in-santa-clara-rent-trends', 'RentJungle');">RentJungle</a><br>
							<del>Data for Private room short term not available or not gathered.</del></p>
							<p><em>As of July 2016, average apartment rent within the city of of Santa Clara, CA is $3078.&nbsp;</em><em>One bedroom apartments in Santa Clara now&nbsp;rents for $2666 a month on an average while&nbsp;two bedroom apartment rents for an average of &nbsp;$3434 a month.</em></p>
							<p><em><strong>and now the details &nbsp;</strong></em></p>
							<blockquote><p>Assume we get a GRAND deal of 2 BHK for 2600. You still spend around $1500 just to make it a living without furnishing*</p></blockquote>
							<ul>
							<li>Your cost per room on an annual lease is $1300.</li>
							<li>Your utilities (internet/Electricity only/Water) per room $100</li>
							<li>Your average monthly spend on all consumables (check your credit card averages) $100</li>
							</ul>
							<blockquote><p>Now add another $50 for furnished rooms and your total now is $1550 per month</p></blockquote>
							<p><em><strong>Take the weekly cleaning and all unlimited freebies on us. Enjoy to the fullest and do share how you will spend the extra 2.5 grands you save each year staying in a private room @ Long Term Stay.</strong></em></p>
							<blockquote><p>Comparing with private rooms on Sulekha or Craigslist. Please allow us just $150 for all the complementary amenities and check out rooms below $1350. You may&nbsp;find very few less than $1200; that too, mostly a leftover room in a family home where the writ of the family is what you will have to adjust to.</p>
							<p>With us you are dealing with a process oriented company and not a person and every customer is more important than any of our own people. We know the reason we are here is to serve and delight you.</p></blockquote>
							<p><em>Need receipts to claims for your stay. Hey!! did we say we are a company and have served customer from more than 200 clients in bay area already.</em></p>
							</div>
						</div>
						<div class="tab-pane fade" id="features" role="tabpanel" aria-labelledby="features-tab">
							<div class="row">
								<div class="col-sm-6">
									<div>
										<i class="fa fa-star" aria-hidden="true"></i>
										Free Wi-Fi Internet inside rooms
									</div>
									<div>
										<i class="fa fa-star" aria-hidden="true"></i>
										Breakfast Items included
									</div>
									<div>
										<i class="fa fa-star" aria-hidden="true"></i>
										Airport Pickup and Drop
									</div>
									<div>
										<i class="fa fa-star" aria-hidden="true"></i>
										Satelite and Cable TV Included
									</div>
									<div>
										<i class="fa fa-star" aria-hidden="true"></i>
										Appliances
									</div>
								</div>
								<div class="col-sm-6">
									<div>
										<i class="fa fa-star" aria-hidden="true"></i>
										Room Cleaning Services
									</div>
									<div>
										<i class="fa fa-star" aria-hidden="true"></i>
										Beautiful view on the city
									</div>
									<div>
										<i class="fa fa-star" aria-hidden="true"></i>
										Free parking on Driveway, Street
									</div>
									<div>
										<i class="fa fa-star" aria-hidden="true"></i>
										Air Conditioning
									</div>
									<div>
										<i class="fa fa-star" aria-hidden="true"></i>
										Refrigerator
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane fade" id="policies" role="tabpanel" aria-labelledby="policies-tab">
							<div>
								<i class="fa fa-star" aria-hidden="true"></i>
								No Smoking in the room.
							</div>
							<div>
								<i class="fa fa-star" aria-hidden="true"></i>
								Pets are not allowed
							</div>
							<div>
								<i class="fa fa-star" aria-hidden="true"></i>
								Cancellation and prepayment policies vary according to room type. Please email to us to learn more about it.
							</div>
							<div>
								<i class="fa fa-star" aria-hidden="true"></i>
								When booking more than 5 rooms, different policies and additional supplements may apply.
							</div>
						</div>
						<div class="tab-pane fade" id="images" role="tabpanel" aria-labelledby="images-tab">
							<div class='embedsocial-album' data-ref="27a62ec73c995efb1ed38131b2cc92c4dde0fb22"></div><script>(function(d, s, id){var js; if (d.getElementById(id)) {return;} js = d.createElement(s); js.id = id; js.src = "https://embedsocial.com/embedscript/ei.js"; d.getElementsByTagName("head")[0].appendChild(js);}(document, "script", "EmbedSocialScript"));</script>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>