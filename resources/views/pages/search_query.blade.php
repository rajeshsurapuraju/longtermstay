

<div class="search-query">
	<div>You searched for</div>
	<div>
			<span>Start Date: </span>
			<span><b>{{$search['start_date']}}</b></span>
			<span>, End Date: </span>
			<span><b>{{$search['end_date']}}</b></span>
			<span>, Location: </span>
				@if ($locations)
                    @foreach($locations as $key=>$loc)
						@if($loc['code'] == $search['location'])
						<span><b>{{$loc['location_name']}}</b></span>
						@endif
                    @endforeach
				@endif
	</div>
</div>