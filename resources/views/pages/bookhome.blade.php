<section>
    <div class="">
    	<div class="">
    		<div class="bg-white mb-10px" style="border-radius: 4px;padding: 30px;">
				<div class="row">
					<div class="col-sm-4">
		    			<img src="https://{{env("CHECKFRONT_HOST")}}{{$item["img"]}}" width="100%" height="auto">
					</div>
					<div class="p-4 bg-search col-sm-8">
	    				<h3 class="pb-3" style="color: #ed2227;padding-bottom: 15px;">{{$item["name"]}}</h3>
	        			<div class="pb-10px">
	                        <div class="" style="color: hsla(0,3%,59%,.82);">{{$item["loc"]}}</div>
					    </div>
						<div class="pb-10px row">
							<div class="col-sm-4">
								<div class="small text-d4d4d4 fw-500">Start Date</div>
								<div class="" style="font-weight: 600;font-size: 15px;">{{$inputs["start_date"]}}</div>
							</div>
		                    <div class="col-sm-4">
		                        <div class="small text-d4d4d4 fw-500">End Date</div>
		                        <div class="" style="font-weight: 600;font-size: 15px;">{{$inputs["user_end_date"]}}</div>
		                    </div>
							<div class="col-sm-4">
		                        <div class="small text-d4d4d4 fw-500">Billing Duration</div>
		                        <div class="" style="font-weight: 600;font-size: 15px;">30 days</div>
		                    </div>
						</div>
	        			<div class="pb-10px">
	                        <div class="small text-d4d4d4 fw-500">Price per month</div>
	                        <div class="" style="font-weight: 600;font-size: 15px;">{{$item["rate"]["summary"]["price"]["total"]}}</div>
							<div>
								<i class="small">You will be billed for first month now and will be charged the same every month</i>
							</div>
	                    </div>
		    			<div class="">
		    				<a href="/listings" class="small">Clear Cart</a>
		    			</div>
	    			</div>
				</div>
    		</div>
    		<div class="bg-white py-30px">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">		
		                <form action="/book/validation/{{$item["item_id"]}}" class="form-horizontal bookings-form" method="post" accept-charset="utf-8">
		                    <div class="book-guest small-90">
		                        <div class="form-group row{{ $errors->has('customer_name') ? ' has-error' : '' }}">
		                            <label for="customer_name" class="control-label">Full Name <span class="required">*</span></label>
		                            <div class="">
		                                <input name="customer_name" type="text" class="form-control must-have" id="customer_name" placeholder="Full Name" value="{{ old('customer_name') }}" data-toggle="tooltip" data-placement="right" title="" data-original-title="please enter name">
		                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
		                                <input name="_method" type="hidden" value="POST">
		                                @if ($errors->has('customer_name'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('customer_name') }}</strong>
		                                    </span>
		                                @endif
		    						</div>
		    					</div>
		    					<div class="form-group row{{ $errors->has('customer_email') ? ' has-error' : '' }}">
		    						<label for="customer_email" class="control-label">Email <span class="required">*</span></label>
		    						<div class="">
		    							<input name="customer_email" type="email" class="form-control must-have" id="customer_email" placeholder="Email" value="{{ old('customer_email') }}" data-toggle="tooltip" data-placement="right" title="" data-original-title="please enter email">
		                                @if ($errors->has('customer_email'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('customer_email') }}</strong>
		                                    </span>
		                                @endif
		    						</div>
		    					</div>
		    					<div class="form-group row{{ $errors->has('customer_phone') ? ' has-error' : '' }}">
		    						<label for="customer_phone" class="control-label">Phone number <span class="required">*</span></label>
		    						<div class="">
		    							<input name="customer_phone" type="tel" class="form-control must-have" id="customer_phone" placeholder="Phone number" value="{{ old('customer_phone') }}" data-toggle="tooltip" data-placement="right" title="" data-original-title="please enter phone number">
		                                @if ($errors->has('customer_phone'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('customer_phone') }}</strong>
		                                    </span>
		                                @endif
		    						</div>
		    					</div>
		    					<div class="form-group row{{ $errors->has('cust_comp_name') ? ' has-error' : '' }}">
		    						<label for="cust_comp_name" class="control-label">Company <span class="required">*</span></label>
		    						<div class="">
		    							<input name="cust_comp_name" type="text" class="form-control must-have" id="cust_comp_name" placeholder="Company" value="{{ old('cust_comp_name') }}" data-toggle="tooltip" data-placement="right" title="" data-original-title="please enter company name">
		                                @if ($errors->has('cust_comp_name'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('cust_comp_name') }}</strong>
		                                    </span>
		                                @endif
		    						</div>
		    					</div>
		    					<div class="form-group row{{ $errors->has('cust_fb') ? ' has-error' : '' }}">
		    						<label for="cust_fb" class="control-label">Facebook Id <span class="required">*</span></label>
		    						<div class="">
		    							<input name="cust_fb" type="text" class="form-control must-have" id="cust_fb" placeholder="Facebook ID" value="{{ old('cust_fb') }}" data-toggle="tooltip" data-placement="right" title="" data-original-title="please enter Facebook ID">
		                                @if ($errors->has('cust_fb'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('cust_fb') }}</strong>
		                                    </span>
		                                @endif
		    						</div>
		    					</div>
		    					<div class="form-group row{{ $errors->has('customer_address') ? ' has-error' : '' }}">
		    						<label for="customer_address" class="control-label">Address <span class="required">*</span></label>
		    						<div class="">
		    							<input name="customer_address" type="text" class="form-control must-have" id="customer_address" placeholder="Address" value="{{ old('customer_address') }}" data-toggle="tooltip" data-placement="right" title="" data-original-title="please enter address">
		                                @if ($errors->has('customer_address'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('customer_address') }}</strong>
		                                    </span>
		                                @endif
		    						</div>
		    					</div>
		    					<div class="form-group row{{ $errors->has('customer_city') ? ' has-error' : '' }}">
		    						<label for="customer_city" class="control-label">City <span class="required">*</span></label>
		    						<div class="">
		    							<input name="customer_city" type="text" class="form-control must-have" id="customer_city" placeholder="City" value="{{ old('customer_city') }}" data-toggle="tooltip" data-placement="right" title="" data-original-title="please enter city">
		                                @if ($errors->has('customer_city'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('customer_city') }}</strong>
		                                    </span>
		                                @endif
		    						</div>
		    					</div>
		    					<div class="form-group row{{ $errors->has('customer_country') ? ' has-error' : '' }}">
		    						<label for="customer_country" class="control-label">Country <span class="required">*</span></label>
		    						<div class="">
		    							<select name="customer_country" id="customer_country" class="form-control must-have bfh-countries" data-country="US">
		                                    <option value="{{ old('customer_country') }}">{{ old('customer_country') }}</option>
		                                    <option value=""></option>
		                                    <option value="AF">Afghanistan</option>
		                                    <option value="AL">Albania</option>
		                                    <option value="DZ">Algeria</option>
		                                    <option value="AS">American Samoa</option>
		                                    <option value="AD">Andorra</option>
		                                    <option value="AO">Angola</option>
		                                    <option value="AI">Anguilla</option>
		                                    <option value="AQ">Antarctica</option>
		                                    <option value="AG">Antigua and Barbuda</option>
		                                    <option value="AR">Argentina</option>
		                                    <option value="AM">Armenia</option>
		                                    <option value="AW">Aruba</option>
		                                    <option value="AU">Australia</option>
		                                    <option value="AT">Austria</option>
		                                    <option value="AZ">Azerbaijan</option>
		                                    <option value="BH">Bahrain</option>
		                                    <option value="BD">Bangladesh</option>
		                                    <option value="BB">Barbados</option>
		                                    <option value="BY">Belarus</option>
		                                    <option value="BE">Belgium</option>
		                                    <option value="BZ">Belize</option>
		                                    <option value="BJ">Benin</option>
		                                    <option value="BM">Bermuda</option>
		                                    <option value="BT">Bhutan</option>
		                                    <option value="BO">Bolivia</option>
		                                    <option value="BA">Bosnia and Herzegovina</option>
		                                    <option value="BW">Botswana</option>
		                                    <option value="BV">Bouvet Island</option>
		                                    <option value="BR">Brazil</option>
		                                    <option value="IO">British Indian Ocean Territory</option>
		                                    <option value="VG">British Virgin Islands</option>
		                                    <option value="BN">Brunei</option>
		                                    <option value="BG">Bulgaria</option>
		                                    <option value="BF">Burkina Faso</option>
		                                    <option value="BI">Burundi</option>
		                                    <option value="CI">Côte d'Ivoire</option>
		                                    <option value="KH">Cambodia</option>
		                                    <option value="CM">Cameroon</option>
		                                    <option value="CA">Canada</option>
		                                    <option value="CV">Cape Verde</option>
		                                    <option value="KY">Cayman Islands</option>
		                                    <option value="CF">Central African Republic</option>
		                                    <option value="TD">Chad</option>
		                                    <option value="CL">Chile</option>
		                                    <option value="CN">China</option>
		                                    <option value="CX">Christmas Island</option>
		                                    <option value="CC">Cocos (Keeling) Islands</option>
		                                    <option value="CO">Colombia</option>
		                                    <option value="KM">Comoros</option>
		                                    <option value="CG">Congo</option>
		                                    <option value="CK">Cook Islands</option>
		                                    <option value="CR">Costa Rica</option>
		                                    <option value="HR">Croatia</option>
		                                    <option value="CU">Cuba</option>
		                                    <option value="CY">Cyprus</option>
		                                    <option value="CZ">Czech Republic</option>
		                                    <option value="CD">Democratic Republic of the Congo</option>
		                                    <option value="DK">Denmark</option>
		                                    <option value="DJ">Djibouti</option>
		                                    <option value="DM">Dominica</option>
		                                    <option value="DO">Dominican Republic</option>
		                                    <option value="TP">East Timor</option>
		                                    <option value="EC">Ecuador</option>
		                                    <option value="EG">Egypt</option>
		                                    <option value="SV">El Salvador</option>
		                                    <option value="GQ">Equatorial Guinea</option>
		                                    <option value="ER">Eritrea</option>
		                                    <option value="EE">Estonia</option>
		                                    <option value="ET">Ethiopia</option>
		                                    <option value="FO">Faeroe Islands</option>
		                                    <option value="FK">Falkland Islands</option>
		                                    <option value="FJ">Fiji</option>
		                                    <option value="FI">Finland</option>
		                                    <option value="MK">Former Yugoslav Republic of Macedonia</option>
		                                    <option value="FR">France</option>
		                                    <option value="FX">France, Metropolitan</option>
		                                    <option value="GF">French Guiana</option>
		                                    <option value="PF">French Polynesia</option>
		                                    <option value="TF">French Southern Territories</option>
		                                    <option value="GA">Gabon</option>
		                                    <option value="GE">Georgia</option>
		                                    <option value="DE">Germany</option>
		                                    <option value="GH">Ghana</option>
		                                    <option value="GI">Gibraltar</option>
		                                    <option value="GR">Greece</option>
		                                    <option value="GL">Greenland</option>
		                                    <option value="GD">Grenada</option>
		                                    <option value="GP">Guadeloupe</option>
		                                    <option value="GU">Guam</option>
		                                    <option value="GT">Guatemala</option>
		                                    <option value="GN">Guinea</option>
		                                    <option value="GW">Guinea-Bissau</option>
		                                    <option value="GY">Guyana</option>
		                                    <option value="HT">Haiti</option>
		                                    <option value="HM">Heard and Mc Donald Islands</option>
		                                    <option value="HN">Honduras</option>
		                                    <option value="HK">Hong Kong</option>
		                                    <option value="HU">Hungary</option>
		                                    <option value="IS">Iceland</option>
		                                    <option value="IN">India</option>
		                                    <option value="ID">Indonesia</option>
		                                    <option value="IR">Iran</option>
		                                    <option value="IQ">Iraq</option>
		                                    <option value="IE">Ireland</option>
		                                    <option value="IL">Israel</option>
		                                    <option value="IT">Italy</option>
		                                    <option value="JM">Jamaica</option>
		                                    <option value="JP">Japan</option>
		                                    <option value="JO">Jordan</option>
		                                    <option value="KZ">Kazakhstan</option>
		                                    <option value="KE">Kenya</option>
		                                    <option value="KI">Kiribati</option>
		                                    <option value="KW">Kuwait</option>
		                                    <option value="KG">Kyrgyzstan</option>
		                                    <option value="LA">Laos</option>
		                                    <option value="LV">Latvia</option>
		                                    <option value="LB">Lebanon</option>
		                                    <option value="LS">Lesotho</option>
		                                    <option value="LR">Liberia</option>
		                                    <option value="LY">Libya</option>
		                                    <option value="LI">Liechtenstein</option>
		                                    <option value="LT">Lithuania</option>
		                                    <option value="LU">Luxembourg</option>
		                                    <option value="MO">Macau</option>
		                                    <option value="MG">Madagascar</option>
		                                    <option value="MW">Malawi</option>
		                                    <option value="MY">Malaysia</option>
		                                    <option value="MV">Maldives</option>
		                                    <option value="ML">Mali</option>
		                                    <option value="MT">Malta</option>
		                                    <option value="MH">Marshall Islands</option>
		                                    <option value="MQ">Martinique</option>
		                                    <option value="MR">Mauritania</option>
		                                    <option value="MU">Mauritius</option>
		                                    <option value="YT">Mayotte</option>
		                                    <option value="MX">Mexico</option>
		                                    <option value="FM">Micronesia</option>
		                                    <option value="MD">Moldova</option>
		                                    <option value="MC">Monaco</option>
		                                    <option value="MN">Mongolia</option>
		                                    <option value="ME">Montenegro</option>
		                                    <option value="MS">Montserrat</option>
		                                    <option value="MA">Morocco</option>
		                                    <option value="MZ">Mozambique</option>
		                                    <option value="MM">Myanmar</option>
		                                    <option value="NA">Namibia</option>
		                                    <option value="NR">Nauru</option>
		                                    <option value="NP">Nepal</option>
		                                    <option value="NL">Netherlands</option>
		                                    <option value="AN">Netherlands Antilles</option>
		                                    <option value="NC">New Caledonia</option>
		                                    <option value="NZ">New Zealand</option>
		                                    <option value="NI">Nicaragua</option>
		                                    <option value="NE">Niger</option>
		                                    <option value="NG">Nigeria</option>
		                                    <option value="NU">Niue</option>
		                                    <option value="NF">Norfolk Island</option>
		                                    <option value="KP">North Korea</option>
		                                    <option value="MP">Northern Marianas</option>
		                                    <option value="NO">Norway</option>
		                                    <option value="OM">Oman</option>
		                                    <option value="PK">Pakistan</option>
		                                    <option value="PW">Palau</option>
		                                    <option value="PS">Palestine</option>
		                                    <option value="PA">Panama</option>
		                                    <option value="PG">Papua New Guinea</option>
		                                    <option value="PY">Paraguay</option>
		                                    <option value="PE">Peru</option>
		                                    <option value="PH">Philippines</option>
		                                    <option value="PN">Pitcairn Islands</option>
		                                    <option value="PL">Poland</option>
		                                    <option value="PT">Portugal</option>
		                                    <option value="PR">Puerto Rico</option>
		                                    <option value="QA">Qatar</option>
		                                    <option value="RE">Reunion</option>
		                                    <option value="RO">Romania</option>
		                                    <option value="RU">Russia</option>
		                                    <option value="RW">Rwanda</option>
		                                    <option value="ST">São Tomé and Príncipe</option>
		                                    <option value="SH">Saint Helena</option>
		                                    <option value="PM">St. Pierre and Miquelon</option>
		                                    <option value="KN">Saint Kitts and Nevis</option>
		                                    <option value="LC">Saint Lucia</option>
		                                    <option value="VC">Saint Vincent and the Grenadines</option>
		                                    <option value="WS">Samoa</option>
		                                    <option value="SM">San Marino</option>
		                                    <option value="SA">Saudi Arabia</option>
		                                    <option value="SN">Senegal</option>
		                                    <option value="RS">Serbia</option>
		                                    <option value="SC">Seychelles</option>
		                                    <option value="SL">Sierra Leone</option>
		                                    <option value="SG">Singapore</option>
		                                    <option value="SK">Slovakia</option>
		                                    <option value="SI">Slovenia</option>
		                                    <option value="SB">Solomon Islands</option>
		                                    <option value="SO">Somalia</option>
		                                    <option value="ZA">South Africa</option>
		                                    <option value="GS">South Georgia and the South Sandwich Islands</option>
		                                    <option value="KR">South Korea</option>
		                                    <option value="ES">Spain</option>
		                                    <option value="LK">Sri Lanka</option>
		                                    <option value="SD">Sudan</option>
		                                    <option value="SR">Suriname</option>
		                                    <option value="SJ">Svalbard and Jan Mayen Islands</option>
		                                    <option value="SZ">Swaziland</option>
		                                    <option value="SE">Sweden</option>
		                                    <option value="CH">Switzerland</option>
		                                    <option value="SY">Syria</option>
		                                    <option value="TW">Taiwan</option>
		                                    <option value="TJ">Tajikistan</option>
		                                    <option value="TZ">Tanzania</option>
		                                    <option value="TH">Thailand</option>
		                                    <option value="BS">The Bahamas</option>
		                                    <option value="GM">The Gambia</option>
		                                    <option value="TG">Togo</option>
		                                    <option value="TK">Tokelau</option>
		                                    <option value="TO">Tonga</option>
		                                    <option value="TT">Trinidad and Tobago</option>
		                                    <option value="TN">Tunisia</option>
		                                    <option value="TR">Turkey</option>
		                                    <option value="TM">Turkmenistan</option>
		                                    <option value="TC">Turks and Caicos Islands</option>
		                                    <option value="TV">Tuvalu</option>
		                                    <option value="VI">US Virgin Islands</option>
		                                    <option value="UG">Uganda</option>
		                                    <option value="UA">Ukraine</option>
		                                    <option value="AE">United Arab Emirates</option>
		                                    <option value="GB">United Kingdom</option>
		                                    <option value="US">United States</option>
		                                    <option value="UM">United States Minor Outlying Islands</option>
		                                    <option value="UY">Uruguay</option>
		                                    <option value="UZ">Uzbekistan</option>
		                                    <option value="VU">Vanuatu</option>
		                                    <option value="VA">Vatican City</option>
		                                    <option value="VE">Venezuela</option>
		                                    <option value="VN">Vietnam</option>
		                                    <option value="WF">Wallis and Futuna Islands</option>
		                                    <option value="EH">Western Sahara</option>
		                                    <option value="YE">Yemen</option>
		                                    <option value="ZM">Zambia</option>
		                                    <option value="ZW">Zimbabwe</option>
		                                </select>
		                                @if ($errors->has('customer_country'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('customer_country') }}</strong>
		                                    </span>
		                                @endif
		    						</div>
		    					</div>
		    					<div class="form-group row{{ $errors->has('customer_region') ? ' has-error' : '' }}">
		    						<label for="customer_region" class="control-label">State/Region <span class="required">*</span></label>
		    						<div class="">
		    							<select name="customer_region" class="form-control must-have bfh-states" id="customer_region" data-country="customer_country" data-toggle="tooltip" data-placement="right" title="" data-original-title="please enter state">
		                                    <option value="{{ old('customer_region') }}">{{ old('customer_region') }}</option>
		                                    <option value=""></option>
		                                    <option value="AL">Alabama</option>
		                                    <option value="AK">Alaska</option>
		                                    <option value="AS">American Samoa</option>
		                                    <option value="AZ">Arizona</option>
		                                    <option value="AR">Arkansas</option>
		                                    <option value="AF">Armed Forces Africa</option>
		                                    <option value="AA">Armed Forces Americas</option>
		                                    <option value="AC">Armed Forces Canada</option>
		                                    <option value="AE">Armed Forces Europe</option>
		                                    <option value="AM">Armed Forces Middle East</option>
		                                    <option value="AP">Armed Forces Pacific</option>
		                                    <option value="CA">California</option>
		                                    <option value="CO">Colorado</option>
		                                    <option value="CT">Connecticut</option>
		                                    <option value="DE">Delaware</option>
		                                    <option value="DC">District of Columbia</option>
		                                    <option value="FM">Federated States Of Micronesia</option>
		                                    <option value="FL">Florida</option>
		                                    <option value="GA">Georgia</option>
		                                    <option value="GU">Guam</option>
		                                    <option value="HI">Hawaii</option>
		                                    <option value="ID">Idaho</option>
		                                    <option value="IL">Illinois</option>
		                                    <option value="IN">Indiana</option>
		                                    <option value="IA">Iowa</option>
		                                    <option value="KS">Kansas</option>
		                                    <option value="KY">Kentucky</option>
		                                    <option value="LA">Louisiana</option>
		                                    <option value="ME">Maine</option>
		                                    <option value="MH">Marshall Islands</option>
		                                    <option value="MD">Maryland</option>
		                                    <option value="MA">Massachusetts</option>
		                                    <option value="MI">Michigan</option>
		                                    <option value="MN">Minnesota</option>
		                                    <option value="MS">Mississippi</option>
		                                    <option value="MO">Missouri</option>
		                                    <option value="MT">Montana</option>
		                                    <option value="NE">Nebraska</option>
		                                    <option value="NV">Nevada</option>
		                                    <option value="NH">New Hampshire</option>
		                                    <option value="NJ">New Jersey</option>
		                                    <option value="NM">New Mexico</option>
		                                    <option value="NY">New York</option>
		                                    <option value="NC">North Carolina</option>
		                                    <option value="ND">North Dakota</option>
		                                    <option value="MP">Northern Mariana Islands</option>
		                                    <option value="OH">Ohio</option>
		                                    <option value="OK">Oklahoma</option>
		                                    <option value="OR">Oregon</option>
		                                    <option value="PW">Palau</option>
		                                    <option value="PA">Pennsylvania</option>
		                                    <option value="PR">Puerto Rico</option>
		                                    <option value="RI">Rhode Island</option>
		                                    <option value="SC">South Carolina</option>
		                                    <option value="SD">South Dakota</option>
		                                    <option value="TN">Tennessee</option>
		                                    <option value="TX">Texas</option>
		                                    <option value="UT">Utah</option>
		                                    <option value="VT">Vermont</option>
		                                    <option value="VI">Virgin Islands</option>
		                                    <option value="VA">Virginia</option>
		                                    <option value="WA">Washington</option>
		                                    <option value="WV">West Virginia</option>
		                                    <option value="WI">Wisconsin</option>
		                                    <option value="WY">Wyoming</option>
		                                </select>
		                                @if ($errors->has('customer_region'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('customer_region') }}</strong>
		                                    </span>
		                                @endif
		    						</div>
		    					</div>
		    					<div class="form-group row{{ $errors->has('customer_postal_zip') ? ' has-error' : '' }}">
		    						<label for="customer_postal_zip" class="control-label">Zip Code <span class="required">*</span></label>
		    						<div class="">
		    							<input name="customer_postal_zip" type="text" class="form-control must-have" id="customer_postal_zip" placeholder="Zip Code" value="{{ old('customer_postal_zip') }}" data-toggle="tooltip" data-placement="right" title="" data-original-title="please enter zip code">
		                                @if ($errors->has('customer_name'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('customer_postal_zip') }}</strong>
		                                    </span>
		                                @endif
		    						</div>
		    					</div>
		    					<div class="form-group row{{ $errors->has('note') ? ' has-error' : '' }}">
		    						<label for="note" class="control-label">Leave us a note</label>
		    						<div class="">
		                                <!-- <label class="form-check-label"> -->
		                                  <!-- <input class="form-check-input" type="checkbox" name="same_as" id="same_as"> -->
		                                  <!-- <span class="small-90">Same as Booking Address</span> -->
		                                <!-- </label> -->
		    							<textarea name="note" type="text" class="form-control" id="note" value="{{ old('note') }}">{{ old('note') }}</textarea>
		                                @if ($errors->has('note'))
		                                    <span class="help-block">
		                                        <strong>The Note field is required.</strong>
		                                    </span>
		                                @endif
		    						</div>
		    					</div>
		    					<div class="form-group row{{ $errors->has('customer_tos_agree') ? ' has-error' : '' }}">
		    						<label for="customer_tos_agree" class="control-label">Terms &amp; Conditions</label>
		    						<div class="small-90">
		    	<p>You acknowledge that you have read and have fully understood the terms and conditions of the Customer Contract form and Guest Agreement detailed in <a href="customeragreement">https://ltstay.com/ltstay/customeragreement</a> governing this booking and you are hereby agreeing to fully comply by it in letter and spirit to the fullest extent.
		    	You acknowledge that each member of your group will sign the Customer Contract form at the time of your arrival or when asked.</p>

		    	<p><b>General Cancellation Policies</b><br>If you must cancel your stay and we receive your notice: more than 30 days prior to check in date: full refund minus deposit; 15-29 days prior to check in date: 50% refund; 14 days or less prior to check-in date: no refund</p>

		    	<p><b>Credit Car Processing Fees</b><br> We love to keep our costs low and prefer Check payments and ACH transfers. However, if you wish to make full payments through card, we will add 3.5% to such payments as card processing fees.</p>
		    							<div>
		    								<p></p>
		    								<input type="checkbox" id="customer_tos_agree" name="customer_tos_agree" value="1" data-toggle="tooltip" data-placement="right" title="" data-original-title="please read and agree to Terms of Service.">
		    								<b> I have read and agreed to the Terms of Service.</b>
		                                    @if ($errors->has('customer_tos_agree'))
		                                        <div class="help-block">
		                                            <strong>{{ $errors->first('customer_tos_agree') }}</strong>
		                                        </div>
		                                    @endif
		    							</div>
		    						</div>
		    					</div>
		    				</div>
		    				<div class="">
		    					<div class="pb-10px">
		    						<button type="submit" class="btn btn-success btn-book">Continue to Payment</button>
		    					</div>
		    					<div class="">
		    						<a href="/home/{{$item['item_id']}}" class="btn">Cancel</a>
		    					</div>
		    				</div>
		    			</form>
					</div>
				</div>
    		</div>
    	</div>
    </div>
</section>
<script>
    (function(w,d){
        var same = d.getElementById('same_as');
        var customerAddress = d.getElementById('customer_address');
        var customerCity = d.getElementById('customer_city');
        var customerCountry = d.getElementById('customer_country');
        var customerRegion = d.getElementById('customer_region');
        var customerZip = d.getElementById('customer_postal_zip');
        // var customerRegion = d.getElementById('customer_region');
        var note = d.getElementById('note');
        

        same.addEventListener('click', function() {
            var checked = this.checked;
            if(checked) {
                note.value = customerAddress.value+'\n'
                            +customerCity.value+' '
                            +customerRegion.value+'\n'
                            +customerCountry.value+'\n'
                            +customerZip.value;
            } else {
                note.value = '';
            }
        });
    })(window, document);
</script>