<div class='container-fluid p-0'>
    <div class='amenities page p-0'>
        <div class='heading-wrapper text-white position-relative'>
            <div class="position-absolute">
                <h2 class='heading'> Amenities </h2>
                <div class=''>Just walk in with your bag. We got everything covered!!!</div>
            </div>
        </div>
        <div class='content-wrapper'>
            <div class=''>
                <div style="padding: 10% 20%;">
                    <div class="row">
                        <div class="col-sm-7">
                            <h3 class=''> Rooms </h3>
                            <div class='heading-subtext fw-500'>Work or relax. The choice, and the space, is yours.</div>
                            <ul class='reset'>                    
                                <li>
                                    <span>Full Furnished Guesthouses with Single, Shared, Family rooms to suit your needs and budget</span>
                                </li>
                                <li>
                                    <span>Ambient lighting and ventilation to keep you happy & healthy</span>
                                </li>
                                <li>
                                    <span>Flexible Workspaces</span>
                                </li>
                                <li>
                                    <span>Table, executive chair, Bedding, Bed , Closets in rooms</span>
                                </li>
                                <li>
                                    <span>Free High Speed 100 Mbps WI-Fi</span>
                                </li>
                            </ul>
                        </div>
                        <div class="col-sm-5">
                            <img src="https://s3-us-west-1.amazonaws.com/longtermstay/amenities_rooms.jpg" width="100%" alt="amenities rooms">
                        </div>
                    </div>
                </div>
            </div>
            <div class=''>
                <div class='bg-white' style="padding: 10% 20%;">
                    <div class="row">
                        <div class="col-sm-7">
                            <h3 class=''> Common Area </h3>
                            <div class='heading-subtext fw-500'>The space that works for meetings, meals, or me-time.</div>
                            <ul class='reset'>
                                <li>
                                    <span>TV, Sofa, Dinning table etc in common area</span>
                                </li>
                                <li>
                                    <span>Free High Speed 100 Mbps WI-Fi, Indian Channels, Roku, Netflix</span>
                                </li>
                                <li>
                                    <span>Washer, dryer, iron box, kitchen appliances and everything to make you look perfect</span>
                                </li>
                                <li>
                                    <span>Professional Weekly Cleaning, onsite service manager</span>
                                </li>
                                <li>
                                    <span>General Consumables right from shaving gel, toothbrush, toiletries, laundry detergent, chemicals, tissues, room fresheners etc;</span>
                                </li>
                            </ul>
                        </div>
                        <div class="col-sm-5">
                            <img src="https://s3-us-west-1.amazonaws.com/longtermstay/amenities_living.jpg" width="100%" alt="common area">
                        </div>
                    </div>
                </div>
            </div>
            <div class=''>
                <div style="padding: 10% 20%;">
                    <div class="row">
                        <div class="col-sm-7">
                            <h3 class=''>Kitchen</h3>
                            <div class='heading-subtext fw-500'>Grab 'n go snacks. Sit-down meals. We've got tastes to suit your speed</div>
                            <ul class='reset'>
                                <li>
                                    <span>Complementary Breakfast items inclusive of almost everything you might need as fruit, juice, milk, cereals, bread, egg, butter, jam, spreads etc;</span>
                                </li>
                                <li>
                                    <span>Cook yourselves or Avail our partner food services</span>
                                </li>
                                <li>
                                    <span>We know you like that early morning coffee or that late night snack. You wont stay hungry as there is something healthy to eat all the time</span>
                                </li>
                            </ul>
                        </div>
                        <div class="col-sm-5">
                            <img src="https://s3-us-west-1.amazonaws.com/longtermstay/amenities_kitchen.jpg" width="100%" alt="kitchen area">
                        </div>
                    </div>
                </div>
            </div>
            <div class=''>
                <div class='bg-white' style="padding: 10% 20%;">
                    <div class="row">
                        <div class="col-sm-7">
                            <h3 class=''> OutDoors </h3>
                            <div class='heading-subtext fw-500'>Rain or Shine!!!!!!!</div>
                            <ul class='reset'>
                                <li>
                                    <span>Most of the Guesthouses have backyards. Stretch your arms, legs, body & mind</span>
                                </li>
                                <li>
                                    <span>Sit down in open spaces for a chat with house mate or phone to one you love</span>
                                </li>
                                <li>
                                    <span>Enjoy the beautiful California weather all the year round</span>
                                </li>
                            </ul>
                        </div>
                        <div class="col-sm-5">
                            <img src="https://s3-us-west-1.amazonaws.com/longtermstay/amenities_outdoors.jpg" width="100%" alt="outdoors">
                        </div>
                    </div>
                </div>
            </div>
            <div class=''>
                <div  style="padding: 10% 20%;">
                    <div class="row">
                        <div class="col-sm-7">
                            <h3 class=''> Policies </h3>
                            <div class='heading-subtext fw-500'>We need some policies to solve world peace.</div>
                            <ul class='reset'>
                                <li>
                                    <span>No Smoking in the room. cough! cough!</span>
                                </li>
                                <li>
                                    <span>Pets are not allowed</span>
                                </li>
                                <li>
                                    <span>No fighting over Shahrukh or Salman, Sachin or Dravid, Game of thrones or Breaking Bad. JK!</span>
                                </li>
                                <li>
                                    <span>Cancellation and prepayment policies vary according to room type. Please email to us to learn more about it</span>
                                </li>
                                <li>
                                    <span>When booking more than 5 rooms, different policies and additional supplements may apply</span>
                                </li>
                            </ul>
                        </div>
                        <div class="col-sm-5">
                            <img src="https://s3-us-west-1.amazonaws.com/longtermstay/amenities_policies.png" width="100%" alt="policies">
                        </div>
                    </div>
                </div>
            </div>
            <div class=''>
                <div class='bg-white' style="padding: 10% 20%;">
                    <div class="row">
                        <div class="col-sm-7">
                            <h3 class=''>Partner Services</h3>
                            <div class='heading-subtext fw-500'>We got you covered!!!</div>
                            <ul class='reset'>
                                <li>
                                    <span>Need a pickup or drop to airport? Need to go for shopping? Need to meet a friend or relative? Or Need to drop and pick at office daily? Avail our trusted super awesome partner services at reasonable cost</span>
                                </li>
                                <li>
                                    <span>Lazy to cook? Lived life like a king till now? Married? Avail our partner food services delivered two times a day to your home or office on time. Dont stay hungry, Dont stay foolish!</span>
                                </li>
                            </ul>
                        </div>
                        <div class="col-sm-5">
                            <img src="https://s3-us-west-1.amazonaws.com/longtermstay/amenities_partners.jpg" width="100%" alt="partner services">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>