<div class="promotions section bg-white">
    <div class="heading-wrapper">
        <h2 class="heading">{{$home_page['promotion_heading']}}</h2>
        <div class="heading-subtext" style="">{{$home_page['promotion_subtxt']}}</div>
    </div>
    <div class="content-wrapper row text-center">
        <div class="col-md-8 col-md-offset-2">
            <div class="row">
                @if($promotions)
                    @foreach($promotions as $item)
                        @if($loop->index > 3)
                            @break
                        @endif
                        <div class="col-xs-12 col-sm-4">
                            <div class="m-20px bd-1px bd-r-8px bg-f9f9f9">
                                <a href="{{$item['url']}}" class="text-black">
                                    <div class="pb-20px">
                                        <img src="{{$item['image']}}" class="bd-r-tr8px" height="263px" width="100%" alt="{{$item['title']}}">
                                    </div>
                                    <h4 class="color-red">
                                        {{$item['title']}}
                                    </h4>
                                    <div class="mb-10px">{{$item['description']}}</div>
                                    <div class="mb-10px d-block">{{$item['discount']}} off</div>
                                    <div class="mb-10px d-block">{{$item['time']}}</div>
                                </a>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
    <div class="btn-wrapper">
        <a href="{{ url($home_page['promotion_url']) }}">Explore All Promotions →</a>
    </div>
</div>
