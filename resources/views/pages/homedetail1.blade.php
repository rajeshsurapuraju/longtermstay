<section class="homedetail">
	
	<div class="big-img">
		<img src="/img/amenities_kitchen.jpg">
		<img src="/img/amenities_living.jpg">
		<img src="/img/amenities_outdoors.jpg">
		<img src="/img/amenities_partners.jpg">
		<img src="/img/amenities_policies.png">
		<img src="/img/amenities_rooms.jpg">
	</div>
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="row">
				<div class="col-sm-8 home-detail-content">
					<div class="bg-white home-detail-section">
						<div class="head">
							<h2 class="text-capitalize">MP02 Master</h2>
					        <div class="text-capitalize summary">Thrilling Adventure up in trees of Baturiti. The  quick brown fox jumped over lazy dog. I trilling Adventure up in trees of Baturiti.</div>
					        <div class="text-capitalize">
								<img src="/img/logo.png" height="30px">
								McCarthy Ranch, Milpitas, California
							</div>
						</div>
					</div>
					<div class="bg-white home-detail-section">
						<div class="row features-icons">
							<div>
								<i class="fa fa-wifi" title="Free Wi-Fi Internet"></i>
							</div>
							<div>
								<i class="fa fa-coffee" title="Breakfast items included"></i>
							</div>
							<div>
								<i class="fa fa-plane" title="Airport Pickup and Drop"></i>
							</div>
							<div>
								<i class="fa fa-tv" title="Satelite and Cable TV Included"></i>
							</div>
							<div>
								<i class="fa fa-car" title="Car parking"></i>
							</div>
						</div>
					</div>
					<div class="bg-white home-detail-section">
						<div class="about">
							<h3 class="">About this home</h3>
							<p>Thrilling Adventure up in trees of Baturiti. The  quick brown fox jumped over lazy dog. I trilling Adventure up in trees of Baturiti. Thrilling Adventure up in trees of Baturiti. The  quick brown fox jumped over lazy dog. I trilling Adventure up in trees of Baturiti.</p>
							<p> fox jumped over lazy dog. I trilling Adventure up in trees of Baturiti. Thrilling Adventure up in trees of Baturiti. The  quick brown fox jumped over lazy dog. I trilling Adventure up in trees of Baturiti.</p>
							<p>Thrilling Adventure Thrilling Adventure up in trees of Baturiti. The  quick brown Thrilling Adventure up in trees of Baturiti. The  quick brown up in trees of Baturiti. The  quick brown fox jumped over lazy dog. I trilling Adventure up in trees of Baturiti. Thrilling Adventure up in trees of Baturiti. The  quick brown fox jumped over lazy dog. I trilling Adventure up in trees of Baturiti.</p>
						</div>
					</div>
					<div class="bg-white home-detail-section">
						<h3 class="">Features of this home</h3>
						<div class="row features">
							<div class="col-sm-6">
								<div>
									<i class="fa fa-check-circle-o" aria-hidden="true"></i>
									Free Wi-Fi Internet inside rooms
								</div>
								<div>
									<i class="fa fa-check-circle-o" aria-hidden="true"></i>
									Breakfast Items included
								</div>
								<div>
									<i class="fa fa-check-circle-o" aria-hidden="true"></i>
									Airport Pickup and Drop
								</div>
								<div>
									<i class="fa fa-check-circle-o" aria-hidden="true"></i>
									Satelite and Cable TV Included
								</div>
								<div>
									<i class="fa fa-check-circle-o" aria-hidden="true"></i>
									Appliances
								</div>
							</div>
							<div class="col-sm-6">
								<div>
									<i class="fa fa-check-circle-o" aria-hidden="true"></i>
									Room Cleaning Services
								</div>
								<div>
									<i class="fa fa-check-circle-o" aria-hidden="true"></i>
									Beautiful view on the city
								</div>
								<div>
									<i class="fa fa-check-circle-o" aria-hidden="true"></i>
									Free parking on Driveway, Street
								</div>
								<div>
									<i class="fa fa-check-circle-o" aria-hidden="true"></i>
									Air Conditioning
								</div>
								<div>
									<i class="fa fa-check-circle-o" aria-hidden="true"></i>
									Refrigerator
								</div>
							</div>
						</div>
					</div>
					<div class="bg-white home-detail-section">
						<h3 class="">Policies</h3>
						<div class="policies">
							<div>
								<i class="fa fa-check-circle-o" aria-hidden="true"></i>
								No Smoking in the room.
							</div>
							<div>
								<i class="fa fa-check-circle-o" aria-hidden="true"></i>
								Pets are not allowed
							</div>
							<div>
								<i class="fa fa-check-circle-o" aria-hidden="true"></i>
								Cancellation and prepayment policies vary according to room type. Please email to us to learn more about it.
							</div>
							<div>
								<i class="fa fa-check-circle-o" aria-hidden="true"></i>
								When booking more than 5 rooms, different policies and additional supplements may apply.
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4 home-detail-book">
					<div class="bg-white home-detail-section">
			  			<h3 class="mb-30px">Book</h3>
			            <form role="search" method="POST" action="/book/534">
			                <div class="form-group row">
			                    <label for="delivery-address" class="control-label col-md-4 py-2 m-0 pr-0 text-d4d4d4">Price</label>
			                    <div class="col-md-8 pl-md-0">
			                        <label for="delivery-address" class="control-label py-2 m-0 p-0"><b>$200</b></label>
			            			<input type='hidden' name='slip[]' id='slip'/ value="tata">
			                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
			                        <input name="_method" type="hidden" value="POST">
                                            
			                    </div>
			                </div>
			                <!-- <div class="form-group row">
			                    <label for="months" class="control-label col-md-4 py-2 m-0 pr-0"># of months</label>
			                    <div class="col-md-8 pl-md-0">
			                        <input id="months" name="months" type="text" class="form-control">
			                    </div>
			                </div> -->
			                <div class="form-group row">
			                    <label for="startdate" class="control-label col-md-4 py-2 m-0 pr-0 text-d4d4d4">Start Date</label>
			                    <div class="col-md-8 pl-md-0">
			                        <div class="mt-2">{{$inputs['start_date']}}</div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="startdate" class="control-label col-md-4 py-2 m-0 pr-0 text-d4d4d4">Duration</label>
			                    <div class="col-md-8 pl-md-0">
			                        <div class="mt-2">30 days</div>
			                    </div>
			                </div>
			                <!-- <div class="form-group row">
			                    <label for="delivery-address" class="control-label col-md-4 py-2 m-0 pr-0 text-d4d4d4">Delivery Address</label>
			                    <div class="col-md-8 pl-md-0">
			                        <input id="delivery-address" name="delivery-address" type="text" class="form-control">
			                    </div>
			                </div> -->
			                <div class="form-group row">
			                    <label for="delivery-address" class="control-label col-md-4 py-2 m-0 pr-0 text-d4d4d4">Total Price</label>
			                    <div class="col-md-8 pl-md-0">
			                        <label for="delivery-address" class="control-label py-2 m-0 p-0"><b>$200</b></label>
			                    </div>
			                </div>
			                <div class="">
			                    <button id="search-btn-nav-top" type="submit" class="btn btn-primary col-8" style="margin-left: 4px;">
			                        Book now
			                    </button>
			                </div>
			            </form>
					</div>
					<div class="bg-white home-detail-section">
						<h3>Busy? Need a reminder?</h3>
						<div>
							<form class="d-inline-block">
								<input type="email" placeholder="email address" style="width:100%;" class="mb-10px form-control">
								Remind me in 
								<input type="number" value="3" style="width:50px;" class="mb-10px text-center form-control d-inline-block" min="1" max="30">
								days
								<input type="submit" value="Remind me!" class="btn btn-success d-block">
							</form>
						</div>
					</div>
					<div class="bg-white home-detail-section">
						<h3>Specials</h3>
						<div>Get 10% off for first month on this home.</div>
						<div>Book within next 1 hour</div>
					</div>
					<div class="bg-white home-detail-section">
						<h3>Check whats nearby</h3>
						<script type='text/javascript'>
							var ws_wsid = '285166fd12b6b003e9d2bf2432ece66f';
							var ws_address = 'Santa paula avenue, Sunnyvale, CA';
							var ws_format = 'tall';
							var ws_width = '100%';
							var ws_height = '350';
						</script>
						<style type='text/css'>#ws-walkscore-tile{position:relative;text-align:left}#ws-walkscore-tile *{float:none;}</style>
						<div id='ws-walkscore-tile'></div>
						<script type='text/javascript' src='http://www.walkscore.com/tile/show-walkscore-tile.php'></script>						
					</div>
					<div class="bg-white home-detail-section">
						<h3>Need a ride?</h3>
						<div>Avail our friendly partner services for a </div>
						<ul>
							<li>pick and drop to airport</li>
							<li>shopping around town</li>
							<li>pick and drop to office</li>
							<li>running errands and more</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>