<div class='container-fluid p-0'>
    <div class='page p-0'>
        <div class="heading-wrapper bg-white py-30px">
            <h2 class="heading" style="text-transform: capitalize;">{{$home_page['sharedliving_heading']}}</h2>
            <div class="heading-subtext">{{$home_page['sharedliving_subtxt']}}</div>
        </div>
        <div class="content-wrapper text-center py-30px">
            <div class="">
                <video src="{{$home_page['sharedliving_video']}}" controls loop autoplay="" muted="" poster="https://s3-us-west-1.amazonaws.com/longtermstay/shared_living.jpg" width="100%" style="position: relative;height: 60vh;">
                Sorry, your browser doesn't support embedded videos, 
                but don't worry, you can <a href="{{$home_page['sharedliving_video']}}">download it</a>
                and watch it with your favorite video player!
                </video>
            </div>
        </div>
    </div>
</div>
