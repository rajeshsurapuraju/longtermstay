<section class="homedetail container-fluid p-0">
    @if(isset($home["rate"]["slip"]))
	<div class="big-img text-center">
	    @if(isset($home["image"]))
	        @foreach ($home["image"] as $img)
				<img src="https://{{env("CHECKFRONT_HOST")}}{{$img['path']}}">
	        @endforeach
		@endif
	</div>
	<div class="">
		<div class="col-md-8 col-md-offset-2">
			<div class="row">
				<div class="col-sm-8 home-detail-content">
					<div class="bg-white home-detail-section">
						<div class="head">
							<!-- <div class="text-capitalize">{{$home["sku"]}}</div> -->
							<h2 class="text-capitalize">{{$home["roomtype"]}}</h2>
							<div class="text-capitalize mb-10px">{{$home["name"]}}</div>
					        <div class="text-capitalize summary">{!! $home["summary"] !!}</div>
					        <div class="text-capitalize">
								<img src="/img/logo.png" height="30px">
								<span id="walkscore-loc" data-loc="{{$home["loc"]}}" data-loc="{{$home["loc-full"]}}" data-loc-lat="{{$home["loc-lat"]}}" data-loc-lon="{{$home["loc-lon"]}}">{{$home["loc-full"]}}</span>
							</div>
						</div>
					</div>
					<div class="bg-white home-detail-section">
						<div class="row features-icons">
							<div>
								<i class="fa fa-wifi" title="Free Wi-Fi Internet"></i>
							</div>
							<div>
								<i class="fa fa-coffee" title="Breakfast items included"></i>
							</div>
							<div>
								<i class="fa fa-plane" title="Airport Pickup and Drop"></i>
							</div>
							<div>
								<i class="fa fa-tv" title="Satelite and Cable TV Included"></i>
							</div>
							<div>
								<i class="fa fa-car" title="Car parking"></i>
							</div>
							<div>
								<i class="fa fa-cutlery" title="You can cook here"></i>
							</div>
						</div>
					</div>
					<div class="bg-white home-detail-section">
						<div class="about">
							<h3 class="">About this home</h3>
							<p>{!! $home["details"] !!}</p>

							<ul class="pills">
								<li>{{$home["bath"]}}</li>
								<li>{{$home["roomtype"]}}</li>
								<li>{{$home["loc"]}}</li>
								<li>{{$home["stayterm"]}}</li>
								<li>{{$home["price"]}}</li>
							</ul>
						</div>
					</div>
					<div id="map" class="mb-15px" style="height: 300px;"></div>
				    <script>
				    	function initMap() {
							var lat = document.getElementById('walkscore-loc').getAttribute('data-loc-lat') || '37.384846';
							var lon = document.getElementById('walkscore-loc').getAttribute('data-loc-lon') || '122.028988';
					        var ltstay = {lat: parseFloat(lat), lng: parseFloat(lon)};
							console.log(ltstay);
					        var map = new google.maps.Map(document.getElementById('map'), {
					          zoom: 16,
					          center: ltstay
					        });
					        var marker = new google.maps.Marker({
					          position: ltstay,
					          map: map
					        });
				      	}
				    </script>
				    <script async defer src="https://maps.googleapis.com/maps/api/js?key={{env("MAPS_KEY")}}&callback=initMap"></script>
					<div class="bg-white home-detail-section">
						<h3 class="">Features of this home</h3>
						<div class="row features">
							<div class="col-sm-6">
								<div>
									<i class="fa fa-check-circle-o" aria-hidden="true"></i>
									Free Wi-Fi Internet inside rooms
								</div>
								<div>
									<i class="fa fa-check-circle-o" aria-hidden="true"></i>
									Breakfast Items included
								</div>
								<div>
									<i class="fa fa-check-circle-o" aria-hidden="true"></i>
									Airport Pickup and Drop
								</div>
								<div>
									<i class="fa fa-check-circle-o" aria-hidden="true"></i>
									Satelite and Cable TV Included
								</div>
								<div>
									<i class="fa fa-check-circle-o" aria-hidden="true"></i>
									Appliances
								</div>
							</div>
							<div class="col-sm-6">
								<div>
									<i class="fa fa-check-circle-o" aria-hidden="true"></i>
									Room Cleaning Services
								</div>
								<div>
									<i class="fa fa-check-circle-o" aria-hidden="true"></i>
									Beautiful view on the city
								</div>
								<div>
									<i class="fa fa-check-circle-o" aria-hidden="true"></i>
									Free parking on Driveway, Street
								</div>
								<div>
									<i class="fa fa-check-circle-o" aria-hidden="true"></i>
									Fan / Air Conditioner
								</div>
								<div>
									<i class="fa fa-check-circle-o" aria-hidden="true"></i>
									Refrigerator
								</div>
							</div>
						</div>
					</div>
					<div class="bg-white home-detail-section">
						<h3 class="">Policies</h3>
						<div class="policies">
							<div>
								<i class="fa fa-check-circle-o" aria-hidden="true"></i>
								No Smoking in the room.
							</div>
							<div>
								<i class="fa fa-check-circle-o" aria-hidden="true"></i>
								Pets are not allowed
							</div>
							<div>
								<i class="fa fa-check-circle-o" aria-hidden="true"></i>
								Cancellation and prepayment policies vary according to room type. Please email to us to learn more about it.
							</div>
							<div>
								<i class="fa fa-check-circle-o" aria-hidden="true"></i>
								When booking more than 5 rooms, different policies and additional supplements may apply.
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4 home-detail-book">
					<div class="bg-white home-detail-section">
			  			<h3 class="pb-10px">Book</h3>
			            <form role="search" method="POST" action="/book/{{$home["item_id"]}}">
			                <div class="form-group row">
			                    <label for="delivery-address" class="control-label col-md-4 py-2 m-0 pr-0 text-d4d4d4">Price</label>
			                    <div class="col-md-8 pl-md-0">
			                        <label for="delivery-address" class="control-label py-2 m-0 p-0"><b>{{$home["rate"]["summary"]["price"]["total"]}}</b></label>
			            			<input type='hidden' name='slip[]' id='slip'/ value="{{$home["rate"]["slip"]}}">
			                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
			                        <input name="_method" type="hidden" value="POST">
                                            
			                    </div>
			                </div>
			                <!-- <div class="form-group row">
			                    <label for="months" class="control-label col-md-4 py-2 m-0 pr-0"># of months</label>
			                    <div class="col-md-8 pl-md-0">
			                        <input id="months" name="months" type="text" class="form-control">
			                    </div>
			                </div> -->
			                <div class="form-group row">
			                    <label for="startdate" class="control-label col-md-4 py-2 m-0 pr-0 text-d4d4d4">Start Date</label>
			                    <div class="col-md-8 pl-md-0">
			                        <div class="mt-2">{{$inputs['start_date']}}</div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="startdate" class="control-label col-md-4 py-2 m-0 pr-0 text-d4d4d4">Duration</label>
			                    <div class="col-md-8 pl-md-0">
			                        <div class="mt-2">30 days</div>
			                    </div>
			                </div>
			                <!-- <div class="form-group row">
			                    <label for="delivery-address" class="control-label col-md-4 py-2 m-0 pr-0 text-d4d4d4">Delivery Address</label>
			                    <div class="col-md-8 pl-md-0">
			                        <input id="delivery-address" name="delivery-address" type="text" class="form-control">
			                    </div>
			                </div> -->
			                <div class="form-group row">
			                    <label for="delivery-address" class="control-label col-md-4 py-2 m-0 pr-0 text-d4d4d4">Monthly Price</label>
			                    <div class="col-md-8 pl-md-0">
			                        <label for="delivery-address" class="control-label py-2 m-0 p-0">
										<b>{{$home["rate"]["summary"]["price"]["total"]}}</b>
										<div><i class="small-90" style="font-weight: normal;"> recurring monthly till stay ends</i></div>
									</label>
			                    </div>
			                </div>
			                <div class="">
			                    <button id="search-btn-nav-top" type="submit" class="btn btn-primary col-8" style="margin-left: 4px;">
			                        Book now
			                    </button>
			                    <!-- <a class="btn btn-primary col-8" href='/book/{{$home["item_id"]}}'>Book now</a> -->
			                </div>
			            </form>
					</div>
					<div class="bg-white home-detail-section d-none">
						<h3>Specials</h3>
						<div>Get 10% off for first month on this home.</div>
						<div>Book within next 1 hour</div>
					</div>
					<div class="bg-white home-detail-section d-none">
						<h3>Busy? Need a reminder?</h3>
						<div>
							<form class="d-inline-block">
								<input type="email" placeholder="email address" style="width:100%;" class="mb-10px form-control">
								Remind me in 
								<input type="number" value="3" style="width:50px;" class="mb-10px text-center form-control d-inline-block" min="1" max="30">
								days
								<input type="submit" value="Remind me!" class="btn btn-success d-block">
							</form>
						</div>
					</div>
					<div class="bg-white home-detail-section">
						<h3>Check whats nearby</h3>
						<script type='text/javascript'>
							var ws_wsid = '285166fd12b6b003e9d2bf2432ece66f';
							var ws_address = document.getElementById('walkscore-loc').getAttribute('data-loc');
							var ws_format = 'tall';
							var ws_width = '100%';
							var ws_height = '350';
						</script>
						<style type='text/css'>#ws-walkscore-tile{position:relative;text-align:left}#ws-walkscore-tile *{float:none;}</style>
						<div id='ws-walkscore-tile'></div>
						<script type='text/javascript' src='/js/walkscore.js'></script>						
					</div>
					<div class="bg-white home-detail-section">
						<h3>Need a ride?</h3>
						<div class="mb-10px">Avail our friendly partner services for</div>
						<ul>
							<li>pick and drop to airport</li>
							<li>shopping around town</li>
							<li>pick and drop to office</li>
							<li>running errands and more</li>
						</ul>
						<div class="small-90" style="padding: 10px 0;"><i>additional rates apply</i></div>
					</div>
				</div>
			</div>
		</div>
	</div>
    @else
        <div class="text-center py-5">
            <div class="pb-4"> This home is no longer available. </div>
            <a href="/listings" class="action-btn">Go Back</a>
        </div>
    @endif
</section>