<div class="blog section">
	<div class="heading-wrapper row">
		<h2 class="heading">Keep informed about your surrondings.</h2>
	</div>
	<div class="content-wrapper row text-center">
		<div>
		@if($blog)
			@foreach($blog as $item)
				@if($loop->index > 5)
					@break
				@endif
				<div class="col-xs-12 col-sm-6 col-md-4 mb-10px">
					<div class="blog-wrap">
						<div class="pb-20px">
							<img src="{{$item['image']}}">
						</div>
						<h3>
							<a href="{{$item['url']}}" class="heading">{{$item['title']}}</a>
						</h3>
						<div>{{$item['date']}}</div>
					</div>
				</div>
			@endforeach
		@endif
		</div>
	</div>
	<div class="btn-wrapper">
		<a href="{{ url('http://blog.ltstay.com') }}" class="btn btn-primary btn-lg">Visit Blog →</a>
	</div>
</div>
