<div class="section bg-white">
    <div>
        <div class="mb-30px text-center">
            <h2 class="heading" style="">{{$home_page['sharedliving_heading']}}</h2>
            <div class="heading-subtext" style="">{{$home_page['sharedliving_subtxt']}}</div>
        </div>
    </div>
    <div class="text-center row" style="">
        <div>
            <video src="{{$home_page['sharedliving_video']}}" controls="" loop="" autoplay="" muted="" poster="https://s3-us-west-1.amazonaws.com/longtermstay/shared_living.jpg" width="100%" style="position: relative;height: 70vh;" title="{{$home_page['sharedliving_heading']}}">
            Sorry, your browser doesn't support embedded videos, 
            but don't worry, you can <a href="{{$home_page['sharedliving_video']}}">download it</a>
            and watch it with your favorite video player!
            </video>
        </div>
    </div>
    <div class="text-center" style="padding-top: 20px;">
        <a href="{{$home_page['sharedliving_url']}}" class="btn btn-primary btn-lg btn-red">Explore More →</a>
    </div>
</div>