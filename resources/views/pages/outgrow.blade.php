<div class="testimonials section bg-f9f9f9 px-0">
    <div class="heading-wrapper">
        <h2 class="heading">{{$home_page['outgrow_heading']}}</h2>
        <div class="heading-subtext">{{$home_page['outgrow_subtxt']}}</div>
    </div>
    <div class="content-wrapper row text-center px-5">
        <div><div class='op-interactive' id='5a0376a78096a2862aa8d4dc' data-url='https://ltstay.outgrow.us/5a0376a78096a2862aa8d4dc?sLead=1' data-surl='https://goo.gl/62qt6j' data-width='100%'></div><script src='//outgrow.co/js/loader/sloader.js'></script><script>initIframe('5a0376a78096a2862aa8d4dc');</script></div>
    </div>
</div>
