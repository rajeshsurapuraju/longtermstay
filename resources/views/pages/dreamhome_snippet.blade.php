<div class="dreamhome blog section">
	<div class="heading-wrapper row">
		<h2 class="heading">Follow your dream. Find the right place.</h2>
		<div class="heading-subtext">Best of the Best places in Long Term Stay</div>
	</div>
	<div class="content-wrapper row text-center">
		<div>
		@if($dreamhome)
			@foreach($dreamhome as $item)
				@if($loop->index > 5)
					@break
				@endif
				<div class="col-xs-12 col-sm-6 col-md-4 mb-10px">
					<div class="blog-wrap">
						<div class="pb-20px">
							<img src="{{$item['image']}}" style="width: 100%;">
						</div>
						<h3>
							<a href="{{$item['url']}}" class="heading">{{$item['name']}}</a>
						</h3>
					</div>
				</div>
			@endforeach
		@endif
		</div>
	</div>
	<!-- div class="btn-wrapper">
		<a href="{{ url('http://blog.ltstay.com') }}" class="btn btn-primary btn-lg">Visit Blog →</a>
	</div -->
</div>
