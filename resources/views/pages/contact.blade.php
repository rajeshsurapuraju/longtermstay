<div class="heading-wrapper py-30px bg-white">
	<h2 class="heading">Contact</h2>
</div>
<div class='contact-us page'>
	@if(Session::has('success'))
		<div class="alert alert-success" style="text-align: center;">
			<strong>{{session('success')}}</strong>
		</div>
	@endif
	<iframe src="https://snazzymaps.com/embed/29381" width="100%" height="600px" style="border:none;"></iframe>
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="row py-30px">
				<p>Thanks for contacting Long Term Stay Inc. A Delaware Corporation, we currently operates 14 active guest houses, spread across south bay area, providing hospitality solutions to almost hundreds of folks working with leading co1mpanies like Apple, Microsoft,  Kohl’s, Intuit, Facebook, Google, Yahoo, Cisco,  Comcast, Motorola, HP, Samsung etc.</p>

				<p>Let us help save you the trouble of house hunting and dealing with the myriads of unknowns and variables. Just walk in with your suitcase and enjoy the unparalleled facilities on offer.</p>
			</div>
			<div class='content-wrapper row'>
		  	<div class="col-xs-12 col-sm-12 col-md-5 col-left">
				<div>
						<div class="mb-10px">
							<a href="mailto:crm@ltstay.com">
								<i class="icon fa fa-envelope"></i>
								crm@ltstay.com
							</a>
						</div>
						<div class="mb-10px">
							<i class="icon fa fa-address-card"></i>
							<span>1584 Branham Ln, 65, San Jose 95118</span>
						</div>
						<div class="mb-10px">
							<a href="tel:+18444587829">
								<i class="icon fa fa-phone"></i>
								<span>1-844-4LTSTAY</span>
							</a>
						</div>
						<div class="mb-10px">
							<a href='https://www.facebook.com/longtermstay/'>
								<i class="icon fa fa-facebook"></i>
								<span>Facebook</span>
							</a>
						</div>
						<div class="mb-10px">
							<a href='https://plus.google.com/+Ltstay/about'>
								<i class="icon fa fa-google-plus"></i>
								Google+
							</a>
						</div>
						<div class="mb-10px">
							<a href='https://twitter.com/uscorppg'>
								<i class="icon fa fa-twitter"></i>
								Twitter
							</a>
						</div>
					</div>
				</div>
			  	<div class="col-xs-12 col-md-7 col-right">
					<div class='text-center'>
						<div class='pb-15px heading-subtext'>Comments or Questions? We'd love to hear from you!</div>
					</div>
					<form method='post' class='contact-form form form-horizontal' action="{{url('contact')}}">
						<?php
		                /*
							if (validation_errors()) {
								echo '<span class="contact-error">';
								echo validation_errors();
								echo '</span>';
							}
		                */
						?>
					  	<div class="form-group">
							<div class='col-sm-12'>
								<label for="name">Name <span class="required">*</span></label>
							</div>
							<div class='col-sm-12'>
								<input name='name' type="text" class="must-have form-control" id="name" placeholder="Name" data-toggle="tooltip" data-placement="right" title="please enter first name & last name. ex: John Appleseed.">
							</div>
					  	</div>
					  	<div class="form-group">
							<div class='col-sm-12'>
								<label for="email">Email <span class="required">*</span></label>
							</div>
							<div class='col-sm-12'>
								<input name='email' type="email" class="must-have form-control" id="email" placeholder="Email" data-toggle="tooltip" data-placement="right" title="please enter email address. ex:crm@ltstay.com"> 
							</div>
					  	</div>
					  	<div class="form-group">
							<div class='col-sm-12'>
								<label for="name">Phone Number</label>
							</div>
							<div class='col-sm-12'>
								<input name='phone' type="number" class="form-control" id="phone" placeholder="Phone Number" data-toggle="tooltip" data-placement="right" title="please enter phone number.">
							</div>
					  	</div>
					  	<div class="form-group">
							<div class='col-sm-12'>
								<label for="message">Questions / Feedback <span class="required">*</span></label>
							</div>
							<div class='col-sm-12'>
								<textarea class="must-have form-control" id="message" name='message' placeHolder='Type your message here.' data-toggle="tooltip" data-placement="right" title="please enter email message here."></textarea>
							</div>
						</div>
						<p>
						  <a class="" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
						    <span>More details</span><i class="fa fa-chevron-down ml-5px"></i>
						  </a>
						</p>
						<div class="collapse" id="collapseExample">
						  	<div class="form-group">
								<div class='col-sm-12'>
									<label for="loc">Preferred Area</label>
								</div>
								<div class='col-sm-12'>
									<select multiple="multiple" name="loc[]" id="loc">
										<option value="Sunnyvale">Sunnyvale</option>
										<option value="San Jose">San Jose</option>
										<option value="Santa Clara">Santa Clara</option>
										<option value="Cupertino">Cupertino</option>
										<option value="Milpitas">Milpitas</option>
										<option value="Other">Other</option>
									</select>
								</div>
						  	</div>
						  	<div class="form-group">
								<div class='col-sm-12'>
									<label for="room">Room type</label>
								</div>
								<div class='col-sm-12'>
									<select name="room[]" multiple="multiple">
										<option value="TBD">TBD</option>
										<option value="Single">Private</option>
										<option value="Shared">Shared</option>
										<option value="Couple">Family</option>
									</select>
                                    <div>
                                        <input type="checkbox" name="flexroom" class="mr-5px">Flexible with room type
                                    </div>
								</div>
                            </div>
						  	<div class="form-group">
								<div class='col-sm-12'>
									<label for="movein">Move-in Date</label>
								</div>
								<div class='col-sm-12'>
									<input name='movein' type="date" class="form-control" id="movein" placeholder="mm/dd/yyyy" data-toggle="tooltip" data-placement="right" title="please enter move-in date.">
                                    <input type="checkbox" name="flexmovein" class="mr-5px">Flexible with move-in date
								</div>
						  	</div>
						  	<div class="form-group">
								<div class='col-sm-12'>
									<label for="address">Office/Business Address</label>
								</div>
								<div class='col-sm-12'>
                                    <textarea class="form-control" id="address" name='address' placeHolder='Type your address here.' data-toggle="tooltip" data-placement="right" title="please enter address here."></textarea>
								</div>
						  	</div>
						  	<div class="form-group">
								<div class='col-sm-12'>
									<label for="calltime">Best time to call</label>
								</div>
								<div class='col-sm-12'>
									<input name='calltime' type="text" class="form-control" id="calltime" placeholder="Best time to call" data-toggle="tooltip" data-placement="right" title="please enter best time to call.">
                                    <input type="checkbox" name="mailfirst" class="mr-5px">Send me a mail first
								</div>
						  	</div>
                            
						</div>
						<div class='btn-wrapper row col-sm-12'>
		                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
							<button type='submit' class='btn btn-danger contact-btn'>Send Message</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
