
<?php
$start_date = date('m/d/Y'); 
$end_date = date('m/d/Y', strtotime('+30 days'));
?>
<div class='locations section bg-white'> 
    <div class='heading-wrapper'>
        <h2 class='heading'>{{$home_page['locations_heading']}}</h2>
        <div class='heading-subtext'>{{$home_page['locations_subtxt']}}</div>
    </div>
    <div class='content-wrapper row'>
        <div class="col-md-12 col-lg-8 col-lg-offset-2">
            <div class='location-wrapper row'>
                <div class='col-xs-6 col-md-3 mb-30px'>
                    <a class='location-link bd-r-8px h-300px h-sm-170px' title="{{$home_page['locations_image1_txt']}}" style="background-image:url({{$home_page['locations_image1']}})" href='/listings?start_date={{$start_date}}&end_date={{$end_date}}&location={{$home_page['locations_image1_loc_code']}}'>
                        <h2>{{$home_page['locations_image1_txt']}}</h2>
                    </a>
                </div>
                <div class='col-xs-6 col-md-3 mb-30px'>
                    <a class='location-link bd-r-8px h-300px h-sm-170px' title="{{$home_page['locations_image2_txt']}}" style="background-image:url({{$home_page['locations_image2']}})" href='listings?start_date={{$start_date}}&end_date={{$end_date}}&location={{$home_page['locations_image2_loc_code']}}'>
                        <h2>{{$home_page['locations_image2_txt']}}</h2>
                    </a>
                </div>
                <div class='col-xs-6 col-md-3 mb-30px'>
                    <a class='location-link bd-r-8px h-300px h-sm-170px' title="{{$home_page['locations_image3_txt']}}" style="background-image:url({{$home_page['locations_image3']}})" href='listings?start_date={{$start_date}}&end_date={{$end_date}}&location={{$home_page['locations_image3_loc_code']}}'>
                        <h2>{{$home_page['locations_image3_txt']}}</h2>
                    </a>
                </div>
                <div class='col-xs-6 col-md-3 mb-30px'>
                    <a class='location-link bd-r-8px h-300px h-sm-170px' title="{{$home_page['locations_image4_txt']}}" style="background-image:url({{$home_page['locations_image4']}})" href='listings?start_date={{$start_date}}&end_date={{$end_date}}&location={{$home_page['locations_image4_loc_code']}}'>
                        <h2>{{$home_page['locations_image4_txt']}}</h2>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class='text-center'>
        <a href='{{url($home_page['locations_url'])}}' class='btn btn-primary btn-lg btn-red'>Explore More Locations →</a>
    </div>
</div>
