<div class='gallery gallery_final section'  style="background-image:url({{$home_page['gallery_image']}})">
    <div class="gallery_txt">
        <h2 class="heading">{{$home_page['gallery_heading']}}</h2>
        <div>{{$home_page['gallery_subtxt']}}</div>
        <a href="{{$home_page['gallery_url']}}" class="btn btn-primary btn-lg">Visit our gallery</a>
    </div>
</div>
