<div class="rooms section">
	<div class="heading-wrapper row">
		<h2 class="heading">Take a look at our limited period deals.</h2>
	</div>
	<div class="content-wrapper row text-center">
		<div class="col-xs-12 col-sm-4">
			<div class="pb-20px">
				<img src="img/longtermstay_single.jpg">
			</div>
			<h3 class="bb-1d mb-10px">
				<a href="/rooms/private" class="heading">Private</a>
			</h3>
			<div>
				<div>Beds: 1 Bed, Executive Chair, Tables</div>
				<div>
					<span>Max person:</span>
					<i class="fa fa-user"></i>
				</div>
			</div>
			<div class="btn-wrapper">
				<a href="{{ url('http://promotions.ltstay.com/rooms/private') }}" class="btn btn-primary btn-lg">Deals starting from $55</a>
			</div>
		</div>
		<div class="col-xs-12 col-sm-4">
			<div class="pb-20px">
				<img src="img/longtermstay_shared.jpg">
			</div>
			<h3 class="bb-1d mb-10px">
				<a href="/rooms/shared" class="heading">Shared</a>
			</h3>
			<div>
				<div>Beds: 2 Single Bed, Table and Exeuctive Chairs</div>
				<div>
					<span>Max person:</span>
					<i class="fa fa-user"></i>
					<i class="fa fa-user"></i>
				</div>
			</div>
			<div class="btn-wrapper">
				<a href="{{ url('http://promotions.ltstay.com/rooms/shared') }}" class="btn btn-primary btn-lg">Deals starting from $35</a>
			</div>
		</div>
		<div class="col-xs-12 col-sm-4">
			<div class="pb-20px">
				<img src="img/longtermstay_family.png">
			</div>
			<h3 class="bb-1d mb-10px">
				<a href="/rooms/family" class="heading">Family</a>
			</h3>
			<div>
				<div>Beds: 1 Queen Bed, Kid Beds</div>
				<div>
					<span>Max person:</span>
					<i class="fa fa-male"></i>
					<i class="fa fa-female"></i>
					<i class="fa fa-child"></i>
				</div>
			</div>
			<div class="btn-wrapper">
				<a href="{{ url('http://promotions.ltstay.com/rooms/family') }}" class="btn btn-primary btn-lg">Deals starting from $75</a>
			</div>
		</div>
	</div>
	<div class="btn-wrapper">
		<a href="{{ url('http://promotions.ltstay.com') }}">Explore All Promotions →</a>
	</div>
</div>
