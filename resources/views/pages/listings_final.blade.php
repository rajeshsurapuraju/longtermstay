<section class="py-md-2 py-2 homes-list container-fluid">
	<div class="row">
	    <div class="col-sm-3 col-lg-2 filter">
	        <!-- filters -->
	        <div class="filter-inner bg-white">
	            <h6 class="refine-by" data-toggle="collapse" href="#collapseRefine" aria-expanded="false" aria-controls="collapseRefine">
	                <span>Refine by</span>
	                <i class="fa fa-chevron-down text-primary d-inline d-md-none"></i>
	            </h6>
	            <div class="collapse show" id="collapseRefine">
	                <div class="small fw-600 pb-1 refine-section-heading">Room Type</div>
	                <ul id="roomtype" class="p-0 pb-3 list-unstyled" data-cat="type">
	                    <li class="small-90">
	                        <input type="radio" id="rt_all" value="all" name="roomtype">
	                        <label for="rt_all">All</label>
	                    </li>
						@foreach($roomtype as $key=>$room)
		                    <li class="small-90">
		                        <input type="radio" id="rt_{{$room['code']}}" value="{{$room['code']}}" name="roomtype">
		                        <label for="rt_{{$room['code']}}">{{ucfirst($room['type'])}}</label>
		                    </li>
						@endforeach
	                </ul>
					<div class="small fw-600 pb-1 refine-section-heading">Attached Bath</div>
	                <ul id="attached-bath" class="p-0 pb-3 list-unstyled" data-cat="type">
	                    <li class="small-90">
							<input type="checkbox" value="ab" id="ab_ab">
	                        <label for="ab_ab">With attached bath</label>
	                    </li>
	                    <li class="small-90">
							<input type="checkbox" value="nab" id="ab_nab">
	                        <label for="ab_nab">Without attached bath</label>
	                    </li>
	                </ul>
					<div class="small fw-600 pb-1 refine-section-heading">Stay Term</div>
	                <ul id="stayterm" class="p-0 pb-3 list-unstyled" data-cat="type">
	                    <li class="small-90">
	                        <input type="radio" id="st_all" value="all" name="stayterm">
	                        <label for="st_all">All</label>
	                    </li>
	                    <li class="small-90">
	                        <input type="radio" id="st_st" value="st" name="stayterm">
	                        <label for="st_st">Short Term</label>
	                    </li>
	                    <li class="small-90">
	                        <input type="radio" id="st_lt" value="lt" name="stayterm">
	                        <label for="st_lt">Long Term</label>
	                    </li>
						
	                </ul>
					<div class="small fw-600 pb-1 refine-section-heading">Features</div>
	                <ul id="features" class="p-0 pb-3 list-unstyled" data-cat="type">
						@foreach($features as $key=>$feature)
		                    <li class="small-90">
								@if($feature['unchangeable'] == 'true')
		                        <input type="checkbox" disabled checked id="ft_{{$feature['code']}}">
								@else
								<input type="checkbox" value="{{$feature['code']}}" id="ft_{{$feature['code']}}">
								@endif
		                        <label for="ft_{{$feature['code']}}">{{ucfirst($feature['name'])}}</label>
		                    </li>
						@endforeach					
	                </ul>
	                <!-- <div class="small fw-600 pb-1 refine-section-heading">Nearby Locations</div>
	                <ul id="location" class="p-0 pb-3 list-unstyled" data-cat="type">
						@foreach($locations as $key=>$loc)
		                    <li class="small-90" data-val="{{strtolower($loc['code'])}}">
		                        <input type="checkbox" id="lc_{{strtolower($loc['code'])}}" value="{{strtolower($loc['code'])}}">
		                        <label for="lc_{{strtolower($loc['code'])}}">{{ucfirst($loc['location_name'])}}</label>
		                    </li>
						@endforeach
	                </ul> -->
	                <div class="small fw-600 pb-1 refine-section-heading">Price per month</div>
					<div id="price-per-month">
					  <div id="amount"  style="margin-bottom:5px; color:#ed2227; font-weight:bold;"></div>
					  <input type="hidden" id="price" value="0-3000">
					</div>
 
					<div id="slider-range"></div>
	            </div>
	        </div>
	    </div>
	    <div class="col-sm-9 col-lg-10">
	        <div>
	            <ul class="list-unstyled p-3 m-0 refine-list d-none">
	            </ul>
	        </div>
	        <div class="text-left bg-white results-view">
	            <span id="results-count">{{count($items)}} results</span>
	            <i class="fa fa-th homes-view text-danger d-md-inline" data-template="home-template2"></i>
	            <i class="fa fa-th-list homes-view d-md-inline" data-template="home-template1"></i>
	        </div>
	        @if(count($items))
	           <div class="home-items"></div>
	        @else
	            <div class="text-center py-30px bg-white my-30px">
	                <div class="pb-4"> No Guest houses meet your criteria. </div>
	            </div>
	        @endif
	    </div>
	</div>
</section>
<script src="/js/filter.min.js"></script>
<script>
    
    $(document).ready(function() {
        var FJS;
        $.ajax({
          url: "/api/homes",
          context: document.body
        }).done(function(data) {
			console.log(data);
            if (data.length) {
				$( "#slider-range" ).slider({
					range: true,
					min: 0,
					max: 3000,
					values: [ 0, 3000 ],
					slide: function( event, ui ) {
				        $( "#amount" ).html( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
						$( "#price" ).val( ui.values[ 0 ] + "-" + ui.values[ 1 ] );
						$( "#price" ).change();
					}
			    });
		  	  	$( "#amount" ).html( "$" + $( "#slider-range" ).slider( "values", 0 ) + " - $" + $( "#slider-range" ).slider( "values", 1 ) );
				$( "#price" ).val( $( "#slider-range" ).slider( "values", 0 ) + "-" + $( "#slider-range" ).slider( "values", 1 ) );

                var FJS = FilterJS(data, '.home-items', {
                    template: '#home-template2'
                });
                // FJS.addCriteria({field: 'year', ele: '#year_filter', type: 'range'})
                FJS.addCriteria({
                    field: 'roomtype_code',
                    ele: '#roomtype input:radio',
                    all: 'all'
                });
                FJS.addCriteria({
                    field: 'bath',
                    ele: '#attached-bath input:checkbox',
                });
                FJS.addCriteria({
                    field: 'stayterm',
                    ele: '#stayterm input:radio',
                    all: 'all'
                });
                FJS.addCriteria({
                    field: 'loc_code',
                    ele: '.filter #location input:checkbox'
                });
                FJS.addCriteria({
                    field: 'price',
                    ele: '#price-per-month #price',
                    type: 'range',
                    all: 'all'
                });
                FJS.addCallback('afterFilter', function(result){
                    if (result.length === 1) {
                        $('#results-count').html(result.length+' result');
                    } else {
                        $('#results-count').html(result.length+' results');
                    }
                });
                $('.homes-view').click(function() {
                    FJS.setTemplate('#'+$(this).attr('data-template'), true);
                    $('.homes-view').each(function(key, node){
                        $(node).removeClass('text-danger');
                    })
                    $(this).addClass('text-danger');
                });
            } else {
                var FJS = FilterJS(data, '.home-items', {
                    template: '#empty-template'
                });
            }
            window.FJS = FJS;
            
        });
    });
</script>
<script id="home-template1" type="text/html">
    <div class="item bg-white mb-2 item-temp1-inner">
        <div class="td-none text-black row">
    		<div class="col-sm-4 col-md-3 col-lg-2 text-center">
    			<img src="https://longtermstay.checkfront.com/<%= typeof(img)!== 'undefined' ?  img : '' %>" width="100%" height="auto">
    		</div>
    		<div class="col-sm-8 col-md-9 col-lg-9 pt-3 pt-md-0 content my-auto">
    			<a href="/home/<%= item_id %>">
                    <div class="text-capitalize"><%= name %></div>
                    <h5 class="mb-10px text-capitalize"><%= roomtype %></h5>
                </a>
    			<div class="d-md-block small-90"><%= summary %></div>
                <p class="fw-600">
                    <i class="small">Available at</i>
                    <span class="fw-600 text-capitalize"><%= loc %></span>
                    <i class="small">for</i>
                    <%=  typeof(rate.summary.price.total)!== 'undefined' ? rate.summary.price.total : ''%>
                    <i class="small">a month</i>
                </p>
    		</div>
        </div>
	</div>
</script>
<script id="home-template2" type="text/html">
    <div class="col-sm-4 col-md-4 col-lg-3 p-3 item-temp2">
		<div class="item-temp2-inner">
			<a href="/home/<%= item_id %>">
				<div class="text-center">
					<img src="https://longtermstay.checkfront.com/<%= typeof(img)!== 'undefined' ?  img : '' %>" style="height: auto; width:100%;border-radius: 4px 4px 0 0;">
				</div>
				<div class="item-temp2-link-wrapper">
					<a href="/home/<%= item_id %>">
		    			<div class="mb-1 text-capitalize ell" title="<%= name %>"><%= name %></div>
		    			<h5 class="mb-1 text-capitalize"><%= roomtype %></h5>
		            </a>
		            <div>
		                <div class="fw-600 text-capitalize"><%= loc %></div>
		                <div>
		                    <%= typeof(rate.summary.price.total)!== 'undefined' ? rate.summary.price.total : '' %>
		                    <i class="small">a month</i>
		                </div>
		            </div>
				</div>
			</a>
		</div>
	</div>
</script>
<script id="empty-template" type="text/html">
    <div class="text-center py-5">
        <div class="pb-4"> No homes meet your criteria. </div>
    </div>
</script>