<div class="testimonials section bg-f9f9f9">
	<div class="heading-wrapper">
		<h2 class="heading">{{$home_page['testimonials_heading']}}</h2>
		<div class="heading-subtext">{{$home_page['testimonials_subtxt']}}</div>
	</div>
	<div class="content-wrapper row text-center px-5">
		@if($embedsocial)
			<div class='embedsocial-reviews' data-ref="cb000702738eead93c7474a25a226fdab83f0c9f"></div><script>(function(d, s, id){var js; if (d.getElementById(id)) {return;} js = d.createElement(s); js.id = id; js.src = "https://embedsocial.com/embedscript/ri.js"; d.getElementsByTagName("head")[0].appendChild(js);}(document, "script", "EmbedSocialReviewsScript"));</script>
		@elseif($testimonials)
			<div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
				<div class="row">
					@foreach($testimonials as $testi)
						@if($loop->index > 2)
							@break
						@endif
						<div class="col-xs-12 col-sm-4 mb-10px">
							<div class="testi-wrap bg-white bd-r-8px">
								<div class="testi-text p-20px">
									<i class="fa fa-lg fa-quote-left"></i>
									<i>{{$testi['description']}}</i>
									<i class="fa fa-lg fa-quote-right"></i>
								</div>
								<div class="">
									@if($testi['image'])
										<img src="{{$testi['image']}}" width="68px">
									@else
										<i class="fa fa-user"></i>
									@endif
								</div>
								<div class="text-left p-20px mt-30px">
									<div>
										{{$testi['name']}}
									</div>
									<div class="pb-10px testi-company">{{$testi['company']}}</div>
									<div>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
									</div>
								</div>
							</div>
						</div>
					@endforeach
				</div>
			</div>
		@endif
	</div>
	<div class="btn-wrapper">
		<a href="{{ url($home_page['testimonials_url']) }}" class="btn btn-primary btn-lg">Read All Reviews →</a>
	</div>
</div>
