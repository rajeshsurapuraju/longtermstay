<div class="rooms section">
	<div class="heading-wrapper row">
		<h2 class="heading">Rooms that suit you.</h2>
		<div class='heading-subtext'>Choose between Private, Shared and Family rooms.</div>
	</div>
	<div class="content-wrapper row text-center">
		<div class="col-xs-12 col-sm-4">
			<div class="pb-20px">
				<img src="img/longtermstay_single.jpg">
			</div>
			<h3 class="bb-1d mb-10px">
				<a href="/rooms/private" class="heading">Private</a>
			</h3>
			<div>
				<div>Beds: 1 Bed, Executive Chair, Tables</div>
				<div>
					<span>Max person:</span>
					<i class="fa fa-user"></i>
				</div>
			</div>
			<div class="btn-wrapper">
				<a href="{{ url('/rooms/private') }}" class="btn btn-primary btn-lg">Explore Private Room</a>
			</div>
		</div>
		<div class="col-xs-12 col-sm-4">
			<div class="pb-20px">
				<img src="img/longtermstay_shared.jpg">
			</div>
			<h3 class="bb-1d mb-10px">
				<a href="/rooms/shared" class="heading">Shared</a>
			</h3>
			<div>
				<div>Beds: 2 Single Bed, Table and Exeuctive Chairs</div>
				<div>
					<span>Max person:</span>
					<i class="fa fa-user"></i>
					<i class="fa fa-user"></i>
				</div>
			</div>
			<div class="btn-wrapper">
				<a href="{{ url('/rooms/shared') }}" class="btn btn-primary btn-lg">Explore Shared Room</a>
			</div>
		</div>
		<div class="col-xs-12 col-sm-4">
			<div class="pb-20px">
				<img src="img/longtermstay_family.png">
			</div>
			<h3 class="bb-1d mb-10px">
				<a href="/rooms/family" class="heading">Family</a>
			</h3>
			<div>
				<div>Beds: 1 Queen Bed, Kid Beds</div>
				<div>
					<span>Max person:</span>
					<i class="fa fa-male"></i>
					<i class="fa fa-female"></i>
					<i class="fa fa-child"></i>
				</div>
			</div>
			<div class="btn-wrapper">
				<a href="{{ url('/rooms/family') }}" class="btn btn-primary btn-lg">Explore Family Room</a>
			</div>
		</div>
	</div>
	<!-- <div class="btn-wrapper">
		<a href="{{ url('/rooms') }}">Explore All Rooms →</a>
	</div> -->
</div>
