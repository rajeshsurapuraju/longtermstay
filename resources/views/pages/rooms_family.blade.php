<div class='heading-wrapper bg-white py-30px'>
	<h2 class='heading'>Family Rooms</h2>
</div>
<div class='rooms page'>
	<div class='content-wrapper container-fluid'>
		<div class="row">
			<div class="col-md-8 col-md-offset-2">		
				<div class="row pb-20px mb-20px">
					<div class="col-xs-12 col-sm-6 col-lg-5 text-center">
						<div class="bg-white">
							<img src="https://s3-us-west-1.amazonaws.com/longtermstay/family1.png" alt="ltstay family room">
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-lg-7">
						<p class="bb-1d pb-20px">
							Lease from <b>$2000 per month</b>
						</p>
						<div class="bb-1d pb-20px mb-10px">
							<div>
								Max person to be booked: 3
							</div>
							<div>
								Room bed: 1 Queen Bed, Kids Bed
							</div>
						</div>
						<p class="bb-1d pb-20px">
							Extending the long term stay option for families with possibly a small kid. Exclusive bathroom in a master suite. All you share is a kitchen with just another family like yours; Ready...
						</p>
					</div>
				</div>
				<div>
					<ul class="nav nav-tabs" id="myTab" role="tablist">
						<li class="nav-item active">
							<a class="nav-link" id="desc-tab" data-toggle="tab" href="#desc" role="tab" aria-controls="desc" aria-selected="true">Description</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="features-tab" data-toggle="tab" href="#features" role="tab" aria-controls="features" aria-selected="false">Features</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="policies-tab" data-toggle="tab" href="#policies" role="tab" aria-controls="policies" aria-selected="false">Policies</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="images-tab" data-toggle="tab" href="#images" role="tab" aria-controls="images" aria-selected="false">Images</a>
						</li>
					</ul>
					<div class="tab-content py-30px" id="myTabContent">
						<div class="tab-pane fade active in" id="desc" role="tabpanel" aria-labelledby="desc-tab">
							<div>
								<p><strong> Family Sharing Short Term: Get rewarded $600 to $750 each month <strong><br>
								</strong></strong></p>
								<p>The family room is not just a great place for your spouse (and a kid!!), the real gem is the pricing. Now who will say that maintaining a family in bay area costs you 2.5K plus. We have sub 2K for lease and just around that on month on month options.</p>
								<p>And that really means you don’t go out for groceries every weekend, but fun!!</p>
								<p>Need a place for your parent’s visiting USA. You have just reach a place which treats them in line with the Indian tradition.</p>
								<p>Wife visiting for just a few months, you can switch between our single/shared rooms and the family room exactly the days you need to. No extras.</p>
								<p>&nbsp;</p>
								<hr>
								<p><strong>Let’s do some&nbsp;maths</strong></p>
								<p><strong><em>First the Actual Data&nbsp;</em></strong><br>
								LTS lease rent for Family Sharing&nbsp;room is (six months minimum) is $2000.<br>
								LTS rent for Family Sharing&nbsp;room short term (month on month) is $2100.</p>
								<p>&nbsp;</p>
								<p>Source<br>
								<a href="http://www.numbeo.com/cost-of-living/compare_cities.jsp?country1=United+States&amp;country2=United+States&amp;city1=Sunnyvale%2C+CA&amp;city2=San+Jose%2C+CA&amp;tracking=getDispatchComparison" onclick="__gaTracker('send', 'event', 'outbound-article', 'http://www.numbeo.com/cost-of-living/compare_cities.jsp?country1=United+States&amp;country2=United+States&amp;city1=Sunnyvale%2C+CA&amp;city2=San+Jose%2C+CA&amp;tracking=getDispatchComparison', 'Numbeo');">Numbeo</a><br>
								<del>Data for family sharing&nbsp;short term not available or not gathered.</del><br>
								<em>Rent Per Month</em><br>
								<em> Apartment (1 bedroom) in City Centre at annual lease starts at 2,195.00 $ and averages up to 2,482.68 $ &nbsp;with an increase of +13.11 %.&nbsp;</em></p>
								<p><em>Whereas Apartment (1 bedroom) Outside of Centre at annual lease starts at 2,021.25 $ and averages up to 2,138.94 $ with an increase of +5.82 %</em></p>
								<p><a href="https://www.rentjungle.com/average-rent-in-santa-clara-rent-trends" onclick="__gaTracker('send', 'event', 'outbound-article', 'https://www.rentjungle.com/average-rent-in-santa-clara-rent-trends', 'RentJungle');">RentJungle</a><br>
								<del>Data for family sharing&nbsp;short term not available or not gathered.</del></p>
								<p><em>As of July 2016, average apartment rent within the city of of Santa Clara, CA is $3078.&nbsp;</em><em>One bedroom apartments in Santa Clara now&nbsp;rents for $2666 a month on an average while&nbsp;two bedroom apartment rents for an average of &nbsp;$3434 a month.</em></p>
								<p>&nbsp;</p>
								<p><em><strong>and now the details &nbsp;</strong></em></p>
								<blockquote><p>Assume we get a GRAND deal of 1&nbsp;BHK for your family at&nbsp;&nbsp;2200 (annual lease). You still spend around $2750 just to make it a living without furnishing*</p></blockquote>
								<ul>
								<li>Your lease costs you $2200.</li>
								<li>Your utilities (internet/Electricity only/Water) $150</li>
								<li>Your average monthly spend on all consumables (check your credit card averages) $300</li>
								<li>Lets assume California&nbsp;minimum wage and put another $50 for 3 hours of cleaning a month</li>
								</ul>
								<blockquote><p>Now add another $50 for furnished rooms and your total now is $2750 per month.</p>
								<p>To be fair, we are discounting any benefit of a month on month stay with that of a small tweak with privacy</p></blockquote>
								<p><em><strong>Take the weekly cleaning and all unlimited freebies on us. Enjoy to the fullest and do share how you will spend the extra 2.5 grands you save each year staying in a private room @ Long Term Stay.</strong></em></p>
								<blockquote><p>Comparing with&nbsp;sublease on Sulekha or Craigslist. Please allow us just $150 for all the complementary amenities and check out anything&nbsp;below $2200. You may&nbsp;find very few less than&nbsp;that; that too, mostly an inconvenient number of months left. And be prepared for surprise rental increase the next time you renew the lease. Average increase is easily $10% in bay area&nbsp;apartments..</p>
								<p>With us you are dealing with a process oriented company and not a person and every customer is more important than any of our own people. We know the reason we are here is to serve and delight you.</p></blockquote>
								<p><em>Need receipts to claims for your stay. Hey!! did we say we are a company and have served customer from more than 200 clients in bay area already.</em></p>
							</div>
						</div>
						<div class="tab-pane fade" id="features" role="tabpanel" aria-labelledby="features-tab">
							<div class="row">
								<div class="col-sm-6">
									<div>
										<i class="fa fa-star" aria-hidden="true"></i>
										Free Wi-Fi Internet inside rooms
									</div>
									<div>
										<i class="fa fa-star" aria-hidden="true"></i>
										Breakfast Items included
									</div>
									<div>
										<i class="fa fa-star" aria-hidden="true"></i>
										Airport Pickup and Drop
									</div>
									<div>
										<i class="fa fa-star" aria-hidden="true"></i>
										Satelite and Cable TV Included
									</div>
									<div>
										<i class="fa fa-star" aria-hidden="true"></i>
										Appliances
									</div>
								</div>
								<div class="col-sm-6">
									<div>
										<i class="fa fa-star" aria-hidden="true"></i>
										Room Cleaning Services
									</div>
									<div>
										<i class="fa fa-star" aria-hidden="true"></i>
										Beautiful view on the city
									</div>
									<div>
										<i class="fa fa-star" aria-hidden="true"></i>
										Free parking on Driveway, Street
									</div>
									<div>
										<i class="fa fa-star" aria-hidden="true"></i>
										Air Conditioning
									</div>
									<div>
										<i class="fa fa-star" aria-hidden="true"></i>
										Refrigerator
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane fade" id="policies" role="tabpanel" aria-labelledby="policies-tab">
							<div>
								<i class="fa fa-star" aria-hidden="true"></i>
								No Smoking in the room.
							</div>
							<div>
								<i class="fa fa-star" aria-hidden="true"></i>
								Pets are not allowed
							</div>
							<div>
								<i class="fa fa-star" aria-hidden="true"></i>
								Cancellation and prepayment policies vary according to room type. Please email to us to learn more about it.
							</div>
							<div>
								<i class="fa fa-star" aria-hidden="true"></i>
								When booking more than 5 rooms, different policies and additional supplements may apply.
							</div>
						</div>
						<div class="tab-pane fade" id="images" role="tabpanel" aria-labelledby="images-tab">
							<div class='embedsocial-album' data-ref="27a62ec73c995efb1ed38131b2cc92c4dde0fb22"></div><script>(function(d, s, id){var js; if (d.getElementById(id)) {return;} js = d.createElement(s); js.id = id; js.src = "https://embedsocial.com/embedscript/ei.js"; d.getElementsByTagName("head")[0].appendChild(js);}(document, "script", "EmbedSocialScript"));</script>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>