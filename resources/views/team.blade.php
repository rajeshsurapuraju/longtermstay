@extends('layouts.main')

@include('pages.meta-tags', ['meta' => $meta[14]])

@section('header')
	    @include('pages.header')
@endsection

@section('body')
<div class='heading-wrapper py-30px bg-white'>
	<h2 class='heading'>Meet Team Long Term Stay</h2>
</div>
<div class='about page'>
	<div class='row'>
		<div class="col-lg-8 col-lg-offset-2">
			<div class='content-wrapper row'>
				@foreach($team as $t)
					<div class="col-sm-3">
					<div>
						<a href="#" class="d-block text-center">
							@if($t['image'])
								<img src="{{$t['image']}}" width="100%"  class="bdrs-4px">
							@else
								<i class="fa fa-user" style="font-size: 100px;"></i>
							@endif
						</a>
					</div>
					<div class="py-20px text-center">
						<h4 class="name mb-10px">{{$t["name"]}}</h4>
						<div class="designation">{{$t["designation"]}}</div>
					</div>
				</div>
				@endforeach
			</div>
		</div>
	</div>
</div>
@endsection
