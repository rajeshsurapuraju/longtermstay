@extends('layouts.main')

@include('pages.meta-tags', ['meta' => $meta[2]])

@section('header')
	    @include('pages.header')
@endsection

@section('body')
	    @include('pages.locations_final')
@endsection
