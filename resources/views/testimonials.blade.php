@extends('layouts.main')

@include('pages.meta-tags', ['meta' => $meta[15]])

@section('header')
	    @include('pages.header')
@endsection

@section('search')
	    @include('pages.search')
@endsection
@section('body')
	    @include('pages.testimonials', ['embedsocial' => env('EMBED_SOCIAL', false)])
@endsection
