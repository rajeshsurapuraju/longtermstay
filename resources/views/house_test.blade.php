@extends('layouts.main')

@include('pages.meta-tags', ['meta' => $meta[1]])

@section('header')
	    @include('pages.header')
@endsection

@section('body')
<main class="my-3">
    @include('pages.homedetail1')
</main>
@endsection
