@extends('layouts.main')

@include('pages.meta-tags', ['meta' => $meta[5]])

@section('header')
	    @include('pages.header')
@endsection

@section('body')
	    @include('pages.contact')
@endsection
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Organization",
  "url": "https://www.longtermstay.co",
  "name": "Long Term Stay",
  "contactPoint": {
    "@type": "ContactPoint",
    "telephone": "+1-844-4LTSTAY",
    "contactType": "Customer service"
  }
}
</script>