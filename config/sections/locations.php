<?php

return [
	'Select a location' => 'select',
	'Sunnyvale' => 'SV',
	'San Jose' => 'SJ',
	'Cupertino' => 'CT',
	'Santa Clara' => 'SC',
	'Piscataway NJ' => 'PNJ',
	'Milpitas' => 'MP'
];