<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');
Route::get('/locations', 'PagesController@locations');
Route::get('/amenities', 'PagesController@amenities');
Route::get('/contact', 'PagesController@contact');
Route::post('/contact', 'PagesController@postContact');
Route::get('/about', 'PagesController@about');
Route::get('/help', 'HelpController@index');
Route::get('/search_validation', 'PagesController@search_validation');
Route::post('/booking_validation', 'BookController@book_validation');
Route::get('/listings', 'ListingsController@listings');
Route::post('/select', 'ListingsController@select');
Route::get('/book', 'ListingsController@book');
Route::get('/locations', 'PagesController@locations');

Route::get('/rooms', 'RoomsController@index');
Route::get('/rooms/private', 'RoomsController@privateroom');
Route::get('/rooms/shared', 'RoomsController@sharedroom');
Route::get('/rooms/family', 'RoomsController@familyroom');

Route::get('/home/{id}', 'HouseController@show');

Route::post('/book/{id}', 'BookController@index');
Route::get('/book/{id}', 'BookController@index');
Route::post('/book/validation/{id}', 'BookController@validation');

Route::get('/gallery', 'GalleryController@index');

Route::get('/sitemap', 'SiteMapController@index');
Route::get('/sitemap.xml', 'SiteMapController@index');
Route::get('/testimonials', 'TestimonialsController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/dreamhome/{name}', 'DreamHomeController@showSlug')->where('name', '[A-Za-z0-9-]+');

Route::get('/shared-living', 'SharedLivingController@index');
Route::get('/team', 'PagesController@team');

// social login
Route::get('/redirect', 'Auth\LoginController@redirectToProvider');
Route::get('/callback', 'Auth\LoginController@handleProviderCallback');

Route::get('/api/homes', function() {
    return Session::get('items');
});

Route::get('/home/detail/sunnyvale', 'HouseController@house_sunnyvale');
Route::get('/home/detail/sanjose-north', 'HouseController@house_sanjose');
Route::get('/home/detail/cupertino', 'HouseController@house_cupertino');
Route::get('/home/detail/santaclara', 'HouseController@house_santaclara');
Route::get('/home/detail/piscataway', 'HouseController@house_piscataway');
Route::get('/home/detail/milpitas', 'HouseController@house_milpitas');
Route::get('/home/detail/metuchen', 'HouseController@house_metuchen');
Route::get('/home/detail/levis-stadium-sanjose', 'HouseController@house_levis');
